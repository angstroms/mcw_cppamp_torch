--
--  Copyright (c) 2014, Facebook, Inc.
--  All rights reserved.
--
--  This source code is licensed under the BSD-style license found in the
--  LICENSE file in the root directory of this source tree. An additional grant
--  of patent rights can be found in the PATENTS file in the same directory.
--
require 'torch'
require 'gputorch'
require 'paths'
require 'xlua'
require 'optim'

local opts = paths.dofile('opts.lua')

opt = opts.parse(arg)
print(opt)

torch.setdefaulttensortype('torch.FloatTensor')

gputorch.setDevice(opt.GPU) -- by default, use GPU 1
torch.manualSeed(opt.manualSeed)

print('Saving everything to: ' .. opt.save)
os.execute('mkdir -p ' .. opt.save)

paths.dofile('data.lua')
paths.dofile('model.lua')
paths.dofile('train.lua')
paths.dofile('test.lua')

epoch = opt.epochNumber

clock = {}
clock.log = 0
clock.log1 = 0
clock.log1 = os.time()

for i=1,opt.nEpochs do
   train()
--   test()
   epoch = epoch + 1
end

print('Total time : ' .. os.time() - clock.log1 .. ' sec')

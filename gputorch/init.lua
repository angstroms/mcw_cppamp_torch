require "torch"
gputorch = require "libgputorch"

torch.GPUStorage.__tostring__ = torch.FloatStorage.__tostring__
torch.GPUTensor.__tostring__ = torch.FloatTensor.__tostring__

include('Tensor.lua')
include('FFI.lua')
include('test.lua')

function gputorch.withDevice(newDeviceID, closure)
  local curDeviceID = gputorch.getDevice()
  gputorch.setDevice(newDeviceID)
  local vals = {pcall(closure)}
  gputorch.setDevice(curDeviceID)
  if vals[1] then
    return unpack(vals, 2)
  end
  error(unpack(vals, 2))
end

return gputorch

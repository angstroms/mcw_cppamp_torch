#include "THCBolt.h"
template<typename UnaryFunction>
void transform(THCState *state, THGPUTensor *first, THGPUTensor *result, UnaryFunction op)
{
  auto avData_first = first->get_array_view();
  auto avData_result = result->get_array_view();
  long size = THGPUTensor_nElement(state, first) - first->storageOffset;
  unary_transform(state, avData_first, first->storageOffset, avData_result, result->storageOffset, size, op);
}
template<typename BinaryFunction>
void transform(THCState *state, THGPUTensor *first1, THGPUTensor *first2, THGPUTensor *result, BinaryFunction op)
{
  auto avData_first1 = first1->get_array_view();
  auto avData_first2 = first2->get_array_view();
  auto avData_result = result->get_array_view();
  long size = THGPUTensor_nElement(state, first1) - first1->storageOffset;
  binary_transform(state, avData_first1, first1->storageOffset, avData_first2, first2->storageOffset,
                   avData_result, result->storageOffset, size, op);
}

template<typename BinaryFunction>
void binary_transform(THCState *state, Concurrency::array_view<float, 1>& first1, long first1Offset,
                      Concurrency::array_view<float, 1>& first2, long first2Offset,
                      Concurrency::array_view<float, 1>& result, long resultOffset, long size,  BinaryFunction& f)
{
  if (size == 0)
    return;

  unsigned grdSz = (size + 255) & ~(255);
  Concurrency::extent<1> grdExt(grdSz);
  Concurrency::tiled_extent<256> t_ext(grdExt);

  THCGPUDeviceState* device_state = state->deviceState;
  Concurrency::accelerator_view accl_view = device_state->get_current_accelerator_view();
  Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<256> tidx) restrict(amp)
  {
    long index = tidx.global[0];

    if(index < size)
      result[resultOffset + index] = (float) f(first1[first1Offset +index], first2[first2Offset + index]);
  });

  return;
}

template<typename UnaryFunction>
void unary_transform(THCState *state, Concurrency::array_view<float, 1>& first, long firstOffset, 
                     Concurrency::array_view<float, 1>& result, long resultOffset, long size, UnaryFunction &f)
{
  if (size == 0)
    return;

  unsigned grdSz = (size + 256) -(size % 256);
  Concurrency::extent<1> grdExt(grdSz);
  Concurrency::tiled_extent<256> t_ext(grdExt);

  THCGPUDeviceState* device_state = state->deviceState;
  Concurrency::accelerator_view accl_view = device_state->get_current_accelerator_view();
  Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<256> tidx) restrict(amp)
  {
    long index = tidx.global[0];

    if(index < size)
      result[resultOffset + index] = f(first[firstOffset + index]);
  });

  return;
}

void fill(THCState *state, Concurrency::array_view<float, 1>& dest, long size, float  val)
{
  unsigned grdSz = (size + 256) -(size % 256);
  Concurrency::extent<1> grdExt(grdSz);
  Concurrency::tiled_extent<256> t_ext(grdExt);

  THCGPUDeviceState* device_state = state->deviceState;
  Concurrency::accelerator_view accl_view = device_state->get_current_accelerator_view();
  Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<256> tidx) restrict(amp)
  {
    int globalId = tidx.global[0];
    if( globalId >= size)
      return;

    dest[globalId] = val;
  });
}
float boltInnerProduct_plus_mse(THCState *state, THGPUTensor *input, THGPUTensor *target)
{
  auto dv_input_data = input->get_bolt_dev_vec(state);
  auto dv_target_data = target->get_bolt_dev_vec(state);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  return bolt::amp::inner_product(devControl, dv_input_data.begin() + input->storageOffset,
                                  dv_input_data.begin() + input->storageOffset + THGPUTensor_nElement(state, input),
                                  dv_target_data.begin() + target->storageOffset,
                                  (float) 0, bolt::amp::plus<float>(), mse_functor());
}

float boltInnerProduct_plus_abs(THCState *state, THGPUTensor *input, THGPUTensor *target)
{
  auto dv_input_data = input->get_bolt_dev_vec(state);
  auto dv_target_data = target->get_bolt_dev_vec(state);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  return bolt::amp::inner_product(devControl, dv_input_data.begin() + input->storageOffset,
                                  dv_input_data.begin() + input->storageOffset + THGPUTensor_nElement(state, input),
                                  dv_target_data.begin() + target->storageOffset,
                                  (float) 0, bolt::amp::plus<float>(), binary_abs_functor());
}

float boltInnerProduct_plus_dist(THCState *state, THGPUTensor *self, THGPUTensor *src, float value)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  return bolt::amp::inner_product(devControl, dv_self_data.begin() + self->storageOffset,
                                  dv_self_data.begin() + self->storageOffset + size,
                                  dv_src_data.begin() + src->storageOffset,
                                  (float) 0,
                                  bolt::amp::plus<float>(),
                                  dist_functor(value));
}

float boltInnerProduct_plus_kl(THCState *state, THGPUTensor *input, THGPUTensor *target)
{
  auto dv_input_data = input->get_bolt_dev_vec(state);
  auto dv_target_data = target->get_bolt_dev_vec(state);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  return bolt::amp::inner_product(devControl, dv_input_data.begin() + input->storageOffset,
                                  dv_input_data.begin() + input->storageOffset + THGPUTensor_nElement(state, input),
                                  dv_target_data.begin() + target->storageOffset,
                                  (float) 0, bolt::amp::plus<float>(), kl_functor());

}

void boltTransform_mse(THCState *state, THGPUTensor *input, THGPUTensor *target, THGPUTensor *gradInput,float norm)
{
  auto dv_input_data = input->get_bolt_dev_vec(state);
  auto dv_target_data = target->get_bolt_dev_vec(state);
  auto dv_gradInput_data = gradInput->get_bolt_dev_vec(state);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_input_data.begin() + input->storageOffset,
                       dv_input_data.begin() + input->storageOffset + THGPUTensor_nElement(state, input),
                       dv_target_data.begin() + target->storageOffset,
                       dv_gradInput_data.begin() + gradInput->storageOffset,
                       mse_updateGradInput_functor(norm));
}

void boltTransform_abs(THCState *state, THGPUTensor *input, THGPUTensor *target, THGPUTensor *gradInput,float norm)
{
  auto dv_input_data = input->get_bolt_dev_vec(state);
  auto dv_target_data = target->get_bolt_dev_vec(state);
  auto dv_gradInput_data = gradInput->get_bolt_dev_vec(state);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_input_data.begin() + input->storageOffset,
                       dv_input_data.begin() + input->storageOffset + THGPUTensor_nElement(state, input),
                       dv_target_data.begin() + target->storageOffset,
                       dv_gradInput_data.begin() + gradInput->storageOffset,
                       abs_updateGradInput_functor(norm));
}

void boltTransform_kl(THCState *state, THGPUTensor *input, THGPUTensor *target, THGPUTensor *gradInput,float norm)
{
  auto dv_input_data = input->get_bolt_dev_vec(state);
  auto dv_target_data = target->get_bolt_dev_vec(state);
  auto dv_gradInput_data = gradInput->get_bolt_dev_vec(state);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_input_data.begin() + input->storageOffset,
                       dv_input_data.begin() + input->storageOffset + THGPUTensor_nElement(state, input),
                       dv_target_data.begin() + target->storageOffset,
                       dv_gradInput_data.begin() + gradInput->storageOffset,
                       kl_updateGradInput_functor(norm));
}

float boltTransform_var_all(THCState *state, THGPUTensor *self, float mean)
{
  long size = THGPUTensor_nElement(state, self);
  auto dv_self_data = self->get_bolt_dev_vec(state);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::device_vector<float> dvdiff(size, true,  devControl);
  bolt::amp::transform(devControl, dv_self_data.begin() + self->storageOffset,
                       dv_self_data.begin() + self->storageOffset + size,
                       dvdiff.begin(),
                       std::bind2nd(bolt::amp::minus<float>(), mean));

  return bolt::amp::inner_product(dvdiff.begin(), dvdiff.end(), dvdiff.begin(), 0.0);
}

void boltTransform_addvalue(THCState *state, THGPUTensor *src, THGPUTensor *self, float value)
{
 transform(state, src, self, addvalue_functor(value));
}

void boltTransform_mulvalue(THCState *state, THGPUTensor *src, THGPUTensor *self, float value)
{
  auto dv_dest_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_dest_data.begin() + self->storageOffset,
                       mulvalue_functor(value));
}

void boltTransform_divvalue(THCState *state, THGPUTensor *src, THGPUTensor *self, float value)
{
  auto dv_dest_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_dest_data.begin() + self->storageOffset,
                       divvalue_functor(value));
}

void boltTransform_pow(THCState *state, THGPUTensor *src, THGPUTensor *self, float value)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset,
                       pow_functor(value));
}

void boltTransform_clamp(THCState *state, THGPUTensor *src, THGPUTensor *self, float min_value, float max_value)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset,
                       clamp_functor(min_value,max_value));
}

void boltTransform_log(THCState *state, THGPUTensor *src, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset, log_functor());
}

void boltTransform_log1p(THCState *state, THGPUTensor *src, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset, log1p_functor());
}

void boltTransform_exp(THCState *state, THGPUTensor *src, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset, exp_functor());
}

void boltTransform_cos(THCState *state, THGPUTensor *src, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset, cos_functor());
}

void boltTransform_acos(THCState *state, THGPUTensor *src, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset, acos_functor());
}

void boltTransform_cosh(THCState *state, THGPUTensor *src, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset, cosh_functor());
}

void boltTransform_sin(THCState *state, THGPUTensor *src, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset, sin_functor());
}

void boltTransform_asin(THCState *state, THGPUTensor *src, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset, asin_functor());
}

void boltTransform_sinh(THCState *state, THGPUTensor *src, THGPUTensor *self) {
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset, sinh_functor());
}

void boltTransform_tan(THCState *state, THGPUTensor *src, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset, tan_functor());
}

void boltTransform_atan(THCState *state, THGPUTensor *src, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset, atan_functor());
}

void boltTransform_tanh(THCState *state, THGPUTensor *src, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset, tanh_functor());
}

void boltTransform_sqrt(THCState *state, THGPUTensor *src, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset, sqrt_functor());
}

void boltTransform_ceil(THCState *state, THGPUTensor *src, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset, ceil_functor());
}

void boltTransform_floor(THCState *state, THGPUTensor *src, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset, floor_functor());
}

void boltTransform_abs(THCState *state, THGPUTensor *src, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset, abs_functor());
}

void boltTransform_round(THCState *state, THGPUTensor *src, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset, round_functor());
}

void boltTransform_sign(THCState *state, THGPUTensor *src, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src_data.begin() + src->storageOffset,
                       dv_src_data.begin() + src->storageOffset + size,
                       dv_self_data.begin() + self->storageOffset,
                       sign_functor());
}

void boltTransformBinary_multiply(THCState *state, THGPUTensor *src1, THGPUTensor *src2, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src1_data = src1->get_bolt_dev_vec(state);
  auto dv_src2_data = src2->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_src2_data.begin() + src2->storageOffset,
                       dv_src2_data.begin() + src2->storageOffset + size,
                       dv_src1_data.begin() + src1->storageOffset,
                       dv_self_data.begin() + self->storageOffset,
                       bolt::amp::multiplies<float>());
}

void boltTransformBinary_divide(THCState *state, THGPUTensor *src1, THGPUTensor *src2, THGPUTensor *self)
{
  transform(state, src1, src2, self, bolt::amp::divides<float>()); 
}

void boltTransformBinary_atan2(THCState *state, THGPUTensor *tx, THGPUTensor *ty, THGPUTensor *self)
{
  auto dv_tx_data = tx->get_bolt_dev_vec(state);
  auto dv_ty_data = ty->get_bolt_dev_vec(state);
  auto dv_self_data = self->get_bolt_dev_vec(state);
  long size = THGPUTensor_nElement(state, self);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  bolt::amp::transform(devControl, dv_tx_data.begin() + tx->storageOffset,
                       dv_tx_data.begin() + tx->storageOffset + size,
                       dv_ty_data.begin() + ty->storageOffset,
                       dv_self_data.begin() + self->storageOffset,
                       atan2_functor());
}

float boltReduce_minimum(THCState *state, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  return bolt::amp::reduce(devControl, dv_self_data.begin() + self->storageOffset,
                           dv_self_data.begin() + self->storageOffset + THGPUTensor_nElement(state, self),
                           (float)(THInf),
                           bolt::amp::minimum<float>());
}

float boltReduce_maximum(THCState *state, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  return bolt::amp::reduce(devControl, dv_self_data.begin() + self->storageOffset,
                           dv_self_data.begin() + self->storageOffset + THGPUTensor_nElement(state, self),
                           (float)(-THInf),
                           bolt::amp::maximum<float>());
}

float boltReduce_plus(THCState *state, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  return bolt::amp::reduce(devControl, dv_self_data.begin() + self->storageOffset,
                           dv_self_data.begin() + THGPUTensor_nElement(state, self),
                           (float)(0),
                           bolt::amp::plus<float>());
}

float boltReduce_multiply(THCState *state, THGPUTensor *self)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  return bolt::amp::reduce(devControl, dv_self_data.begin() + self->storageOffset,
                           dv_self_data.begin() + self->storageOffset + THGPUTensor_nElement(state, self),
                           (float)(1),
                           bolt::amp::multiplies<float>());
}

float boltInnerPdt(THCState *state, THGPUTensor *self, THGPUTensor *src)
{
  auto dv_self_data = self->get_bolt_dev_vec(state);
  auto dv_src_data = src->get_bolt_dev_vec(state);
  bolt::amp::control &devControl(bolt::amp::control::getDefault());
  devControl.setAccelerator(state->deviceState->get_current_accelerator());
  float result = bolt::amp::inner_product(devControl, dv_self_data.begin(), dv_self_data.end(), dv_src_data.begin(), 0.0);
  return result;
}


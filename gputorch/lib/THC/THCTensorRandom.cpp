#include "THCTensorRandom.h"
#include "THCGeneral.h"
#include <amp.h>
#include <amp_math.h>
#include <MTGP/gpurand_mtgp32.h>

#define MAX_NUM_BLOCKS 64
#define BLOCK_SIZE 256

#ifndef DIVUP
#define DIVUP(x, y) (((x) + (y) - 1) / (y))
#endif

/* Sets up generator. Allocates but does not create the generator states. */
void initializeGenerator(THCState* state, Generator* gen)
{
  assert(gen);
  gen->gen_states = new GPURandStateMtgp32;
  assert(gen->gen_states);
  GPURandStateMtgp32_init(state->deviceState->get_current_accelerator_view(), gen->gen_states);
}

/* Frees memory allocated during setup. */
void destroyGenerator(Generator* gen)
{
  if (gen->gen_states)
  {
    GPURandStateMtgp32_release(gen->gen_states);
    delete gen->gen_states;
  }
}

/* Creates a new generator state given the seed. */
void createGeneratorState(THCState* state, Generator* gen, unsigned long seed)
{
  Concurrency::accelerator_view accl_view = state->deviceState->get_current_accelerator_view();
  if (mtgp32_init_params_kernel(accl_view, mtgp32_params_fast_11213, gen->gen_states))
  {
    THError("Creating MTGP constants failed.");
  }
  // Using device API
  if (mtgp32_init_seed_kernel(accl_view, gen->gen_states, seed))
  {
    THError("Creating MTGP kernel state failed.");
  }
}

/* Initialize generator array (must be called before any other function) */
void THCRandom_init(THCState *state, int devices, int current_device)
{
  THCRNGState* rng_state = state->rngState;
  rng_state->num_devices = devices;
  rng_state->gen = (Generator*)malloc(rng_state->num_devices * sizeof(Generator));
  for (int i = 0; i < rng_state->num_devices; ++i)
  {
    rng_state->gen[i].initf = 0;
    rng_state->gen[i].initial_seed = 0;
    rng_state->gen[i].gen_states = NULL;
  }
  rng_state->current_gen = &rng_state->gen[current_device];
  // Initialize the generator for the current device. Other generators will be
  // initialized on-demand in THCRandom_setGenerator.
  initializeGenerator(state, rng_state->current_gen);
  THCRandom_seed(state);
}

/* Destroy generators and free memory */
void THCRandom_shutdown(THCState *state)
{
  THCRNGState* rng_state = state->rngState;
  if (rng_state->gen == NULL) return;
  for (int i = 0; i < rng_state->num_devices; ++i)
  {
    destroyGenerator(&rng_state->gen[i]);
  }
  free(rng_state->gen);
  rng_state->gen = NULL;
  rng_state->current_gen = NULL;
  // Manually release memory
  free(rng_state);
  rng_state = NULL;
}

/* Set the generator for the current device */
/* device: the device index starting from 0 */
void THCRandom_setGenerator(THCState *state, int device)
{
  THCRNGState* rng_state = state->rngState;
  if (device >= rng_state->num_devices) THError("Invalid device index.");
  rng_state->current_gen = &rng_state->gen[device];
  if (rng_state->current_gen->initf == 0)
  {
    initializeGenerator(state, rng_state->current_gen);
    THCRandom_seed(state);
  }
}

/* Reset the generator for the current device after a device reset */
void THCRandom_resetGenerator(THCState *state)
{
  THCRNGState* rng_state = state->rngState;
  initializeGenerator(state, rng_state->current_gen);
  THCRandom_manualSeed(state, rng_state->current_gen->initial_seed);
}

/* Random seed */
unsigned long THCRandom_seed(THCState *state)
{
  unsigned long s = (unsigned long)time(0);
  THCRandom_manualSeed(state, s);
  return s;
}

unsigned long THCRandom_seedAll(THCState *state)
{
  unsigned long s = (unsigned long)time(0);
  THCRandom_manualSeedAll(state, s);
  return s;
}

/* Manually set the seed */
void THCRandom_manualSeed(THCState *state, unsigned long seed)
{
  THCRNGState* rng_state = state->rngState;
  if (rng_state->current_gen == NULL)
  {
    THError("Random number generators have not been initialized.");
  }
  rng_state->current_gen->initial_seed = seed;
  createGeneratorState(state, rng_state->current_gen, seed);
  rng_state->current_gen->initf = 1;
}

void THCRandom_manualSeedAll(THCState *state, unsigned long seed)
{
  THCRNGState* rng_state = state->rngState;
  int currentDevice = state->deviceState->getCurrentDevID();
  for (int i = 0; i < rng_state->num_devices; ++i) {
    // device ID starts from 1
    state->deviceState->setCurrentDevID(i + 1);
    THCRandom_setGenerator(state, i);
    THCRandom_manualSeed(state, seed);
  }
  // Restore current device
  state->deviceState->setCurrentDevID(currentDevice);
  THCRandom_setGenerator(state, currentDevice - 1);
}

/* Get the initial seed */
unsigned long THCRandom_initialSeed(THCState *state)
{
  return state->rngState->current_gen->initial_seed;
}

void THCRandom_getRNGState(THCState *state, THByteTensor *rng_state)
{
  // The RNG state comprises the MTPG32 states and the seed.
  static const size_t states_size = 1 * sizeof(HOSTRandStateMtgp32);
  static const size_t seed_size = sizeof(unsigned long);
  static const size_t total_size = states_size + seed_size;
  THByteTensor_resize1d(rng_state, total_size);
  THArgCheck(THByteTensor_nElement(rng_state) == total_size, 1, "RNG state is wrong size");
  THArgCheck(THByteTensor_isContiguous(rng_state), 1, "RNG state must be contiguous");
  // FIXME: We get array_view pointers from stack, so host copy is fine, unless it is freed ahead
  GPURandStateMtgp32_copy_D2H(state->rngState->current_gen->gen_states,
                              THByteTensor_data(rng_state));
  memcpy(THByteTensor_data(rng_state) + states_size, &state->rngState->current_gen->initial_seed, seed_size);
}

void THCRandom_setRNGState(THCState *state, THByteTensor *rng_state)
{
  static const size_t states_size = 1 * sizeof(HOSTRandStateMtgp32);
  static const size_t seed_size = sizeof(unsigned long);
  static const size_t total_size = states_size + seed_size;
  THArgCheck(THByteTensor_nElement(rng_state) == total_size, 1, "RNG state is wrong size");
  THArgCheck(THByteTensor_isContiguous(rng_state), 1, "RNG state must be contiguous");
  GPURandStateMtgp32_copy_H2D(THByteTensor_data(rng_state),
                              state->rngState->current_gen->gen_states);
  memcpy(&state->rngState->current_gen->initial_seed, THByteTensor_data(rng_state) + states_size, seed_size);
}
// ALlow GPU-based distributions from now on
#if 0
// TODO: currently can't use pfe since no kernel versions of all GPURAND_FUNC from underlying AMP
// Just prepare data on host and then copy to device side of the array
#define GENERATE_KERNEL1(NAME, ARG1, GPURAND_FUNC, TRANSFORM)                                              \
void NAME(THCState* state, int size, THGPUTensor *result, ARG1)                                            \
{                                                                                                          \
  std::mt19937 gen;                                                                                        \
  float* vec = (float *)malloc(size * sizeof(float));                                                      \
  for (int i = 0; i < size; i++) {                                                                         \
    std::GPURAND_FUNC<float> rand(0.0, 0.9);                                                               \
    float x = rand(gen);                                                                                   \
    x = TRANSFORM;                                                                                         \
    vec[i] = x;                                                                                            \
  }                                                                                                        \
  auto avDst = result->get_array_view();                                                                   \
  auto Dst = avDst.section(Concurrency::index<1>(result->storageOffset), Concurrency::extent<1>(size));    \
  Concurrency::copy(vec, vec + size, Dst);                                                                 \
  free(vec);                                                                                               \
}

// TODO: currently can't use pfe since no kernel versions of all GPURAND_FUNC from underlying AMP
// Just prepare data on host and then copy to device side of the array
#define GENERATE_KERNEL2(NAME, ARG1, ARG2, GPURAND_FUNC, TRANSFORM)                                        \
void NAME(THCState* state, int size, THGPUTensor *result, ARG1, ARG2)                                      \
{                                                                                                          \
  std::mt19937 gen;                                                                                        \
  float* vec = (float *)malloc(size * sizeof(float));                                                      \
  for (int i = 0; i < size; i++) {                                                                         \
    std::GPURAND_FUNC<float> rand(0, 0.9);                                                                 \
    float x = rand(gen);                                                                                   \
    x = TRANSFORM;                                                                                         \
    vec[i] = x;                                                                                            \
  }                                                                                                        \
  auto avDst = result->get_array_view();                                                                   \
  auto Dst = avDst.section(Concurrency::index<1>(result->storageOffset), Concurrency::extent<1>(size));    \
  Concurrency::copy(vec, vec + size, Dst);                                                                 \
  free(vec);                                                                                               \
}

GENERATE_KERNEL2(generate_uniform, double a, double b, uniform_real_distribution, x * (b - a) + a)
GENERATE_KERNEL1(generate_bernoulli, double p, uniform_real_distribution, (float)x <= p)
GENERATE_KERNEL2(generate_normal, double mean, double stdv, normal_distribution,(float)((x * stdv) + mean))
GENERATE_KERNEL1(generate_geometric, double p, uniform_real_distribution, (log(1 - x) / log(p)) + 1)
GENERATE_KERNEL1(generate_exponential, double lambda, uniform_real_distribution, (float)(-1. / lambda * log(1 - x)))
GENERATE_KERNEL2(generate_cauchy, double median, double sigma, uniform_real_distribution, (float)(median + sigma * tan(M_PI*(x-0.5))))
#else
#define GENERATE_KERNEL1(NAME, ARG1, GPURAND_FUNC, FUNCTOR)                                                \
void NAME(THCState* state, int size, THGPUTensor *result, double ARG1)                                     \
{                                                                                                          \
  GPURandStateMtgp32* s = state->rngState->current_gen->gen_states;                                        \
  Concurrency::accelerator_view accl_view = state->deviceState->get_current_accelerator_view();            \
  Concurrency::array_view<float> av_result = result->get_array_view();                                     \
  GPURAND_FUNC##_kernel(accl_view, s, av_result, FUNCTOR);                                                 \
}
#define GENERATE_KERNEL2(NAME, ARG1, ARG2, GPURAND_FUNC, FUNCTOR)                                          \
void NAME(THCState* state, int size, THGPUTensor *result, double ARG1, double ARG2)                        \
{                                                                                                          \
  GPURandStateMtgp32* s = state->rngState->current_gen->gen_states;                                        \
  Concurrency::accelerator_view accl_view = state->deviceState->get_current_accelerator_view();            \
  Concurrency::array_view<float> av_result = result->get_array_view();                                     \
  GPURAND_FUNC##_kernel(accl_view, s, av_result, FUNCTOR);                                                 \
}
struct user_uniform_functor {
  const double _a;
  const double _b;
  user_uniform_functor(double a, double b) restrict(amp,cpu) : _a(a), _b(b) {}
  inline double operator()(const float& x) const restrict(amp,cpu) {
    return x * (_b - _a) + _a;
  }
};
GENERATE_KERNEL2(generate_uniform, a, b, user_uniform, user_uniform_functor(a,b))
struct user_bernoulli_functor {
  const double _p;
  user_bernoulli_functor(double p) restrict(amp,cpu) : _p(p) {}
  inline double operator()(const float& x) const restrict(amp,cpu) {
    return (double)x <= _p;
  }
};
GENERATE_KERNEL1(generate_bernoulli, p, user_uniform, user_bernoulli_functor(p))
struct user_normal_functor {
  const double _stdv;
  const double _mean;
  user_normal_functor(double stdv, double mean) restrict(amp,cpu) : _stdv(stdv), _mean(mean) {}
  inline double operator()(const float& x) const restrict(amp,cpu) {
    return (x * _stdv) + _mean;
  }
};
GENERATE_KERNEL2(generate_normal, mean, stdv, user_normal, user_normal_functor(stdv, mean))
struct user_geometric_functor {
  const double _p;
  user_geometric_functor(double p) restrict(amp,cpu) : _p(p) {}
  inline double operator()(const float& x) const restrict(amp,cpu) {
    return (Concurrency::precise_math::log((double)(1 - x)) / Concurrency::precise_math::log(_p)) + 1;
  }
};
GENERATE_KERNEL1(generate_geometric, p, user_uniform, user_geometric_functor(p))
struct user_exponential_functor {
  const double _lambda;
  user_exponential_functor(double lambda) restrict(amp,cpu) : _lambda(lambda) {}
  inline double operator()(const float& x) const restrict(amp,cpu) {
    return (double)(-1. / _lambda * Concurrency::precise_math::log((double)(1 - x)));
  }
};
GENERATE_KERNEL1(generate_exponential, lambda, user_uniform, user_exponential_functor(lambda))
struct user_cauchy_functor {
  const double _median;
  const double _sigma;
  user_cauchy_functor(double median, double sigma) restrict(amp,cpu) : _median(median), _sigma(sigma) {}
  inline double operator()(const float& x) const restrict(amp,cpu) {
    return (double)(_median + _sigma * Concurrency::precise_math::tan((double)M_PI*(x-0.5)));
  }
};
GENERATE_KERNEL2(generate_cauchy, median, sigma, user_uniform, user_cauchy_functor(median, sigma))

#endif
#undef GENERATE_KERNEL1
#undef GENERATE_KERNEL2

/* Separate kernel because curand_log_normal gets extra parameters. */
void generate_log_normal(THCState* state, int size, THGPUTensor *self_, double mean, double stddev)
{
  GPURandStateMtgp32* s = state->rngState->current_gen->gen_states;
  Concurrency::accelerator_view accl_view = state->deviceState->get_current_accelerator_view();
  Concurrency::array_view<float, 1> av_result = self_->get_array_view();
  user_log_normal_kernel(accl_view, s, av_result, mean, stddev);
}

#define NUM_BLOCKS min((int)DIVUP(size, BLOCK_SIZE), MAX_NUM_BLOCKS)
void THGPUTensor_uniform(THCState *state, THGPUTensor *self_, double a, double b)
{
  if (state->rngState->current_gen == NULL)
  {
    THError("Random number generators have not been initialized.");
  }
  THGPUTensor *self = THGPUTensor_newContiguous(state, self_);
  long size = THGPUTensor_nElement(state, self);

  generate_uniform(state, size, self, a, b);

  THGPUTensor_freeCopyTo(state, self, self_);
};

void THGPUTensor_bernoulli(THCState *state, THGPUTensor *self_, double p)
{
  if (state->rngState->current_gen == NULL)
  {
    THError("Random number generators have not been initialized.");
  }
  THGPUTensor *self = THGPUTensor_newContiguous(state, self_);
  long size = THGPUTensor_nElement(state, self);

  generate_bernoulli(state, size, self, p);

  THGPUTensor_freeCopyTo(state, self, self_);
};

void THGPUTensor_normal(THCState *state, THGPUTensor *self_, double mean, double stdv)
{
  if (state->rngState->current_gen == NULL)
  {
    THError("Random number generators have not been initialized.");
  }
  THGPUTensor *self = THGPUTensor_newContiguous(state, self_);
  long size = THGPUTensor_nElement(state, self);

  generate_normal(state, size, self, mean, stdv);

  THGPUTensor_freeCopyTo(state, self, self_);
};

void THGPUTensor_logNormal(THCState *state, THGPUTensor *self_, double mean, double stdv)
{
  if (state->rngState->current_gen == NULL)
  {
    THError("Random number generators have not been initialized.");
  }
  THGPUTensor *self = THGPUTensor_newContiguous(state, self_);
  long size = THGPUTensor_nElement(state, self);
  generate_log_normal(state, size, self, mean, stdv);

  THGPUTensor_freeCopyTo(state, self, self_);
};

void THGPUTensor_geometric(THCState *state, THGPUTensor *self_, double p)
{
  if (state->rngState->current_gen == NULL)
  {
    THError("Random number generators have not been initialized.");
  }
  THGPUTensor *self = THGPUTensor_newContiguous(state, self_);
  long size = THGPUTensor_nElement(state, self);

  generate_geometric(state, size, self, p);

  THGPUTensor_freeCopyTo(state, self, self_);
};

void THGPUTensor_exponential(THCState *state, THGPUTensor *self_, double lambda)
{
  if (state->rngState->current_gen == NULL)
  {
    THError("Random number generators have not been initialized.");
  }
  THGPUTensor *self = THGPUTensor_newContiguous(state, self_);
  long size = THGPUTensor_nElement(state, self);

  generate_exponential(state, size, self, lambda);

  THGPUTensor_freeCopyTo(state, self, self_);
};

void THGPUTensor_cauchy(THCState* state, THGPUTensor *self_, double median, double sigma)
{
  if (state->rngState->current_gen == NULL)
  {
    THError("Random number generators have not been initialized.");
  }
  THGPUTensor *self = THGPUTensor_newContiguous(state, self_);
  long size = THGPUTensor_nElement(state, self);

  generate_cauchy(state, size, self, median, sigma);

  THGPUTensor_freeCopyTo(state, self, self_);
};
#undef NUM_BLOCKS

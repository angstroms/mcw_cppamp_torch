#ifndef THC_STORAGE_COPY_INC
#define THC_STORAGE_COPY_INC

#include "THCStorage.h"
#include "THCGeneral.h"

/* Support for copy between different Storage types */

THC_API void THGPUStorage_rawCopy(THCState *state, THGPUStorage *storage, float *src);
THC_API void THGPUStorage_copy(THCState *state, THGPUStorage *storage, THGPUStorage *src);
THC_API void THGPUStorage_copyByte(THCState *state, THGPUStorage *storage, struct THByteStorage *src);
THC_API void THGPUStorage_copyChar(THCState *state, THGPUStorage *storage, struct THCharStorage *src);
THC_API void THGPUStorage_copyShort(THCState *state, THGPUStorage *storage, struct THShortStorage *src);
THC_API void THGPUStorage_copyInt(THCState *state, THGPUStorage *storage, struct THIntStorage *src);
THC_API void THGPUStorage_copyLong(THCState *state, THGPUStorage *storage, struct THLongStorage *src);
THC_API void THGPUStorage_copyFloat(THCState *state, THGPUStorage *storage, struct THFloatStorage *src);
THC_API void THGPUStorage_copyDouble(THCState *state, THGPUStorage *storage, struct THDoubleStorage *src);

THC_API void THByteStorage_copyGPU(THCState *state, THByteStorage *self, struct THGPUStorage *src);
THC_API void THCharStorage_copyGPU(THCState *state, THCharStorage *self, struct THGPUStorage *src);
THC_API void THShortStorage_copyGPU(THCState *state, THShortStorage *self, struct THGPUStorage *src);
THC_API void THIntStorage_copyGPU(THCState *state, THIntStorage *self, struct THGPUStorage *src);
THC_API void THLongStorage_copyGPU(THCState *state, THLongStorage *self, struct THGPUStorage *src);
THC_API void THFloatStorage_copyGPU(THCState *state, THFloatStorage *self, struct THGPUStorage *src);
THC_API void THDoubleStorage_copyGPU(THCState *state, THDoubleStorage *self, struct THGPUStorage *src);
THC_API void THGPUStorage_copyGPU(THCState *state, THGPUStorage *self, THGPUStorage *src);

#endif

#ifndef THC_TENSOR_INC
#define THC_TENSOR_INC

#include "THTensor.h"
#include "THCStorage.h"
#include "THCGeneral.h"
#include "bolt/amp/functional.h"
#include "bolt/amp/fill.h"
#include "bolt/amp/device_vector.h"
#include "bolt/amp/transform.h"
#include "bolt/amp/transform_reduce.h"
#include "bolt/amp/reduce.h"
#include "bolt/amp/inner_product.h"
#include "bolt/amp/copy.h"

#define TH_TENSOR_REFCOUNTED 1

typedef struct THGPUTensor
{
  long *size;
  long *stride;
  int nDimension;
  THGPUStorage *storage;
  long storageOffset;
  int refcount;
  char flag;

  // Function to return array_view associated with Tensor
  inline Concurrency::array_view<float,1> get_array_view()
  {
    Concurrency::array_view<float,1>* avPtr = static_cast<Concurrency::array_view<float>*>
                                                         (this->storage->allocatorContext);
    avPtr->discard_data(); // Don't need the host reference anymore as Device is up to date
    return *avPtr;
  }

  // Function to return a bolt device vector from the tensor
  bolt::amp::device_vector<float> get_bolt_dev_vec(THCState *state)
  {
    Concurrency::array_view<float,1>* avPtr = static_cast<Concurrency::array_view<float>*>
                                                         (this->storage->allocatorContext);

    bolt::amp::control &devControl(bolt::amp::control::getDefault());
    devControl.setAccelerator(state->deviceState->get_current_accelerator());
    bolt::amp::device_vector<float> dv_a(*avPtr, this->storage->size, true, devControl);

    return dv_a;
  }
  // Function to get ID of the device on which this Tensor resides
  int getDevice() {
    Concurrency::accelerator_view accl_view = get_accelerator_view();
    for (int i = 0; i < Concurrency::accelerator::get_all().size(); i ++) {
      if (accl_view.get_accelerator() == Concurrency::accelerator::get_all()[i])
        return i;
    }
    assert(1 && "not valid device ID");
    return 0;
  }

  // Function to return the source accelerator view. This will be used by get_array_view(THCState *state, )
  // method of  THGPUTensor to provide the array_view in the appropriate accelerator.
  inline Concurrency::accelerator_view get_accelerator_view()
  {
    Concurrency::array_view<float,1>* avPtr = static_cast<Concurrency::array_view<float>*>
                                                         (this->storage->allocatorContext);
    return avPtr->get_source_accelerator_view();
  }

} THGPUTensor;

/**** access methods ****/
THC_API THGPUStorage* THGPUTensor_storage(THCState *state, const THGPUTensor *self);
THC_API long THGPUTensor_storageOffset(THCState *state, const THGPUTensor *self);
THC_API int THGPUTensor_nDimension(THCState *state, const THGPUTensor *self);
THC_API long THGPUTensor_size(THCState *state, const THGPUTensor *self, int dim);
THC_API long THGPUTensor_stride(THCState *state, const THGPUTensor *self, int dim);
THC_API THLongStorage *THGPUTensor_newSizeOf(THCState *state, THGPUTensor *self);
THC_API THLongStorage *THGPUTensor_newStrideOf(THCState *state, THGPUTensor *self);
THC_API float *THGPUTensor_data(THCState *state, const THGPUTensor *self);

THC_API void THGPUTensor_setFlag(THCState *state, THGPUTensor *self, const char flag);
THC_API void THGPUTensor_clearFlag(THCState *state, THGPUTensor *self, const char flag);


/**** creation methods ****/
THC_API THGPUTensor *THGPUTensor_new(THCState *state);
THC_API THGPUTensor *THGPUTensor_newWithTensor(THCState *state, THGPUTensor *tensor);
/* stride might be NULL */
THC_API THGPUTensor *THGPUTensor_newWithStorage(THCState *state, THGPUStorage *storage_, long storageOffset_,
                                                THLongStorage *size_, THLongStorage *stride_);
THC_API THGPUTensor *THGPUTensor_newWithStorage1d(THCState *state, THGPUStorage *storage_, long storageOffset_,
                                long size0_, long stride0_);
THC_API THGPUTensor *THGPUTensor_newWithStorage2d(THCState *state, THGPUStorage *storage_, long storageOffset_,
                                long size0_, long stride0_,
                                long size1_, long stride1_);
THC_API THGPUTensor *THGPUTensor_newWithStorage3d(THCState *state, THGPUStorage *storage_, long storageOffset_,
                                long size0_, long stride0_,
                                long size1_, long stride1_,
                                long size2_, long stride2_);
THC_API THGPUTensor *THGPUTensor_newWithStorage4d(THCState *state, THGPUStorage *storage_, long storageOffset_,
                                long size0_, long stride0_,
                                long size1_, long stride1_,
                                long size2_, long stride2_,
                                long size3_, long stride3_);

/* stride might be NULL */
THC_API THGPUTensor *THGPUTensor_newWithSize(THCState *state, THLongStorage *size_, THLongStorage *stride_);
THC_API THGPUTensor *THGPUTensor_newWithSize1d(THCState *state, long size0_);
THC_API THGPUTensor *THGPUTensor_newWithSize2d(THCState *state, long size0_, long size1_);
THC_API THGPUTensor *THGPUTensor_newWithSize3d(THCState *state, long size0_, long size1_, long size2_);
THC_API THGPUTensor *THGPUTensor_newWithSize4d(THCState *state, long size0_, long size1_, long size2_, long size3_);

THC_API THGPUTensor *THGPUTensor_newClone(THCState *state, THGPUTensor *self);
THC_API THGPUTensor *THGPUTensor_newContiguous(THCState *state, THGPUTensor *tensor);
THC_API THGPUTensor *THGPUTensor_newSelect(THCState *state, THGPUTensor *tensor, int dimension_, long sliceIndex_);
THC_API THGPUTensor *THGPUTensor_newNarrow(THCState *state, THGPUTensor *tensor, int dimension_, long firstIndex_, long size_);
THC_API THGPUTensor *THGPUTensor_newTranspose(THCState *state, THGPUTensor *tensor, int dimension1_, int dimension2_);
THC_API THGPUTensor *THGPUTensor_newUnfold(THCState *state, THGPUTensor *tensor, int dimension_, long size_, long step_);

THC_API void THGPUTensor_resize(THCState *state, THGPUTensor *tensor, THLongStorage *size, THLongStorage *stride);
THC_API void THGPUTensor_resizeAs(THCState *state, THGPUTensor *tensor, THGPUTensor *src);
THC_API void THGPUTensor_resize1d(THCState *state, THGPUTensor *tensor, long size0_);
THC_API void THGPUTensor_resize2d(THCState *state, THGPUTensor *tensor, long size0_, long size1_);
THC_API void THGPUTensor_resize3d(THCState *state, THGPUTensor *tensor, long size0_, long size1_, long size2_);
THC_API void THGPUTensor_resize4d(THCState *state, THGPUTensor *tensor, long size0_, long size1_, long size2_, long size3_);
THC_API void THGPUTensor_resize5d(THCState *state, THGPUTensor *tensor, long size0_, long size1_, long size2_, long size3_, long size4_);

THC_API void THGPUTensor_set(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_setStorage(THCState *state, THGPUTensor *self, THGPUStorage *storage_, long storageOffset_,
                                    THLongStorage *size_, THLongStorage *stride_);
THC_API void THGPUTensor_setStorage1d(THCState *state, THGPUTensor *self, THGPUStorage *storage_, long storageOffset_,
                                    long size0_, long stride0_);
THC_API void THGPUTensor_setStorage2d(THCState *state, THGPUTensor *self, THGPUStorage *storage_, long storageOffset_,
                                    long size0_, long stride0_,
                                    long size1_, long stride1_);
THC_API void THGPUTensor_setStorage3d(THCState *state, THGPUTensor *self, THGPUStorage *storage_, long storageOffset_,
                                    long size0_, long stride0_,
                                    long size1_, long stride1_,
                                    long size2_, long stride2_);
THC_API void THGPUTensor_setStorage4d(THCState *state, THGPUTensor *self, THGPUStorage *storage_, long storageOffset_,
                                    long size0_, long stride0_,
                                    long size1_, long stride1_,
                                    long size2_, long stride2_,
                                    long size3_, long stride3_);

THC_API void THGPUTensor_narrow(THCState *state, THGPUTensor *self, THGPUTensor *src, int dimension_, long firstIndex_, long size_);
THC_API void THGPUTensor_select(THCState *state, THGPUTensor *self, THGPUTensor *src, int dimension_, long sliceIndex_);
THC_API void THGPUTensor_transpose(THCState *state, THGPUTensor *self, THGPUTensor *src, int dimension1_, int dimension2_);
THC_API void THGPUTensor_unfold(THCState *state, THGPUTensor *self, THGPUTensor *src, int dimension_, long size_, long step_);

THC_API void THGPUTensor_squeeze(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_squeeze1d(THCState *state, THGPUTensor *self, THGPUTensor *src, int dimension_);

THC_API int THGPUTensor_isContiguous(THCState *state, const THGPUTensor *self);
THC_API int THGPUTensor_isSameSizeAs(THCState *state, const THGPUTensor *self, const THGPUTensor *src);
THC_API long THGPUTensor_nElement(THCState *state, const THGPUTensor *self);

THC_API void THGPUTensor_retain(THCState *state, THGPUTensor *self);
THC_API void THGPUTensor_free(THCState *state, THGPUTensor *self);
THC_API void THGPUTensor_freeCopyTo(THCState *state, THGPUTensor *self, THGPUTensor *dst);

/* Slow access methods [check everything] */
THC_API void THGPUTensor_set1d(THCState *state, THGPUTensor *tensor, long x0, float value);
THC_API void THGPUTensor_set2d(THCState *state, THGPUTensor *tensor, long x0, long x1, float value);
THC_API void THGPUTensor_set3d(THCState *state, THGPUTensor *tensor, long x0, long x1, long x2, float value);
THC_API void THGPUTensor_set4d(THCState *state, THGPUTensor *tensor, long x0, long x1, long x2, long x3, float value);

THC_API float THGPUTensor_get1d(THCState *state, const THGPUTensor *tensor, long x0);
THC_API float THGPUTensor_get2d(THCState *state, const THGPUTensor *tensor, long x0, long x1);
THC_API float THGPUTensor_get3d(THCState *state, const THGPUTensor *tensor, long x0, long x1, long x2);
THC_API float THGPUTensor_get4d(THCState *state, const THGPUTensor *tensor, long x0, long x1, long x2, long x3);

THC_API int THGPUTensor_getDevice(THCState *state, THGPUTensor *self);

#endif

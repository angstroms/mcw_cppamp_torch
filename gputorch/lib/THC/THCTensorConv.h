#ifndef TH_GPU_TENSOR_CONV_INC
#define TH_GPU_TENSOR_CONV_INC

#include "THCTensor.h"

THC_API void THGPUTensor_conv2Dmv(THCState *state, THGPUTensor *output, float beta, THGPUTensor *input,
                                  THGPUTensor *kernel, long srow, long scol, const char *type);

THC_API void THGPUTensor_conv2Dmm(THCState *state, THGPUTensor *output, float beta, THGPUTensor *input,
                                  THGPUTensor *kernel, long srow, long scol, const char *type);

THC_API void THGPUTensor_conv2DRevger(THCState *state, THGPUTensor *output, float beta, float alpha,
                                      THGPUTensor *input, THGPUTensor *kernel,
                                      long srow, long scol);

THC_API void THGPUTensor_conv2DRevgerm(THCState *state, THGPUTensor *output, float beta, float alpha,
                                       THGPUTensor *input, THGPUTensor *kernel,
                                       long srow, long scol);

THC_API void THGPUTensor_conv2Dmap(THCState *state, THGPUTensor *output, THGPUTensor *input,
                                   THGPUTensor *kernel, long stride_x, long stride_y,
                                   THGPUTensor *table, long fanin);

#endif

#include "THCGeneral.h"
#include "TH.h"
#include "THCTensorRandom.h"

void THGPUInit(THCState* state)
{
  state->deviceState = (THCGPUDeviceState*)malloc(sizeof(THCGPUDeviceState));
  THCGPUDeviceState* device_state = state->deviceState;
  // Get all the available accelerators
  std::vector<Concurrency::accelerator> accs = Concurrency::accelerator::get_all();

  // Logic to get rid of CPU from the list of accelerators
  device_state->deviceCount = accs.size() - 1;
  device_state->accArr = new Concurrency::accelerator[device_state->deviceCount];
  int deviceId = 0;
  for (int i = 0; i < accs.size(); i++)
  {
    if (Concurrency::accelerator::cpu_accelerator != accs[i].get_device_path())
    {
      device_state->accArr[deviceId] = accs[i];
      deviceId++;
    }
  }
  //Choose the first accelerator as default.
  device_state->currentDeviceID = 1;

  state->rngState = (THCRNGState*)malloc(sizeof(THCRNGState));
  // Default device index is 0
  THCRandom_init(state, device_state->deviceCount, 0);
}

void THGPUShutdown(THCState *state)
{
  THCRandom_shutdown(state);
  delete[] state->deviceState->accArr;
  free(state->deviceState);
  state->deviceState = NULL;
}

void __THGPUCheck(int err, const char *file, const int line)
{
  if (err != 0)
  {
    THError("%s(%i) : GPU runtime error : %s",
            file, line, "");
  }
}


void THGPUGetGridSize(int *nBlockPerColumn_, int *nBlockPerRow_, int *nThreadPerBlock_, long size)
{
  const int nThreadPerBlock = 256;
  long nBlockPerGrid = size / nThreadPerBlock;
  long nBlockPerColumn = 0L;
  long nBlockPerRow = 0L;

  if (size % nThreadPerBlock)
    nBlockPerGrid++;

  if (nBlockPerGrid <= 65535)
  {
    nBlockPerRow = nBlockPerGrid;
    nBlockPerColumn = 1;
  }
  else if (nBlockPerGrid <= (65355L * 65355L))
  {
    unsigned int uiSqrt = (unsigned int)(sqrt((float)nBlockPerGrid));
    nBlockPerRow = uiSqrt;
    nBlockPerColumn = uiSqrt;
    while ((nBlockPerRow * nBlockPerColumn) < nBlockPerGrid)
      nBlockPerRow++;
  }
  else
    THError("too large vector for GPU, sorry");

  *nBlockPerColumn_ = (int)nBlockPerColumn;
  *nBlockPerRow_ = (int)nBlockPerRow;
  *nThreadPerBlock_ = (int)nThreadPerBlock;
}

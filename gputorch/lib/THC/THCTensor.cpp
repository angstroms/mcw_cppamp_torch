#include "THCGeneral.h"
#include "THCTensor.h"
#include "THCTensorCopy.h"

/**** access methods ****/
THGPUStorage *THGPUTensor_storage(THCState *state, const THGPUTensor *self)
{
  return self->storage;
}

long THGPUTensor_storageOffset(THCState *state, const THGPUTensor *self)
{
  return self->storageOffset;
}

int THGPUTensor_nDimension(THCState *state, const THGPUTensor *self)
{
  return self->nDimension;
}

long THGPUTensor_size(THCState *state, const THGPUTensor *self, int dim)
{
  THArgCheck((dim >= 0) && (dim < self->nDimension), 2, "out of range");
  return self->size[dim];
}

long THGPUTensor_stride(THCState *state, const THGPUTensor *self, int dim)
{
  THArgCheck((dim >= 0) && (dim < self->nDimension), 2, "out of range");
  return self->stride[dim];
}

THLongStorage *THGPUTensor_newSizeOf(THCState *state, THGPUTensor *self)
{
  THLongStorage *size = THLongStorage_newWithSize(self->nDimension);
  THLongStorage_rawCopy(size, self->size);
  return size;
}

THLongStorage *THGPUTensor_newStrideOf(THCState *state, THGPUTensor *self)
{
  THLongStorage *stride = THLongStorage_newWithSize(self->nDimension);
  THLongStorage_rawCopy(stride, self->stride);
  return stride;
}

float *THGPUTensor_data(THCState *state, const THGPUTensor *self)
{
  if(self->storage)
    return (self->storage->data+self->storageOffset);
  else
    return NULL;
}

void THGPUTensor_setFlag(THCState *state, THGPUTensor *self, const char flag)
{
  self->flag |= flag;
}

void THGPUTensor_clearFlag(THCState *state, THGPUTensor *self, const char flag)
{
  self->flag &= ~flag;
}

/**** creation methods ****/

static void THGPUTensor_rawInit(THCState *state, THGPUTensor *self);
static void THGPUTensor_rawSet(THCState *state, THGPUTensor *self, THGPUStorage *storage, long storageOffset, int nDimension, long *size, long *stride);
static void THGPUTensor_rawResize(THCState *state, THGPUTensor *self, int nDimension, long *size, long *stride);

/* Empty init */
THGPUTensor *THGPUTensor_new(THCState *state)
{
  THGPUTensor *self = (THGPUTensor*)THAlloc(sizeof(THGPUTensor));
  THGPUTensor_rawInit(state, self);
  return self;
}

/* Pointer-copy init */
THGPUTensor *THGPUTensor_newWithTensor(THCState *state, THGPUTensor *tensor)
{
  THGPUTensor *self = (THGPUTensor*)THAlloc(sizeof(THGPUTensor));
  THGPUTensor_rawInit(state, self);
  THGPUTensor_rawSet(state, self,
                     tensor->storage,
                     tensor->storageOffset,
                     tensor->nDimension,
                     tensor->size,
                     tensor->stride);
  return self;
}

/* Storage init */
THGPUTensor *THGPUTensor_newWithStorage(THCState *state, THGPUStorage *storage, long storageOffset, THLongStorage *size, THLongStorage *stride)
{
  THGPUTensor *self = (THGPUTensor*)THAlloc(sizeof(THGPUTensor));
  if (size && stride)
    THArgCheck(size->size == stride->size, 4, "inconsistent size");

  THGPUTensor_rawInit(state, self);
  THGPUTensor_rawSet(state, self,
                     storage,
                     storageOffset,
                     (size ? size->size : (stride ? stride->size : 0)),
                     (size ? size->data : NULL),
                     (stride ? stride->data : NULL));

  return self;
}
THGPUTensor *THGPUTensor_newWithStorage1d(THCState *state, THGPUStorage *storage, long storageOffset,
                                          long size0, long stride0)
{
  return THGPUTensor_newWithStorage4d(state, storage, storageOffset, size0, stride0, -1, -1,  -1, -1,  -1, -1);
}

THGPUTensor *THGPUTensor_newWithStorage2d(THCState *state, THGPUStorage *storage, long storageOffset,
                                          long size0, long stride0,
                                          long size1, long stride1)
{
  return THGPUTensor_newWithStorage4d(state, storage, storageOffset, size0, stride0, size1, stride1,  -1, -1,  -1, -1);
}

THGPUTensor *THGPUTensor_newWithStorage3d(THCState *state, THGPUStorage *storage, long storageOffset,
                                          long size0, long stride0,
                                          long size1, long stride1,
                                          long size2, long stride2)
{
  return THGPUTensor_newWithStorage4d(state, storage, storageOffset, size0, stride0, size1, stride1,  size2, stride2,  -1, -1);
}

THGPUTensor *THGPUTensor_newWithStorage4d(THCState *state, THGPUStorage *storage, long storageOffset,
                                          long size0, long stride0,
                                          long size1, long stride1,
                                          long size2, long stride2,
                                          long size3, long stride3)
{
  long size[4] = {size0, size1, size2, size3};
  long stride[4] = {stride0, stride1, stride2, stride3};

  THGPUTensor *self = (THGPUTensor*)THAlloc(sizeof(THGPUTensor));
  THGPUTensor_rawInit(state, self);
  THGPUTensor_rawSet(state, self, storage, storageOffset, 4, size, stride);

  return self;
}

THGPUTensor *THGPUTensor_newWithSize(THCState *state, THLongStorage *size, THLongStorage *stride)
{
  return THGPUTensor_newWithStorage(state, NULL, 0, size, stride);
}

THGPUTensor *THGPUTensor_newWithSize1d(THCState *state, long size0)
{
  return THGPUTensor_newWithSize4d(state, size0, -1, -1, -1);
}

THGPUTensor *THGPUTensor_newWithSize2d(THCState *state, long size0, long size1)
{
  return THGPUTensor_newWithSize4d(state, size0, size1, -1, -1);
}

THGPUTensor *THGPUTensor_newWithSize3d(THCState *state, long size0, long size1, long size2)
{
  return THGPUTensor_newWithSize4d(state, size0, size1, size2, -1);
}

THGPUTensor *THGPUTensor_newWithSize4d(THCState *state, long size0, long size1, long size2, long size3)
{
  long size[4] = {size0, size1, size2, size3};

  THGPUTensor *self = (THGPUTensor*)THAlloc(sizeof(THGPUTensor));
  THGPUTensor_rawInit(state, self);
  THGPUTensor_rawResize(state, self, 4, size, NULL);

  return self;
}

THGPUTensor *THGPUTensor_newClone(THCState *state, THGPUTensor *self)
{
  THGPUTensor *tensor = THGPUTensor_new(state);
  THGPUTensor_resizeAs(state, tensor, self);
  THGPUTensor_copy(state, tensor, self);
  return tensor;
}

THGPUTensor *THGPUTensor_newContiguous(THCState *state, THGPUTensor *self)
{
  if (!THGPUTensor_isContiguous(state, self))
    return THGPUTensor_newClone(state, self);
  else
  {
    THGPUTensor_retain(state, self);
    return self;
  }
}

THGPUTensor *THGPUTensor_newSelect(THCState *state, THGPUTensor *tensor, int dimension_, long sliceIndex_)
{
  THGPUTensor *self = THGPUTensor_newWithTensor(state, tensor);
  THGPUTensor_select(state, self, NULL, dimension_, sliceIndex_);
  return self;
}

THGPUTensor *THGPUTensor_newNarrow(THCState *state, THGPUTensor *tensor, int dimension_, long firstIndex_, long size_)
{
  THGPUTensor *self = THGPUTensor_newWithTensor(state, tensor);
  THGPUTensor_narrow(state, self, NULL, dimension_, firstIndex_, size_);
  return self;
}

THGPUTensor *THGPUTensor_newTranspose(THCState *state, THGPUTensor *tensor, int dimension1_, int dimension2_)
{
  THGPUTensor *self = THGPUTensor_newWithTensor(state, tensor);
  THGPUTensor_transpose(state, self, NULL, dimension1_, dimension2_);
  return self;
}

THGPUTensor *THGPUTensor_newUnfold(THCState *state, THGPUTensor *tensor, int dimension_, long size_, long step_)
{
  THGPUTensor *self = THGPUTensor_newWithTensor(state, tensor);
  THGPUTensor_unfold(state, self, NULL, dimension_, size_, step_);
  return self;
}

/* Resize */
void THGPUTensor_resize(THCState *state, THGPUTensor *self, THLongStorage *size, THLongStorage *stride)
{
  THArgCheck(size != NULL, 2, "invalid size");
  if (stride)
    THArgCheck(stride->size == size->size, 3, "invalid stride");

  THGPUTensor_rawResize(state, self, size->size, size->data, (stride ? stride->data : NULL));
}

void THGPUTensor_resizeAs(THCState *state, THGPUTensor *self, THGPUTensor *src)
{
  int isSame = 0;
  int d;
  if (self->nDimension == src->nDimension)
  {
    isSame = 1;
    for (d = 0; d < self->nDimension; d++)
    {
      if (self->size[d] != src->size[d])
      {
        isSame = 0;
        break;
      }
    }
  }

  if (!isSame)
    THGPUTensor_rawResize(state, self, src->nDimension, src->size, NULL);
}

void THGPUTensor_resize1d(THCState *state, THGPUTensor *tensor, long size0)
{
  THGPUTensor_resize4d(state, tensor, size0, -1, -1, -1);
}

void THGPUTensor_resize2d(THCState *state, THGPUTensor *tensor, long size0, long size1)
{
  THGPUTensor_resize4d(state, tensor, size0, size1, -1, -1);
}

void THGPUTensor_resize3d(THCState *state, THGPUTensor *tensor, long size0, long size1, long size2)
{
  THGPUTensor_resize4d(state, tensor, size0, size1, size2, -1);
}

void THGPUTensor_resize4d(THCState *state, THGPUTensor *self, long size0, long size1, long size2, long size3)
{
  long size[4] = {size0, size1, size2, size3};

  THGPUTensor_rawResize(state, self, 4, size, NULL);
}

void THGPUTensor_resize5d(THCState *state, THGPUTensor *self, long size0, long size1, long size2, long size3, long size4)
{
  long size[5] = {size0, size1, size2, size3, size4};
  THGPUTensor_rawResize(state, self, 5, size, NULL);
}

void THGPUTensor_set(THCState *state, THGPUTensor *self, THGPUTensor *src)
{
  if (self != src)
    THGPUTensor_rawSet(state, self,
                       src->storage,
                       src->storageOffset,
                       src->nDimension,
                       src->size,
                       src->stride);
}

void THGPUTensor_setStorage(THCState *state, THGPUTensor *self, THGPUStorage *storage_, long storageOffset_, THLongStorage *size_, THLongStorage *stride_)
{
  if (size_ && stride_)
    THArgCheck(size_->size == stride_->size, 5, "inconsistent size/stride sizes");

  THGPUTensor_rawSet(state, self,
                     storage_,
                     storageOffset_,
                     (size_ ? size_->size : (stride_ ? stride_->size : 0)),
                     (size_ ? size_->data : NULL),
                     (stride_ ? stride_->data : NULL));
}

void THGPUTensor_setStorage1d(THCState *state, THGPUTensor *self, THGPUStorage *storage_, long storageOffset_,
                              long size0_, long stride0_)
{
  THGPUTensor_setStorage4d(state, self, storage_, storageOffset_,
                           size0_, stride0_, -1, -1,
                           -1, -1, -1, -1);
}

void THGPUTensor_setStorage2d(THCState *state, THGPUTensor *self, THGPUStorage *storage_, long storageOffset_,
                              long size0_, long stride0_,
                              long size1_, long stride1_)
{
  THGPUTensor_setStorage4d(state, self, storage_, storageOffset_,
                           size0_, stride0_, size1_, stride1_,
                           -1, -1, -1, -1);
}

void THGPUTensor_setStorage3d(THCState *state, THGPUTensor *self, THGPUStorage *storage_, long storageOffset_,
                              long size0_, long stride0_,
                              long size1_, long stride1_,
                              long size2_, long stride2_)
{
  THGPUTensor_setStorage4d(state, self, storage_, storageOffset_,
                           size0_, stride0_,
                           size1_, stride1_,
                           size2_, stride2_,
                           -1, -1);
}

void THGPUTensor_setStorage4d(THCState *state, THGPUTensor *self, THGPUStorage *storage_, long storageOffset_,
                              long size0_, long stride0_,
                              long size1_, long stride1_,
                              long size2_, long stride2_,
                              long size3_, long stride3_)
{
  long size[4] = {size0_, size1_, size2_, size3_};
  long stride[4] = {stride0_, stride1_, stride2_, stride3_};

  THGPUTensor_rawSet(state, self, storage_, storageOffset_, 4, size, stride);
}


void THGPUTensor_narrow(THCState *state, THGPUTensor *self, THGPUTensor *src, int dimension, long firstIndex, long size)
{
  if (!src)
    src = self;

  THArgCheck((dimension >= 0) && (dimension < src->nDimension), 3, "out of range");
  THArgCheck((firstIndex >= 0) && (firstIndex < src->size[dimension]), 4, "out of range");
  THArgCheck((size > 0) && (firstIndex+size <= src->size[dimension]), 5, "out of range");

  THGPUTensor_set(state, self, src);

  if (firstIndex > 0)
    self->storageOffset += firstIndex * self->stride[dimension];

  self->size[dimension] = size;
}

void THGPUTensor_select(THCState *state, THGPUTensor *self, THGPUTensor *src, int dimension, long sliceIndex)
{
  int d;

  if (!src)
    src = self;

  THArgCheck(src->nDimension > 1, 1, "cannot select on a vector");
  THArgCheck((dimension >= 0) && (dimension < src->nDimension), 3, "out of range");
  THArgCheck((sliceIndex >= 0) && (sliceIndex < src->size[dimension]), 4, "out of range");

  THGPUTensor_set(state, self, src);
  THGPUTensor_narrow(state, self, NULL, dimension, sliceIndex, 1);
  for (d = dimension; d < self->nDimension-1; d++)
  {
    self->size[d] = self->size[d + 1];
    self->stride[d] = self->stride[d + 1];
  }
  self->nDimension--;
}

void THGPUTensor_transpose(THCState *state, THGPUTensor *self, THGPUTensor *src, int dimension1, int dimension2)
{
  long z;

  if (!src)
    src = self;

  THArgCheck((dimension1 >= 0) && (dimension1 < src->nDimension), 1, "out of range");
  THArgCheck((dimension2 >= 0) && (dimension2 < src->nDimension), 2, "out of range");

  THGPUTensor_set(state, self, src);

  if (dimension1 == dimension2)
    return;

  z = self->stride[dimension1];
  self->stride[dimension1] = self->stride[dimension2];
  self->stride[dimension2] = z;
  z = self->size[dimension1];
  self->size[dimension1] = self->size[dimension2];
  self->size[dimension2] = z;
}

void THGPUTensor_unfold(THCState *state, THGPUTensor *self, THGPUTensor *src, int dimension, long size, long step)
{
  long *newSize;
  long *newStride;
  int d;

  if (!src)
    src = self;

  THArgCheck((src->nDimension > 0), 1, "cannot unfold an empty tensor");
  THArgCheck(dimension < src->nDimension, 2, "out of range");
  THArgCheck(size <= src->size[dimension], 3, "out of range");
  THArgCheck(step > 0, 4, "invalid step");

  THGPUTensor_set(state, self, src);

  newSize = (long*)THAlloc(sizeof(long)*(self->nDimension+1));
  newStride = (long*)THAlloc(sizeof(long)*(self->nDimension+1));

  newSize[self->nDimension] = size;
  newStride[self->nDimension] = self->stride[dimension];
  for (d = 0; d < self->nDimension; d++)
  {
    if (d == dimension)
    {
      newSize[d] = (self->size[d] - size) / step + 1;
      newStride[d] = step * self->stride[d];
    }
    else
    {
      newSize[d] = self->size[d];
      newStride[d] = self->stride[d];
    }
  }

  THFree(self->size);
  THFree(self->stride);

  self->size = newSize;
  self->stride = newStride;
  self->nDimension++;
}

/* we have to handle the case where the result is a number */
void THGPUTensor_squeeze(THCState *state, THGPUTensor *self, THGPUTensor *src)
{
  int ndim = 0;
  int d;

  if (!src)
    src = self;

  THGPUTensor_set(state, self, src);

  for (d = 0; d < src->nDimension; d++)
  {
    if (src->size[d] != 1)
    {
      if (d != ndim)
      {
        self->size[ndim] = src->size[d];
        self->stride[ndim] = src->stride[d];
      }
      ndim++;
    }
  }

  /* right now, we do not handle 0-dimension tensors */
  if (ndim == 0 && src->nDimension > 0)
  {
    self->size[0] = 1;
    self->stride[0] = 1;
    ndim = 1;
  }
  self->nDimension = ndim;
}

void THGPUTensor_squeeze1d(THCState *state, THGPUTensor *self, THGPUTensor *src, int dimension)
{
  int d;

  if (!src)
    src = self;

  THArgCheck(dimension < src->nDimension, 3, "dimension out of range");

  THGPUTensor_set(state, self, src);

  if (src->size[dimension] == 1 && src->nDimension > 1)
  {
    for (d = dimension; d < self->nDimension-1; d++)
    {
      self->size[d] = self->size[d+1];
      self->stride[d] = self->stride[d+1];
    }
    self->nDimension--;
  }
}

int THGPUTensor_isContiguous(THCState *state, const THGPUTensor *self)
{
  long z = 1;
  int d;
  for (d = self->nDimension - 1; d >= 0; d--)
  {
    if (self->size[d] != 1)
    {
      if (self->stride[d] == z)
        z *= self->size[d];
      else
        return 0;
    }
  }
  return 1;
}

int THGPUTensor_isSameSizeAs(THCState *state, const THGPUTensor *self, const THGPUTensor* src)
{
  int d;
  if (self->nDimension != src->nDimension)
    return 0;
  for (d = 0; d < self->nDimension; ++d)
  {
    if (self->size[d] != src->size[d])
      return 0;
  }
  return 1;
}

long THGPUTensor_nElement(THCState *state, const THGPUTensor *self)
{
  if (self->nDimension == 0)
    return 0;
  else
  {
    long nElement = 1;
    int d;
    for (d = 0; d < self->nDimension; d++)
      nElement *= self->size[d];
    return nElement;
  }
}

void THGPUTensor_retain(THCState *state, THGPUTensor *self)
{
  if (self->flag & TH_TENSOR_REFCOUNTED)
    ++self->refcount;
}

void THGPUTensor_free(THCState *state, THGPUTensor *self)
{
  if (!self)
    return;

  if (self->flag & TH_TENSOR_REFCOUNTED)
  {
    if (--self->refcount == 0)
    {
      THFree(self->size);
      THFree(self->stride);
      if (self->storage)
        THGPUStorage_free(state, self->storage);
      THFree(self);
    }
  }
}

void THGPUTensor_freeCopyTo(THCState *state, THGPUTensor *self, THGPUTensor *dst)
{
  if (self != dst)
    THGPUTensor_copy(state, dst, self);

  THGPUTensor_free(state, self);
}

static void THGPUTensor_rawInit(THCState *state, THGPUTensor *self)
{
  self->refcount = 1;
  self->storage = NULL;
  self->storageOffset = 0;
  self->size = NULL;
  self->stride = NULL;
  self->nDimension = 0;
  self->flag = TH_TENSOR_REFCOUNTED;
}

static void THGPUTensor_rawSet(THCState *state, THGPUTensor *self, THGPUStorage *storage, long storageOffset, int nDimension, long *size, long *stride)
{
  /* storage */
  if (self->storage != storage)
  {
    if (self->storage)
      THGPUStorage_free(state, self->storage);

    if (storage)
    {
      self->storage = storage;
      THGPUStorage_retain(state, self->storage);
    }
    else
      self->storage = NULL;
  }

  /* storageOffset */
  if (storageOffset < 0)
    THError("Tensor: invalid storage offset");
  self->storageOffset = storageOffset;

  /* size and stride */
  THGPUTensor_rawResize(state, self, nDimension, size, stride);
}

static void THGPUTensor_rawResize(THCState *state, THGPUTensor *self, int nDimension, long *size, long *stride)
{
  int d;
  int nDimension_;
  long totalSize;
  int hascorrectsize = 1;

  nDimension_ = 0;
  for (d = 0; d < nDimension; d++)
  {
    if (size[d] > 0)
    {
      nDimension_++;
      if ((self->nDimension > d) && (size[d] != self->size[d]))
        hascorrectsize = 0;

      if ((self->nDimension > d) && stride && (stride[d] >= 0) && (stride[d] != self->stride[d]))
        hascorrectsize = 0;
    }
    else
      break;
  }
  nDimension = nDimension_;

  if (nDimension != self->nDimension)
    hascorrectsize = 0;

  if (hascorrectsize)
    return;

  if (nDimension > 0)
  {
    if (nDimension != self->nDimension)
    {
      self->size = (long*)THRealloc(self->size, sizeof(long)*nDimension);
      self->stride = (long*)THRealloc(self->stride, sizeof(long)*nDimension);
      self->nDimension = nDimension;
    }

    totalSize = 1;
    for (d = self->nDimension - 1; d >= 0; d--)
    {
      self->size[d] = size[d];
      if(stride && (stride[d] >= 0))
        self->stride[d] = stride[d];
      else
      {
        if (d == self->nDimension - 1)
          self->stride[d] = 1;
        else
          self->stride[d] = self->size[d + 1]*self->stride[d + 1];
      }
      totalSize += (self->size[d] - 1)*self->stride[d];
    }

    if (totalSize+self->storageOffset > 0)
    {
      if (!self->storage)
        self->storage = THGPUStorage_new(state);
      if (totalSize+self->storageOffset > self->storage->size)
        THGPUStorage_resize(state, self->storage, totalSize+self->storageOffset);
    }
  }
  else
    self->nDimension = 0;
}

void THGPUTensor_set1d(THCState *state, THGPUTensor *tensor, long x0, float value)
{
  THArgCheck(tensor->nDimension == 1, 1, "tensor must have one dimension");
  THArgCheck((x0 >= 0) && (x0 < tensor->size[0]), 2, "out of range");
  THGPUStorage_set(state, tensor->storage, tensor->storageOffset + x0 * tensor->stride[0], value);
}

float THGPUTensor_get1d(THCState *state, const THGPUTensor *tensor, long x0)
{
  THArgCheck(tensor->nDimension == 1, 1, "tensor must have one dimension");
  THArgCheck((x0 >= 0) && (x0 < tensor->size[0]), 2, "out of range");
  return THGPUStorage_get(state, tensor->storage, tensor->storageOffset+x0*tensor->stride[0]);
}

void THGPUTensor_set2d(THCState *state, THGPUTensor *tensor, long x0, long x1, float value)
{
  THArgCheck(tensor->nDimension == 2, 1, "tensor must have two dimensions");
  THArgCheck((x0 >= 0) && (x0 < tensor->size[0]) && (x1 >= 0) && (x1 < tensor->size[1]), 2, "out of range");
  THGPUStorage_set(state, tensor->storage, tensor->storageOffset + x0 * tensor->stride[0] + x1 * tensor->stride[1], value);
}

float THGPUTensor_get2d(THCState *state, const THGPUTensor *tensor, long x0, long x1)
{
  THArgCheck(tensor->nDimension == 2, 1, "tensor must have two dimensions");
  THArgCheck((x0 >= 0) && (x0 < tensor->size[0]) && (x1 >= 0) && (x1 < tensor->size[1]), 2, "out of range");
  return THGPUStorage_get(state, tensor->storage, tensor->storageOffset + x0 * tensor->stride[0] + x1 * tensor->stride[1]);
}

void THGPUTensor_set3d(THCState *state, THGPUTensor *tensor, long x0, long x1, long x2, float value)
{
  THArgCheck(tensor->nDimension == 3, 1, "tensor must have three dimensions");
  THArgCheck((x0 >= 0) && (x0 < tensor->size[0]) && (x1 >= 0) && (x1 < tensor->size[1]) && (x2 >= 0) && (x2 < tensor->size[2]), 2, "out of range");
  THGPUStorage_set(state, tensor->storage, tensor->storageOffset + x0 * tensor->stride[0] + x1 * tensor->stride[1] + x2 * tensor->stride[2], value);
}

float THGPUTensor_get3d(THCState *state, const THGPUTensor *tensor, long x0, long x1, long x2)
{
  THArgCheck(tensor->nDimension == 3, 1, "tensor must have three dimensions");
  THArgCheck((x0 >= 0) && (x0 < tensor->size[0]) && (x1 >= 0) && (x1 < tensor->size[1]) && (x2 >= 0) && (x2 < tensor->size[2]), 2, "out of range");
  return THGPUStorage_get(state, tensor->storage, tensor->storageOffset + x0 * tensor->stride[0] + x1 * tensor->stride[1] + x2 * tensor->stride[2]);
}

void THGPUTensor_set4d(THCState *state, THGPUTensor *tensor, long x0, long x1, long x2, long x3, float value)
{
  THArgCheck(tensor->nDimension == 4, 1, "tensor must have four dimensions");
  THArgCheck((x0 >= 0) && (x0 < tensor->size[0]) && (x1 >= 0) && (x1 < tensor->size[1]) && (x2 >= 0) && (x2 < tensor->size[2]) && (x3 >= 0) && ( x3 < tensor->size[3]), 2, "out of range");
  THGPUStorage_set(state, tensor->storage, tensor->storageOffset + x0 * tensor->stride[0] + x1 * tensor->stride[1] + x2 * tensor->stride[2] + x3 * tensor->stride[3], value);
}

float THGPUTensor_get4d(THCState *state, const THGPUTensor *tensor, long x0, long x1, long x2, long x3)
{
  THArgCheck(tensor->nDimension == 4, 1, "tensor must have four dimensions");
  THArgCheck((x0 >= 0) && (x0 < tensor->size[0]) && (x1 >= 0) && (x1 < tensor->size[1]) && (x2 >= 0) && (x2 < tensor->size[2]) && (x3 >= 0) && ( x3 < tensor->size[3]), 2, "out of range");
  return THGPUStorage_get(state, tensor->storage, tensor->storageOffset + x0 * tensor->stride[0] + x1 * tensor->stride[1] + x2 * tensor->stride[2]+x3*tensor->stride[3]);
}

THC_API int THGPUTensor_getDevice(THCState* state, THGPUTensor* thc) 
{
  if (!thc->storage) 
     return 0;
  return thc->getDevice();
}

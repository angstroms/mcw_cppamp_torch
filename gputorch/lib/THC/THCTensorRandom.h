#ifndef TH_GPU_TENSOR_RANDOM_INC
#define TH_GPU_TENSOR_RANDOM_INC

#include "THCTensor.h"
#include <MTGP/gpurand_mtgp32.h>
#include <random>

/* Generator */
typedef struct _Generator {
  struct GPURandStateMtgp32* gen_states;
  int initf;
  unsigned long initial_seed;
} Generator;

typedef struct THCRNGState {
  /* One generator per GPU */
  Generator* gen;
  Generator* current_gen;
  int num_devices;
} THCRNGState;

struct THCState;

THC_API void THCRandom_init(struct THCState *state, int num_devices, int current_device);
THC_API void THCRandom_shutdown(struct THCState *state);
/* device: the device index starting from 0 */
THC_API void THCRandom_setGenerator(struct THCState *state, int device);
THC_API void THCRandom_resetGenerator(struct THCState *state);
THC_API unsigned long THCRandom_seed(struct THCState *state);
THC_API unsigned long THCRandom_seedAll(struct THCState *state);
THC_API void THCRandom_manualSeed(struct THCState *state, unsigned long the_seed_);
THC_API void THCRandom_manualSeedAll(struct THCState *state, unsigned long the_seed_);
THC_API unsigned long THCRandom_initialSeed(struct THCState *state);
THC_API void THCRandom_getRNGState(struct THCState *state, THByteTensor *rng_state);
THC_API void THCRandom_setRNGState(struct THCState *state, THByteTensor *rng_state);
THC_API void THGPUTensor_geometric(struct THCState *state, THGPUTensor *self, double p);
THC_API void THGPUTensor_bernoulli(struct THCState *state, THGPUTensor *self, double p);
THC_API void THGPUTensor_uniform(struct THCState *state, THGPUTensor *self, double a, double b);
THC_API void THGPUTensor_normal(struct THCState *state, THGPUTensor *self, double mean, double stdv);
THC_API void THGPUTensor_exponential(struct THCState *state, THGPUTensor *self, double lambda);
THC_API void THGPUTensor_cauchy(struct THCState *state, THGPUTensor *self, double median, double sigma);
THC_API void THGPUTensor_logNormal(struct THCState *state, THGPUTensor *self, double mean, double stdv);

#endif


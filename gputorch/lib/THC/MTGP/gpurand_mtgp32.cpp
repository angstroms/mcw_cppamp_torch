#include <amp.h>
#include "gpurand_mtgp32.h"

using namespace Concurrency;

//#define DEBUG_GPURAND
#ifdef DEBUG_GPURAND
#include <iostream>
template<typename T>
void VerifyData(T expected, T actual, const char* s) {
  if (expected != actual) {
    std::cout << s << "  expected=" << expected << "  actual="<<actual<<std::endl;
    exit(1);
  }
}
// Host APIs
/**
 * This function initializes the internal state array with a 32-bit
 * integer seed. The allocated memory should be freed by calling
 * mtgp32_free(). \b para should be one of the elements in the
 * parameter table (mtgp32-param-ref.c).
 *
 * @param[out] array MTGP internal status vector.
 * @param[in] para parameter structure
 * @param[in] seed a 32-bit integer used as the seed.
 */
void mtgp32_init_state(uint32_t array[],
		      const mtgp32_params_fast_t *para, uint32_t seed) {
    int i;
    int size = para->mexp / 32 + 1;
    uint32_t hidden_seed;
    uint32_t tmp;
    hidden_seed = para->tbl[4] ^ (para->tbl[8] << 16);
    tmp = hidden_seed;
    tmp += tmp >> 16;
    tmp += tmp >> 8;
    memset(array, tmp & 0xff, sizeof(uint32_t) * size);
    array[0] = seed;
    array[1] = hidden_seed;
    for (i = 1; i < size; i++) {
	array[i] ^= UINT32_C(1812433253) * (array[i - 1]
					    ^ (array[i - 1] >> 30))
	    + i;
    }
    for (i = size; i < MTGP32_STATE_SIZE; i++)
      array[i] = 0;
}
#endif


// C-style
void GPURandStateMtgp32_init(Concurrency::accelerator_view accl_view,
                             GPURandStateMtgp32* s)
{
  // kernel params
  Concurrency::array<uint32_t, 2> arrP = Concurrency::array<uint32_t, 2>(Concurrency::extent<2>(GPURAND_GROUP_NUM, MTGP32_TS), accl_view);
  s->param_tbl = new Concurrency::array_view<uint32_t, 2>(arrP);
  Concurrency::array<uint32_t, 2> arrT = Concurrency::array<uint32_t, 2>(Concurrency::extent<2>(GPURAND_GROUP_NUM, MTGP32_TS), accl_view);
  s->temper_tbl = new Concurrency::array_view<uint32_t, 2>(arrT);
  Concurrency::array<uint32_t, 2> arrS = Concurrency::array<uint32_t, 2>(Concurrency::extent<2>(GPURAND_GROUP_NUM, MTGP32_TS), accl_view);
  s->single_temper_tbl = new Concurrency::array_view<uint32_t, 2>(arrS);
  Concurrency::array<uint32_t, 1> arrPos = Concurrency::array<uint32_t, 1>(Concurrency::extent<1>(GPURAND_GROUP_NUM), accl_view);
  s->pos_tbl = new Concurrency::array_view<uint32_t, 1>(arrPos);
  Concurrency::array<uint32_t, 1> arrSh1 = Concurrency::array<uint32_t, 1>(Concurrency::extent<1>(GPURAND_GROUP_NUM), accl_view);
  s->sh1_tbl = new Concurrency::array_view<uint32_t, 1>(arrSh1);
  Concurrency::array<uint32_t, 1> arrSh2 = Concurrency::array<uint32_t, 1>(Concurrency::extent<1>(GPURAND_GROUP_NUM), accl_view);
  s->sh2_tbl = new Concurrency::array_view<uint32_t, 1>(arrSh2);
  Concurrency::array<uint32_t, 1> arrMask= Concurrency::array<uint32_t, 1>(Concurrency::extent<1>(1), accl_view);
  s->mask = new Concurrency::array_view<uint32_t, 1>(arrMask);
  // Redundant member
  Concurrency::array<uint32_t, 1> arrMexp = Concurrency::array<uint32_t, 1>(Concurrency::extent<1>(GPURAND_GROUP_NUM), accl_view);
  s->mexp_tbl = new Concurrency::array_view<uint32_t, 1>(arrMexp);

  // states
  Concurrency::array<uint32_t, 2> arrStatus = Concurrency::array<uint32_t, 2>(Concurrency::extent<2>(USER_GROUP_NUM, MTGP32_STATE_SIZE), accl_view);
  s->d_status = new Concurrency::array_view<uint32_t, 2>(arrStatus);
  Concurrency::array<uint32_t, 1> arrOffset = Concurrency::array<uint32_t, 1>(Concurrency::extent<1>(USER_GROUP_NUM), accl_view);
  s->offset = new Concurrency::array_view<uint32_t, 1>(arrOffset);
  Concurrency::array<uint32_t, 1> arrIndex = Concurrency::array<uint32_t, 1>(Concurrency::extent<1>(USER_GROUP_NUM), accl_view);
  s->index = new Concurrency::array_view<uint32_t, 1>(arrIndex);
}

#define FREE_MEMBER(s, member) \
  if (s->member) {             \
    s->member->~array_view();  \
    delete s->member;          \
  }
// TODO: Big memory leak and cause device buffer allocation fail if exits abnormally
void GPURandStateMtgp32_release(GPURandStateMtgp32* s)
{
  FREE_MEMBER(s, param_tbl);
  FREE_MEMBER(s, temper_tbl);
  FREE_MEMBER(s, single_temper_tbl);
  FREE_MEMBER(s, pos_tbl);
  FREE_MEMBER(s, sh1_tbl);
  FREE_MEMBER(s, sh2_tbl);
  FREE_MEMBER(s, mask);
  FREE_MEMBER(s, mexp_tbl);

  FREE_MEMBER(s, d_status);
  FREE_MEMBER(s, offset);
  FREE_MEMBER(s, index);
}

void GPURandStateMtgp32_copy_D2H(void* src, void* dst)
{
  HOSTRandStateMtgp32* host = (HOSTRandStateMtgp32*)(dst);
  GPURandStateMtgp32* gpu = (GPURandStateMtgp32*)(src);
  Concurrency::copy(*(gpu->offset), host->offset);
  Concurrency::copy(*(gpu->index), host->index);
  Concurrency::copy(*(gpu->d_status), host->d_status);

  Concurrency::copy(*(gpu->mexp_tbl), host->mexp_tbl);
  Concurrency::copy(*(gpu->param_tbl), host->param_tbl);
  Concurrency::copy(*(gpu->temper_tbl), host->temper_tbl);
  Concurrency::copy(*(gpu->single_temper_tbl), host->single_temper_tbl);
  Concurrency::copy(*(gpu->pos_tbl), host->pos_tbl);
  Concurrency::copy(*(gpu->sh1_tbl), host->sh1_tbl);
  Concurrency::copy(*(gpu->sh2_tbl), host->sh2_tbl);
  Concurrency::copy(*(gpu->mask), host->mask);
}
void GPURandStateMtgp32_copy_H2D(void* src, void* dst)
{
  HOSTRandStateMtgp32* host = (HOSTRandStateMtgp32*)(src);
  GPURandStateMtgp32* gpu = (GPURandStateMtgp32*)(dst);
  Concurrency::copy(host->offset, *(gpu->offset));
  Concurrency::copy(host->index, *(gpu->index));
  Concurrency::copy(host->d_status, *(gpu->d_status));

  Concurrency::copy(host->param_tbl, *(gpu->param_tbl));
  Concurrency::copy(host->temper_tbl, *(gpu->temper_tbl));
  Concurrency::copy(host->single_temper_tbl, *(gpu->single_temper_tbl));
  Concurrency::copy(host->pos_tbl, *(gpu->pos_tbl));
  Concurrency::copy(host->sh1_tbl, *(gpu->sh1_tbl));
  Concurrency::copy(host->sh2_tbl, *(gpu->sh2_tbl));
  Concurrency::copy(host->mask, *(gpu->mask));
}


// The following are device APIs

// Copy param constants onto device
int mtgp32_init_params_kernel(Concurrency::accelerator_view accl_view,
                       mtgp32_params_fast_t * params,
                       GPURandStateMtgp32* s)
{
  Concurrency::array_view<uint32_t, 2> av_param_tbl = *(s->param_tbl);
  Concurrency::array_view<uint32_t, 2> av_temper_tbl = *(s->temper_tbl);
  Concurrency::array_view<uint32_t, 2> av_single_temper_tbl = *(s->single_temper_tbl);
  Concurrency::array_view<uint32_t, 1> av_pos_tbl = *(s->pos_tbl);
  Concurrency::array_view<uint32_t, 1> av_sh1_tbl = *(s->sh1_tbl);
  Concurrency::array_view<uint32_t, 1> av_sh2_tbl = *(s->sh2_tbl);
  Concurrency::array_view<uint32_t, 1> av_mask = *(s->mask);
  Concurrency::array_view<uint32_t, 1> av_mexp_tbl = *(s->mexp_tbl);

  // Prepare data source
  uint32_t vec_param[GPURAND_GROUP_NUM * MTGP32_TS];
  uint32_t vec_temper[GPURAND_GROUP_NUM * MTGP32_TS];
  uint32_t vec_single_temper[GPURAND_GROUP_NUM * MTGP32_TS];
  uint32_t vec_pos[GPURAND_GROUP_NUM];
  uint32_t vec_sh1[GPURAND_GROUP_NUM];
  uint32_t vec_sh2[GPURAND_GROUP_NUM];
  uint32_t vec_mexp[GPURAND_GROUP_NUM];

  for (int i = 0; i < GPURAND_GROUP_NUM; i++) {
    vec_pos[i] = params[i].pos;
    vec_sh1[i] = params[i].sh1;
    vec_sh2[i] = params[i].sh2;
    vec_mexp[i] = params[i].mexp;
    for (int j = 0; j < MTGP32_TS; j++) {
      vec_param[i * MTGP32_TS + j] = params[i].tbl[j];
      vec_temper[i * MTGP32_TS + j] = params[i].tmp_tbl[j];
      vec_single_temper[i * MTGP32_TS + j] = params[i].flt_tmp_tbl[j];
    }
  }

  // Ugly codes to populate array_views
  //av_mask[0] = params[0].mask;
  Concurrency::copy(&params[0].mask, av_mask);
  Concurrency::copy(vec_param, av_param_tbl);
  Concurrency::copy(vec_temper, av_temper_tbl);
  Concurrency::copy(vec_single_temper, av_single_temper_tbl);
  Concurrency::copy(vec_pos, av_pos_tbl);
  Concurrency::copy(vec_sh1, av_sh1_tbl);
  Concurrency::copy(vec_sh2, av_sh2_tbl);
  Concurrency::copy(vec_mexp, av_mexp_tbl);

#ifdef DEBUG_GPURAND
  for (int i = 0; i < GPURAND_GROUP_NUM; i++) {
    VerifyData(vec_pos[i], av_pos_tbl[i], "pos table");
    VerifyData(vec_sh1[i], av_sh1_tbl[i], "sh1 table");
    VerifyData(vec_sh2[i], av_sh2_tbl[i], "sh2 table");
    VerifyData(vec_mexp[i], av_mexp_tbl[i], "mexp table");
    for (int j = 0; j < MTGP32_TS; j++) {
      VerifyData(vec_param[i * MTGP32_TS + j], av_param_tbl[i][j], "param table");
      VerifyData(vec_temper[i * MTGP32_TS + j],av_temper_tbl[i][j], "temper table");
      VerifyData(vec_single_temper[i * MTGP32_TS + j], av_single_temper_tbl[i][j], "single_temper table");
    }
  }
  VerifyData(params[0].mask, av_mask[0], "mask");
#endif
  return 0;
}

// Initialize GPURandStateMtgp32 by seed
int mtgp32_init_seed_kernel(Concurrency::accelerator_view accl_view,
                            GPURandStateMtgp32* s, unsigned long seed)
{
  seed = seed ^ (seed >> 32);
  int nGroups = USER_GROUP_NUM;
  Concurrency::array_view<uint32_t, 2> &av_param_tbl = *(s->param_tbl);
  Concurrency::array_view<uint32_t, 1> &av_offset = *(s->offset);
  Concurrency::array_view<uint32_t, 1> &av_index = *(s->index);
  Concurrency::array_view<uint32_t, 1> &av_mexp_tbl = *(s->mexp_tbl);

  Concurrency::array_view<uint32_t, 2> &av_d_status = *(s->d_status);
  Concurrency::extent<1> ext(nGroups * MTGP32_TN);
  Concurrency::tiled_extent<256> t_ext(ext);
  av_param_tbl.discard_data();
  av_mexp_tbl.discard_data();
  av_index.discard_data();
  av_offset.discard_data();
  av_d_status.discard_data();
  Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<MTGP32_TN> tidx) restrict(amp)
  {
    const int groupId = tidx.tile[0];
    if (groupId >= nGroups)
      return;
    const int threadId = tidx.local[0];
    tile_static unsigned int status[MTGP32_STATE_SIZE];
    uint32_t mexp = av_mexp_tbl[groupId];
    int size = mexp / 32 + 1;
    // Initialize state
    {
      int i;
      uint32_t hidden_seed;
      uint32_t tmp;
      hidden_seed = av_param_tbl[groupId][4] ^ (av_param_tbl[groupId][8] << 16);
      tmp = hidden_seed;
      tmp += tmp >> 16;
      tmp += tmp >> 8;
      tmp = tmp & 0xff;
      tmp |= tmp << 8;
      tmp |= tmp << 16;
      status[threadId] = tmp;
      if ((size < MTGP32_STATE_SIZE) && (threadId < size - MTGP32_TN)) {
        status[MTGP32_TN + threadId] = tmp;
      }
      tidx.barrier.wait_with_tile_static_memory_fence();
      if (threadId == 0) {
        status[0] = seed + 1 + groupId;
        status[1] = hidden_seed;
        for (i = 1; i < size; i++) {
          status[i] ^= UINT32_C(1812433253) * (status[i - 1] ^ (status[i - 1] >> 30)) + i;
          av_d_status[groupId][i] = status[i];
        }
        #ifdef DEBUG_GPURAND
        for (i = size; i < MTGP32_STATE_SIZE; i++) {
          av_d_status[groupId][i] = 0;
        }
        #endif
        av_offset[groupId] = 0;
        av_index[groupId] = groupId;
        av_d_status[groupId][0] = seed + 1 + groupId;
      }
    }
  });

#ifdef DEBUG_GPURAND
{
  av_param_tbl.synchronize();
  av_offset.synchronize();
  av_index.synchronize();
  av_mexp_tbl.synchronize();
  av_d_status.synchronize();
  
  for (int i = 0; i < USER_GROUP_NUM; i++) {
    VerifyData((uint32_t)0, av_offset[i], "offset table");
    VerifyData((uint32_t)i, av_index[i], "index table");
  }

  uint32_t host_status[USER_GROUP_NUM][MTGP32_STATE_SIZE];
  for (int i = 0; i < USER_GROUP_NUM; i++)
    mtgp32_init_state(&host_status[i][0], &mtgp32_params_fast_11213[i], seed + i + 1);

  for (int i = 0; i < USER_GROUP_NUM; i++) {
    for (int j = 0; j < MTGP32_STATE_SIZE; j++) {
      VerifyData(host_status[i][j], av_d_status[i][j], "rand status");
    }
  }
}
#endif
  return 0;
}

#pragma once

// FIXME: Better to inline every amp-restricted functions and make it static
#include <amp.h>
#include <amp_math.h>
#include "mtgp32-fast.h"

using namespace Concurrency;

#define MTGP32_MEXP 11213
#define MTGP32_N 351
#define MTGP32_FLOOR_2P 256
#define MTGP32_CEIL_2P 512
#define MTGP32_TN MTGP32_FLOOR_2P // TBL_NUMBER, do not exceed 256, i.e. WAVEFRONT_SIZE
#define MTGP32_LS (MTGP32_TN * 3)
#define MTGP32_TS 16  // TBL_SIZE
#define GPURAND_GROUP_NUM 200  // < MTGP32_TN, user specifies USER_GROUP_NUM
#define MTGP32_STATE_SIZE 1024 // The effective state number is =351
#define MTGP32_STATE_MASK 1023
#define USER_GROUP_NUM 64  // < GPURAND_GROUP_NUM

#ifndef DIVUP
#define DIVUP(x, y) (((x) + (y) - 1) / (y))
#endif
#define BLOCK_SIZE 256
#define MAX_NUM_BLOCKS 64

// Structure of GPURandStateMtgp32
typedef struct GPURandStateMtgp32 {
  Concurrency::array_view<uint32_t, 1>* offset; // size: USER_GROUP_NUM
  Concurrency::array_view<uint32_t, 1>* index;  // size: USER_GROUP_NUM
  Concurrency::array_view<uint32_t, 2>* d_status; // extent<2>(USER_GROUP_NUM, MTGP32_STATE_SIZE)
  // mtgp32 kernel params
  Concurrency::array_view<uint32_t, 1>* mexp_tbl; // size: 1. Redundant
  Concurrency::array_view<uint32_t, 2>* param_tbl; // extent<2>(GPURAND_GROUP_NUM, MTGP32_TN)
  Concurrency::array_view<uint32_t, 2>* temper_tbl; // extent<2>(GPURAND_GROUP_NUM, MTGP32_TN)
  Concurrency::array_view<uint32_t, 2>* single_temper_tbl; // extent<2>(GPURAND_GROUP_NUM, MTGP32_TN)
  Concurrency::array_view<uint32_t, 1>* pos_tbl;  // size: MTGP32_TN
  Concurrency::array_view<uint32_t, 1>* sh1_tbl;  // size: MTGP32_TN
  Concurrency::array_view<uint32_t, 1>* sh2_tbl;  // size: MTGP32_TN
  Concurrency::array_view<uint32_t, 1>* mask;   // size: 1
} GPURandStateMtgp32;

// host array
typedef struct HOSTRandStateMtgp32 {
  uint32_t offset[USER_GROUP_NUM]; // size: USER_GROUP_NUM
  uint32_t index[USER_GROUP_NUM];  // size: USER_GROUP_NUM
  uint32_t d_status[USER_GROUP_NUM * MTGP32_STATE_SIZE]; // extent<2>(USER_GROUP_NUM, MTGP32_STATE_SIZE)
  // mtgp32 kernel params
  uint32_t mexp_tbl[GPURAND_GROUP_NUM]; // size: 1. Redundant
  uint32_t param_tbl[GPURAND_GROUP_NUM * MTGP32_TN]; // extent<2>(GPURAND_GROUP_NUM, MTGP32_TN)
  uint32_t temper_tbl[GPURAND_GROUP_NUM * MTGP32_TN]; // extent<2>(GPURAND_GROUP_NUM, MTGP32_TN)
  uint32_t single_temper_tbl[GPURAND_GROUP_NUM * MTGP32_TN]; // extent<2>(GPURAND_GROUP_NUM, MTGP32_TN)
  uint32_t pos_tbl[MTGP32_TN];  // size: MTGP32_TN
  uint32_t sh1_tbl[MTGP32_TN];  // size: MTGP32_TN
  uint32_t sh2_tbl[MTGP32_TN];  // size: MTGP32_TN
  uint32_t mask[1];   // size: 1
} HOSTRandStateMtgp32;

void GPURandStateMtgp32_init(Concurrency::accelerator_view accl_view,
                             GPURandStateMtgp32* s);
void GPURandStateMtgp32_release(GPURandStateMtgp32* s);
void GPURandStateMtgp32_copy_D2H(void* src, void* dst);
void GPURandStateMtgp32_copy_H2D(void* src, void* dst);


// Device APIs
int mtgp32_init_params_kernel(Concurrency::accelerator_view accl_view,
                              mtgp32_params_fast_t * params, GPURandStateMtgp32* s);
int mtgp32_init_seed_kernel(Concurrency::accelerator_view accl_view,
                            GPURandStateMtgp32* s, unsigned long seed);
// amp-compatible utilities
#if 0
static inline float as_float(uint32_t x) restrict(amp)
{
  union {
   uint32_t i;
   float f;
  } u;
  u.i = x;
  return u.f;
}
#endif
/**
 * The function of the recursion formula calculation.
 *
 * @param[in] X1 the farthest part of state array.
 * @param[in] X2 the second farthest part of state array.
 * @param[in] Y a part of state array.
 * @return output
 */
static inline uint32_t para_rec(uint32_t* param_tbl, uint32_t sh1, uint32_t sh2,
                            uint32_t X1, uint32_t X2, uint32_t Y, uint32_t mask) restrict(amp)
{
  uint32_t X = (X1 & mask) ^ X2;
  uint32_t MAT;

  X ^= X << sh1;
  Y = X ^ (Y >> sh2);
  MAT = param_tbl[Y & 0x0f];
  return Y ^ MAT;
}

/**
 * The tempering function.
 *
 * @param[in] mtgp mtgp32 structure
 * @param[in] V the output value should be tempered.
 * @param[in] T the tempering helper value.
 * @return the tempered value.
 */
static inline uint32_t temper(uint32_t* temper_tbl, uint32_t V, uint32_t T) restrict(amp)
{
  uint32_t MAT;

  T ^= T >> 16;
  T ^= T >> 8;
  MAT = temper_tbl[T & 0x0f]; // from group
  return V ^ MAT;
}

/**
 * The tempering and converting function.
 * By using the preset-ted table, converting to IEEE format
 * and tempering are done simultaneously.
 *
 * @param[in] V the output value should be tempered.
 * @param[in] T the tempering helper value.
 * @return the tempered and converted value.
 */
#if 0
static inline uint32_t temper_single(uint32_t* single_temper_tbl, uint32_t V, uint32_t T) restrict(amp)
{
  uint32_t MAT;
  uint32_t r;

  T ^= T >> 16;
  T ^= T >> 8;
  MAT = single_temper_tbl[T & 0x0f]; // from group
  r = (V >> 9) ^ MAT;
  return r;
}
#endif
// in one workgroup
static inline unsigned int gpurand(Concurrency::array_view<uint32_t, 2> &av_param_tbl,
                            Concurrency::array_view<uint32_t, 2> &av_temper_tbl,
                            Concurrency::array_view<uint32_t, 1> &av_sh1_tbl,
                            Concurrency::array_view<uint32_t, 1> &av_sh2_tbl,
                            Concurrency::array_view<uint32_t, 1> &av_offset,
                            Concurrency::array_view<uint32_t, 1> &av_index,
                            Concurrency::array_view<uint32_t, 1> &av_pos_tbl,
                            Concurrency::array_view<uint32_t, 1> &av_mask,
                            Concurrency::array_view<uint32_t, 2> &av_d_status,
                            int groupId, int threadId) restrict(amp)
{
  unsigned int t;
  unsigned int d;
  uint32_t* pParam_tbl = &av_param_tbl[groupId][0];
  uint32_t* pTemper_tbl = &av_temper_tbl[groupId][0];
  uint32_t* d_status = &av_d_status[groupId][0];
  uint32_t offset = av_offset[groupId];
  uint32_t sh1 = av_sh1_tbl[groupId];
  uint32_t sh2 = av_sh2_tbl[groupId];
  int index = av_index[groupId];
  int pos = av_pos_tbl[index];
  unsigned int r;
  unsigned int o;
  d = groupId;
  //assert( d <= 256 );
  t = threadId;
  r = para_rec(pParam_tbl, sh1, sh2, d_status[(t + offset) & MTGP32_STATE_MASK],
           d_status[(t + offset + 1) & MTGP32_STATE_MASK],
           d_status[(t + offset + pos) & MTGP32_STATE_MASK],
           av_mask[0]);
  d_status[(t + offset + MTGP32_N) & MTGP32_STATE_MASK] = r;
  o = temper(pTemper_tbl, r, d_status[(t + offset + pos -1) & MTGP32_STATE_MASK]);
  // We setup barrier outside
  #if 0
  tidx.barrier.wait();
  #endif
  if (t == 0)
  {
    av_offset[groupId] = (av_offset[groupId] + d) & MTGP32_STATE_MASK;
  }
  return o;
}


#define GPURAND_2POW32_INV (2.3283064e-10f)
#define GPURAND_SQRT2 (-1.4142135f)
#define GPURAND_SQRT2_DOUBLE (-1.4142135623730951)
static inline float _gpurand_uniform(unsigned int x) restrict(amp)
{
  return x * GPURAND_2POW32_INV + (GPURAND_2POW32_INV/2.0f);
}
inline float gpurand_uniform(Concurrency::array_view<uint32_t, 2> av_param_tbl,
                      Concurrency::array_view<uint32_t, 2> av_temper_tbl,
                      Concurrency::array_view<uint32_t, 1> av_sh1_tbl,
                      Concurrency::array_view<uint32_t, 1> av_sh2_tbl,
                      Concurrency::array_view<uint32_t, 1> av_offset,
                      Concurrency::array_view<uint32_t, 1> av_index,
                      Concurrency::array_view<uint32_t, 1> av_pos_tbl,
                      Concurrency::array_view<uint32_t, 1> av_mask,
                      Concurrency::array_view<uint32_t, 2> av_d_status,
                      int groupId, int threadId) restrict(amp)
{
  unsigned int x = gpurand(av_param_tbl, av_temper_tbl, av_sh1_tbl, av_sh2_tbl, av_offset,
                    av_index, av_pos_tbl, av_mask, av_d_status, groupId, threadId);
  return _gpurand_uniform(x);
}
// http://hg.savannah.gnu.org/hgweb/octave/rev/cb85e836d035
static inline double do_erfcinv( float x, bool refine) restrict(amp)
{
  // Coefficients of rational approximation.
  static const double a[] =
    { -2.806989788730439e+01,  1.562324844726888e+02,
      -1.951109208597547e+02,  9.783370457507161e+01,
      -2.168328665628878e+01,  1.772453852905383e+00 };
  static const double b[] =
    { -5.447609879822406e+01,  1.615858368580409e+02,
      -1.556989798598866e+02,  6.680131188771972e+01,
      -1.328068155288572e+01 };
  static const double c[] =
    { -5.504751339936943e-03, -2.279687217114118e-01,
      -1.697592457770869e+00, -1.802933168781950e+00,
       3.093354679843505e+00,  2.077595676404383e+00 };
  static const double d[] =
    {  7.784695709041462e-03,  3.224671290700398e-01,
       2.445134137142996e+00,  3.754408661907416e+00 };

  static const double spi2 =  8.862269254527579e-01; // sqrt(pi)/2.
  //static const double pi = 3.14159265358979323846;
  static const double pbreak = 0.95150;
  double y;

  // Select case.
  if (x <= 1+pbreak && x >= 1-pbreak)
    {
      // Middle region.
      const double q = 0.5*(1-x), r = q*q;
      const double yn = (((((a[0]*r + a[1])*r + a[2])*r + a[3])*r + a[4])*r + a[5])*q;
      const double yd = ((((b[0]*r + b[1])*r + b[2])*r + b[3])*r + b[4])*r + 1.0;
      y = yn / yd;
    }
  else if (x < 2.0 && x > 0.0)
    {
      // Tail region.
      const double q = x < 1 ? Concurrency::precise_math::sqrt (-2*Concurrency::precise_math::log ((double)0.5*x)) 
      : Concurrency::precise_math::sqrt (-2*Concurrency::precise_math::log ((double)0.5*(2-x)));
      const double yn = ((((c[0]*q + c[1])*q + c[2])*q + c[3])*q + c[4])*q + c[5];
      const double yd = (((d[0]*q + d[1])*q + d[2])*q + d[3])*q + 1.0;
      y = yn / yd;
      if (x < 1-pbreak)
        y *= -1;
    }
  else if (x == 0.0)
    return 0;//octave_Inf;
  else if (x == 2.0)
    return 0;//-octave_Inf;
  else
    return 1;//octave_NaN;

  if (refine) {
    // One iteration of Halley's method gives full precision.
    double u = (erf(y) - (1-x)) * spi2 * exp (y*y);
    y -= u / (1 + y*u);
  }

 return y;
}

static inline double erfcinvd(double x) restrict(amp)
{
  return do_erfcinv (x, true);
}

static inline float erfcinvf(float x) restrict(amp)
{
  return do_erfcinv (x, false);
}

static inline float _gpurand_normal_icdf(unsigned int x) restrict(amp)
{
  float s = GPURAND_SQRT2;
  // Mirror to avoid loss of precision
  if(x > 0x80000000UL) {
      x = 0xffffffffUL - x;
      s = -s;
  }
  float p = x * GPURAND_2POW32_INV + (GPURAND_2POW32_INV/2.0f);
  // p is in (0, 0.5], 2p is in (0, 1]
  return s * erfcinvf(2.0f * p);
}
static inline float gpurand_normal(Concurrency::array_view<uint32_t, 2> av_param_tbl,
                     Concurrency::array_view<uint32_t, 2> av_temper_tbl,
                     Concurrency::array_view<uint32_t, 1> av_sh1_tbl,
                     Concurrency::array_view<uint32_t, 1> av_sh2_tbl,
                     Concurrency::array_view<uint32_t, 1> av_offset,
                     Concurrency::array_view<uint32_t, 1> av_index,
                     Concurrency::array_view<uint32_t, 1> av_pos_tbl,
                     Concurrency::array_view<uint32_t, 1> av_mask,
                     Concurrency::array_view<uint32_t, 2> av_d_status,
                     int groupId, int threadId) restrict(amp)
{
  unsigned int x = gpurand(av_param_tbl, av_temper_tbl, av_sh1_tbl, av_sh2_tbl, av_offset,
                    av_index, av_pos_tbl, av_mask, av_d_status, groupId, threadId);
  return _gpurand_normal_icdf(x);
}

static inline double gpurand_log_normal(Concurrency::array_view<uint32_t, 2> av_param_tbl,
                         Concurrency::array_view<uint32_t, 2> av_temper_tbl,
                         Concurrency::array_view<uint32_t, 1> av_sh1_tbl,
                         Concurrency::array_view<uint32_t, 1> av_sh2_tbl,
                         Concurrency::array_view<uint32_t, 1> av_offset,
                         Concurrency::array_view<uint32_t, 1> av_index,
                         Concurrency::array_view<uint32_t, 1> av_pos_tbl,
                         Concurrency::array_view<uint32_t, 1> av_mask,
                         Concurrency::array_view<uint32_t, 2> av_d_status,
                         int groupId, int threadId, double mean, double stddev) restrict(amp)
{
  unsigned int x = gpurand(av_param_tbl, av_temper_tbl, av_sh1_tbl, av_sh2_tbl, av_offset,
                    av_index, av_pos_tbl, av_mask, av_d_status, groupId, threadId);
  return Concurrency::precise_math::exp(mean + ((double)stddev * _gpurand_normal_icdf(x)));
}

// User defined wrappers
static inline void user_log_normal_kernel(Concurrency::accelerator_view accl_view, 
                         GPURandStateMtgp32 *s,
                         Concurrency::array_view<float, 1> &av_result,
                         double mean, double stddev)
{
  const int size = av_result.get_extent().size();
  int rounded_size = DIVUP(size, BLOCK_SIZE) * BLOCK_SIZE;
  int blocks = std::min((int)DIVUP(size, BLOCK_SIZE), MAX_NUM_BLOCKS);
  Concurrency::extent<1> ext(blocks*BLOCK_SIZE);
  Concurrency::tiled_extent<BLOCK_SIZE> t_ext(ext);
  Concurrency::array_view<uint32_t, 2> &av_param_tbl = *(s->param_tbl);
  Concurrency::array_view<uint32_t, 2> &av_temper_tbl = *(s->temper_tbl);
  Concurrency::array_view<uint32_t, 1> &av_sh1_tbl = *(s->sh1_tbl);
  Concurrency::array_view<uint32_t, 1> &av_sh2_tbl = *(s->sh2_tbl);
  Concurrency::array_view<uint32_t, 1> &av_offset = *(s->offset);
  Concurrency::array_view<uint32_t, 1> &av_index = *(s->index);
  Concurrency::array_view<uint32_t, 1> &av_pos_tbl = *(s->pos_tbl);
  Concurrency::array_view<uint32_t, 1> &av_mask = *(s->mask);
  Concurrency::array_view<uint32_t, 2> &av_d_status = *(s->d_status);
  av_param_tbl.discard_data();
  av_temper_tbl.discard_data();
  av_sh1_tbl.discard_data();
  av_sh2_tbl.discard_data();
  av_offset.discard_data();
  av_index.discard_data();
  av_pos_tbl.discard_data();
  av_mask.discard_data();
  av_d_status.discard_data();

  av_result.discard_data();
  Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<BLOCK_SIZE> tidx) restrict(amp)
  {
    int threadId = tidx.global[0];
    int groupId = tidx.tile[0];
    if (groupId >= USER_GROUP_NUM)
      return;
    double x = gpurand_log_normal(av_param_tbl, av_temper_tbl, av_sh1_tbl, av_sh2_tbl, av_offset,
                   av_index, av_pos_tbl, av_mask, av_d_status, groupId, threadId, mean, stddev);
    tidx.barrier.wait_with_tile_static_memory_fence();

    for (int i = threadId; i < rounded_size; i += BLOCK_SIZE * MAX_NUM_BLOCKS) {
      if (i < size) {
        av_result[i] = x;
      }
    }
  });
}


// User defined wrappers
template <typename UnaryFunction>
inline void user_uniform_kernel(Concurrency::accelerator_view accl_view, 
                         GPURandStateMtgp32 *s,
                         Concurrency::array_view<float, 1> &av_result,
                         UnaryFunction f)
{
  const int size = av_result.get_extent().size();
  int rounded_size = DIVUP(size, BLOCK_SIZE) * BLOCK_SIZE;
  int blocks = std::min((int)DIVUP(size, BLOCK_SIZE), MAX_NUM_BLOCKS);
  Concurrency::extent<1> ext(blocks*BLOCK_SIZE);
  Concurrency::tiled_extent<BLOCK_SIZE> t_ext(ext);
  Concurrency::array_view<uint32_t, 2> &av_param_tbl = *(s->param_tbl);
  Concurrency::array_view<uint32_t, 2> &av_temper_tbl = *(s->temper_tbl);
  Concurrency::array_view<uint32_t, 1> &av_sh1_tbl = *(s->sh1_tbl);
  Concurrency::array_view<uint32_t, 1> &av_sh2_tbl = *(s->sh2_tbl);
  Concurrency::array_view<uint32_t, 1> &av_offset = *(s->offset);
  Concurrency::array_view<uint32_t, 1> &av_index = *(s->index);
  Concurrency::array_view<uint32_t, 1> &av_pos_tbl = *(s->pos_tbl);
  Concurrency::array_view<uint32_t, 1> &av_mask = *(s->mask);
  Concurrency::array_view<uint32_t, 2> &av_d_status = *(s->d_status);
  av_param_tbl.discard_data();
  av_temper_tbl.discard_data();
  av_sh1_tbl.discard_data();
  av_sh2_tbl.discard_data();
  av_offset.discard_data();
  av_index.discard_data();
  av_pos_tbl.discard_data();
  av_mask.discard_data();
  av_d_status.discard_data();

  av_result.discard_data();
  Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<BLOCK_SIZE> tidx) restrict(amp)
  {
    int threadId = tidx.global[0];
    int groupId = tidx.tile[0];
    if (groupId >= USER_GROUP_NUM)
      return;
    float x = gpurand_uniform(av_param_tbl, av_temper_tbl, av_sh1_tbl, av_sh2_tbl, av_offset,
                    av_index, av_pos_tbl, av_mask, av_d_status, groupId, threadId);
    tidx.barrier.wait_with_tile_static_memory_fence();
    for (int i = threadId; i < rounded_size; i += BLOCK_SIZE * MAX_NUM_BLOCKS) {
      if (i < size) {
        double y = f(x);
        av_result[i] = y;
      }
    }
  });
}

template <typename UnaryFunction>
void user_normal_kernel(Concurrency::accelerator_view accl_view, 
                         GPURandStateMtgp32 *s,
                         Concurrency::array_view<float, 1> &av_result,
                         UnaryFunction f)
{
  const int size = av_result.get_extent().size();
  int rounded_size = DIVUP(size, BLOCK_SIZE) * BLOCK_SIZE;
  int blocks = std::min((int)DIVUP(size, BLOCK_SIZE), MAX_NUM_BLOCKS);
  Concurrency::extent<1> ext(blocks*BLOCK_SIZE);
  Concurrency::tiled_extent<BLOCK_SIZE> t_ext(ext);
  Concurrency::array_view<uint32_t, 2> &av_param_tbl = *(s->param_tbl);
  Concurrency::array_view<uint32_t, 2> &av_temper_tbl = *(s->temper_tbl);
  Concurrency::array_view<uint32_t, 1> &av_sh1_tbl = *(s->sh1_tbl);
  Concurrency::array_view<uint32_t, 1> &av_sh2_tbl = *(s->sh2_tbl);
  Concurrency::array_view<uint32_t, 1> &av_offset = *(s->offset);
  Concurrency::array_view<uint32_t, 1> &av_index = *(s->index);
  Concurrency::array_view<uint32_t, 1> &av_pos_tbl = *(s->pos_tbl);
  Concurrency::array_view<uint32_t, 1> &av_mask = *(s->mask);
  Concurrency::array_view<uint32_t, 2> &av_d_status = *(s->d_status);
  av_param_tbl.discard_data();
  av_temper_tbl.discard_data();
  av_sh1_tbl.discard_data();
  av_sh2_tbl.discard_data();
  av_offset.discard_data();
  av_index.discard_data();
  av_pos_tbl.discard_data();
  av_mask.discard_data();
  av_d_status.discard_data();

  av_result.discard_data();
  Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<BLOCK_SIZE> tidx) restrict(amp)
  {
    int threadId = tidx.global[0];
    int groupId = tidx.tile[0];
    if (groupId >= USER_GROUP_NUM)
      return;
    float x = gpurand_normal(av_param_tbl, av_temper_tbl, av_sh1_tbl, av_sh2_tbl, av_offset,
                    av_index, av_pos_tbl, av_mask, av_d_status, groupId, threadId);
    tidx.barrier.wait_with_tile_static_memory_fence();
    for (int i = threadId; i < rounded_size; i += BLOCK_SIZE * MAX_NUM_BLOCKS) {
      if (i < size) {
        double y = f(x);
        av_result[i] = y;
      }
    }
  });
}


#include "THCBlas.h"
#include "THCGeneral.h"
#include "amp_math.h"
#define OFFSET(N, incX) ((incX) > 0 ? 0 : ((N) - 1) * (-(incX)))
#define BLOCK_SIZE 256
#define TILE_DIM   16
#define THREADS    16
#define GEMM_BLOCK 256

// Matrix Multiplication with  A and B matrices  not transposed
static void gemm_NoTransAB(THCState *state, Concurrency::array_view<float, 1> &A, long aOffset,
                           Concurrency::array_view<float, 1> &B, long bOffset,
                           Concurrency::array_view<float, 1> &C, long cOffset,
                           int M, int N, int K, int lda, int ldb, int ldc,
                           float alpha, float beta)
{
  // Make grid size in each dimension an exact multiple of threadblock size in the corresponding dimension
  Concurrency::extent<2> grdExt((N + (THREADS - 1)) & ~(THREADS - 1),(M + (THREADS-1)) & ~(THREADS - 1));
  Concurrency::tiled_extent<THREADS, THREADS> t_ext(grdExt);

  THCGPUDeviceState* device_state = state->deviceState;
  Concurrency::accelerator_view accl_view = device_state->get_current_accelerator_view();
  Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<THREADS, THREADS> tidx) restrict(amp)
  {
    float CValue = 0;
    int Row = tidx.tile[0] * TILE_DIM + tidx.local[0];
    int Col = tidx.tile[1] * TILE_DIM + tidx.local[1];
    tile_static float As[TILE_DIM][TILE_DIM];
    tile_static float Bs[TILE_DIM][TILE_DIM];

    for (int k = 0; k < (TILE_DIM + K - 1) / TILE_DIM; k++)
    {
      // Read Matrix B from global to shared tile
      if (k * TILE_DIM + tidx.local[1] < K && Row < N)
        Bs[tidx.local[0]][tidx.local[1]] = B[bOffset + Row * K + k * TILE_DIM + tidx.local[1]];
      else
        Bs[tidx.local[0]][tidx.local[1]] = 0.0;

      // Read Matrix A from global to shared tile
      if (k*TILE_DIM + tidx.local[0] < K && Col < M)
        As[tidx.local[0]][tidx.local[1]] = A[aOffset + (k * TILE_DIM + tidx.local[0]) * M + Col];
      else
        As[tidx.local[0]][tidx.local[1]] = 0.0;

      // Wait until all shared memory gets filled
      tidx.barrier.wait();

      for (int n = 0; n < TILE_DIM; ++n)
        CValue += Bs[tidx.local[0]][n] * As[n][tidx.local[1]] * alpha;

      tidx.barrier.wait();
    }

    if (Row < N && Col < M)
    {
      C[cOffset + (tidx.global[0] * M) + tidx.global[1]] *= beta;
      C[cOffset + (tidx.global[0] * M) + tidx.global[1]] += CValue;
    }
  });
}

#define TILESIZE 16 

#define  MTS                                                                \
           for (int iter = 0; iter < MICROTILESIZE_A ; iter++)              \
           {                                                                \
             rA[0][iter] = lA[offA + (iter * TILESIZE)];                    \
           }                                                                \
           for (int iter = 0; iter < MICROTILESIZE_B ; iter++)              \
           {                                                                \
             rB[0][iter] = lB[offB + (iter * TILESIZE)];                    \
           }                                                                \
           for (int rowIndex = 0; rowIndex < MICROTILESIZE_A ; rowIndex++)  \
           {                                                                \
           for (int colIndex = 0; colIndex < MICROTILESIZE_B ; colIndex++)  \
           {                                                                \
           rC[rowIndex][colIndex] = rA[0][rowIndex] * rB[0][colIndex] +     \
                                    rC[rowIndex][colIndex];                 \
           }                                                                \
           }                                                                \
           offA += (MICROTILESIZE_A * TILESIZE);                            \
           offB += (MICROTILESIZE_B * TILESIZE);                            \

static void gemm_NoTransAB_batch(THCState *state, Concurrency::array_view<float, 1> &A, long aOffset, long A_batchOffset,
                                 Concurrency::array_view<float, 1> &B, long bOffset, long B_batchOffset,
                                 Concurrency::array_view<float, 1> &C, long cOffset, long C_batchOffset,
                                 int M, int N, int K, int lda, int ldb, int ldc,
                                 float alpha, float beta, int batchSize)
{
  THCGPUDeviceState* device_state = state->deviceState;
  Concurrency::accelerator_view accl_view = device_state->get_current_accelerator_view();
  if ((M - N) > 2000)
  {
#define MICROTILESIZE_A 4 
#define MICROTILESIZE_B 1
    Concurrency::extent<3> grdExt(batchSize, ((N / MICROTILESIZE_B) + (TILESIZE - 1)) & ~(TILESIZE - 1), ((M / MICROTILESIZE_A) + (TILESIZE - 1)) & ~(TILESIZE - 1));
    Concurrency::tiled_extent<1, TILESIZE, TILESIZE> t_ext(grdExt);

    Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<1, TILESIZE, TILESIZE> tidx) restrict(amp)
    {
      int elt = tidx.tile[0];
      float rC[MICROTILESIZE_A][MICROTILESIZE_B] = {{0}};
      float rA[1][MICROTILESIZE_A];
      float rB[1][MICROTILESIZE_B];
      tile_static float lA[TILESIZE * TILESIZE * MICROTILESIZE_A];
      tile_static float lB[TILESIZE * TILESIZE * MICROTILESIZE_B];
      int gidx = tidx.tile[2];
      int gidy = tidx.tile[1];
      int idx = tidx.local[2];
      int idy = tidx.local[1];
      int idt = TILESIZE * idy + idx;
      int idxT = idt % TILESIZE;
      int idyT = idt / TILESIZE;

      int block_k = 0;
      do
      {
        for (int sec = 0; sec < MICROTILESIZE_B; ++sec)
        {
          if (gidy * TILESIZE * MICROTILESIZE_B + idxT + (sec * TILESIZE) < N && block_k * TILESIZE + idyT < K)
          {
            lB[(idyT * TILESIZE * MICROTILESIZE_B) + idxT + (sec * TILESIZE)] = B[bOffset + B_batchOffset * elt + (gidy * TILESIZE * MICROTILESIZE_B + idxT + sec * TILESIZE) * ldb + idyT + block_k * TILESIZE];
          }
          else
          {
            lB[(idyT * TILESIZE * MICROTILESIZE_B) + idxT + (sec * TILESIZE)] = 0;
          }
        }

        for (int sec = 0; sec < MICROTILESIZE_A; ++sec)
        {
          if (gidx * TILESIZE * MICROTILESIZE_A + idxT + (sec * TILESIZE) < M && block_k * TILESIZE + idyT < K)
          {
            lA[(idyT * TILESIZE * MICROTILESIZE_A) + idxT + (sec * TILESIZE)] = A[aOffset + A_batchOffset * elt + (gidx * TILESIZE * MICROTILESIZE_A) + idxT + (sec * TILESIZE) +  idyT * lda + block_k * (lda * TILESIZE)];
          }
          else
          {
            lA[(idyT * TILESIZE * MICROTILESIZE_A) + idxT + (sec * TILESIZE)] = 0;
          }
        }
        tidx.barrier.wait();

        int offA = idx;
        int offB = idy;
        for (int iter=0; iter < TILESIZE; ++iter)
        {
          MTS;
        }
        tidx.barrier.wait();
      } while (++block_k < (((K + TILESIZE - 1) & ~(TILESIZE - 1))/TILESIZE));

      int xIndex = gidx * TILESIZE * MICROTILESIZE_A + idx;
      int yIndex = gidy * TILESIZE * MICROTILESIZE_B + idy;
      for (int col = 0; col < MICROTILESIZE_A; col++)
      {
        for (int row = 0; row < MICROTILESIZE_B ; row++)
        {
        if (xIndex + (TILESIZE * col) < M && (yIndex) + (TILESIZE * row) < N)
          C[cOffset + C_batchOffset * elt + (xIndex + TILESIZE * col) + (yIndex + TILESIZE * row) * ldc] = alpha * rC[col][row] + beta * C[cOffset + C_batchOffset * elt + (xIndex + TILESIZE * col) + (yIndex + TILESIZE * row) * ldc];
        }
      }
   });
#undef MICROTILESIZE_A
#undef MICROTILESIZE_B
  }
  else if ((K - M) > 500 && (K - N) > 500)
  {
    // Make grid size in each dimension an exact multiple of threadblock size in the corresponding dimension
    Concurrency::extent<3> grdExt(batchSize, (N + (THREADS - 1)) & ~(THREADS - 1),(M + (THREADS-1)) & ~(THREADS - 1));
    Concurrency::tiled_extent<1, THREADS, THREADS> t_ext(grdExt);

    Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<1, THREADS, THREADS> tidx) restrict(amp)
    {
      float CValue = 0;
      int elt = tidx.tile[0];
      int Row = tidx.tile[1] * TILE_DIM + tidx.local[1];
      int Col = tidx.tile[2] * TILE_DIM + tidx.local[2];
      tile_static float As[TILE_DIM*2][TILE_DIM];
      tile_static float Bs[TILE_DIM][TILE_DIM*2];

      for (int k = 0; k < (TILE_DIM + K - 1) / TILE_DIM; k+=2)
      {
        // Read Matrix B from global to shared tile
        for (int sec = 0; sec < 2; sec++)
        {
        if (((k + sec) * TILE_DIM + tidx.local[2]) < K && Row < N)
          Bs[tidx.local[1]][tidx.local[2] + sec * TILE_DIM] = B[bOffset + B_batchOffset * elt + Row * K + (k + sec) * TILE_DIM + tidx.local[2]];
        else
          Bs[tidx.local[1]][tidx.local[2] + sec * TILE_DIM] = 0.0;

        // Read Matrix A from global to shared tile
        if ((k + sec) * TILE_DIM + tidx.local[1] < K && Col < M)
          As[tidx.local[1] + sec * TILE_DIM][tidx.local[2]] = A[aOffset + A_batchOffset * elt + ((k + sec) * TILE_DIM + tidx.local[1]) * M + Col];
        else
          As[tidx.local[1] + sec * TILE_DIM][tidx.local[2]] = 0.0;
        }

        // Wait until all shared memory gets filled
        tidx.barrier.wait();

        for (int n = 0; n < TILE_DIM*2; ++n)
          CValue += Bs[tidx.local[1]][n] * As[n][tidx.local[2]] * alpha;

        tidx.barrier.wait();
      }

      if (Row < N && Col < M)
      {
        C[cOffset + C_batchOffset * elt + (tidx.global[1] * M) + tidx.global[2]] *= beta;
        C[cOffset + C_batchOffset * elt + (tidx.global[1] * M) + tidx.global[2]] += CValue;
      }
    });
  }
  else if ((M - N) > 500 && K > 1000)
  {
#define MICROTILESIZE_A 3 
#define MICROTILESIZE_B 1 
    Concurrency::extent<3> grdExt(batchSize, ((N / MICROTILESIZE_B) + (TILESIZE - 1)) & ~(TILESIZE - 1), ((M / MICROTILESIZE_A) + (TILESIZE - 1)) & ~(TILESIZE - 1));
    Concurrency::tiled_extent<1, TILESIZE, TILESIZE> t_ext(grdExt);

    Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<1, TILESIZE, TILESIZE> tidx) restrict(amp)
    {
      int elt = tidx.tile[0];
      float rC[MICROTILESIZE_A][MICROTILESIZE_B] = {{0}};
      float rA[1][MICROTILESIZE_A];
      float rB[1][MICROTILESIZE_B];
      tile_static float lA[TILESIZE * TILESIZE * MICROTILESIZE_A];
      tile_static float lB[TILESIZE * TILESIZE * MICROTILESIZE_B];
      int gidx = tidx.tile[2];
      int gidy = tidx.tile[1];
      int idx = tidx.local[2];
      int idy = tidx.local[1];
      int idt = TILESIZE * idy + idx;
      int idxT = idt % TILESIZE;
      int idyT = idt / TILESIZE;

      int block_k = 0;
      do
      {
        for (int sec = 0; sec < MICROTILESIZE_B; ++sec)
        {
          if (gidy * TILESIZE * MICROTILESIZE_B + idxT + (sec * TILESIZE) < N && block_k * TILESIZE + idyT < K)
          {
            lB[(idyT * TILESIZE * MICROTILESIZE_B) + idxT + (sec * TILESIZE)] = B[bOffset + B_batchOffset * elt + (gidy * TILESIZE * MICROTILESIZE_B + idxT + sec * TILESIZE) * ldb + idyT + block_k * TILESIZE];
          }
          else
          {
            lB[(idyT * TILESIZE * MICROTILESIZE_B) + idxT + (sec * TILESIZE)] = 0;
          }
        }

        for (int sec = 0; sec < MICROTILESIZE_A; ++sec)
        {
          if (gidx * TILESIZE * MICROTILESIZE_A + idxT + (sec * TILESIZE) < M && block_k * TILESIZE + idyT < K)
          {
            lA[(idyT * TILESIZE * MICROTILESIZE_A) + idxT + (sec * TILESIZE)] = A[aOffset + A_batchOffset * elt + (gidx * TILESIZE * MICROTILESIZE_A) + idxT + (sec * TILESIZE) +  idyT * lda + block_k * (lda * TILESIZE)];
          }
          else
          {
            lA[(idyT * TILESIZE * MICROTILESIZE_A) + idxT + (sec * TILESIZE)] = 0;
          }
        }
        tidx.barrier.wait();

        int offA = idx;
        int offB = idy;
        for (int iter=0; iter < TILESIZE; ++iter)
        {
          MTS;
        }
        tidx.barrier.wait();
      } while (++block_k < (((K + TILESIZE - 1) & ~(TILESIZE - 1))/TILESIZE));

      int xIndex = gidx * TILESIZE * MICROTILESIZE_A + idx;
      int yIndex = gidy * TILESIZE * MICROTILESIZE_B + idy;
      for (int col = 0; col < MICROTILESIZE_A; col++)
      {
        for (int row = 0; row < MICROTILESIZE_B ; row++)
        {
        if (xIndex + (TILESIZE * col) < M && (yIndex) + (TILESIZE * row) < N)
          C[cOffset + C_batchOffset * elt + (xIndex + TILESIZE * col) + (yIndex + TILESIZE * row) * ldc] = alpha * rC[col][row] + beta * C[cOffset + C_batchOffset * elt + (xIndex + TILESIZE * col) + (yIndex + TILESIZE * row) * ldc];
        }
      }
   });
#undef MICROTILESIZE_A
#undef MICROTILESIZE_B
  }
  else
  {
    // Make grid size in each dimension an exact multiple of threadblock size in the corresponding dimension
    Concurrency::extent<3> grdExt(batchSize, (N + (THREADS - 1)) & ~(THREADS - 1),(M + (THREADS-1)) & ~(THREADS - 1));
    Concurrency::tiled_extent<1, THREADS, THREADS> t_ext(grdExt);

    Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<1, THREADS, THREADS> tidx) restrict(amp)
    {
      float CValue = 0;
      int elt = tidx.tile[0];
      int Row = tidx.tile[1] * TILE_DIM + tidx.local[1];
      int Col = tidx.tile[2] * TILE_DIM + tidx.local[2];
      tile_static float As[TILE_DIM][TILE_DIM];
      tile_static float Bs[TILE_DIM][TILE_DIM];

      for (int k = 0; k < (TILE_DIM + K - 1) / TILE_DIM; k++)
      {
        // Read Matrix B from global to shared tile
        if ((k * TILE_DIM + tidx.local[2]) < K && Row < N)
          Bs[tidx.local[1]][tidx.local[2]] = B[bOffset + B_batchOffset * elt + Row * K + k * TILE_DIM + tidx.local[2]];
        else
          Bs[tidx.local[1]][tidx.local[2]] = 0.0;

        // Read Matrix A from global to shared tile
        if (k*TILE_DIM + tidx.local[1] < K && Col < M)
          As[tidx.local[1]][tidx.local[2]] = A[aOffset + A_batchOffset * elt + (k * TILE_DIM + tidx.local[1]) * M + Col];
        else
          As[tidx.local[1]][tidx.local[2]] = 0.0;

        // Wait until all shared memory gets filled
        tidx.barrier.wait();

        for (int n = 0; n < TILE_DIM; ++n)
          CValue += Bs[tidx.local[1]][n] * As[n][tidx.local[2]] * alpha;

        tidx.barrier.wait();
      }

      if (Row < N && Col < M)
      {
        C[cOffset + C_batchOffset * elt + (tidx.global[1] * M) + tidx.global[2]] *= beta;
        C[cOffset + C_batchOffset * elt + (tidx.global[1] * M) + tidx.global[2]] += CValue;
      }
    });
  }
}


// Matrix Multiplication with  matrix A Transposed
static void gemm_NoTransB(THCState *state, Concurrency::array_view<float, 1> &A, long aOffset,
                          Concurrency::array_view<float, 1> &B, long bOffset,
                          Concurrency::array_view<float, 1> &C, long cOffset,
                          int M, int N, int K, int lda, int ldb, int ldc,
                          float alpha, float beta)
{
  
  THCGPUDeviceState* device_state = state->deviceState;
  Concurrency::accelerator_view accl_view = device_state->get_current_accelerator_view();
  // If K is small then make use of threads and blocks across M and N dimension
  if (K >= N && K >= M && M < 2000)
  {
    // Make grid size in each dimension an exact multiple of threadblock size in the corresponding dimension
    Concurrency::extent<2> grdExt(N, M * GEMM_BLOCK);
    Concurrency::tiled_extent<1, GEMM_BLOCK> t_ext(grdExt);

    Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<1, GEMM_BLOCK> tidx) restrict(amp)
    {
      int threadIdx = tidx.local[1];
      int blockIdx = tidx.tile[1];
      int Row = tidx.tile[0];
      int Col = blockIdx;

      tile_static float sh[GEMM_BLOCK];
      sh[threadIdx] = 0;

      for (int tileId = 0; tileId < ((K + GEMM_BLOCK - 1) & ~(GEMM_BLOCK - 1)) / GEMM_BLOCK; tileId++)
      {
        if (tileId * GEMM_BLOCK + threadIdx < K && Col < M && Row < N)
          sh[threadIdx] += A[aOffset + Col * K + tileId * GEMM_BLOCK + threadIdx] * B[bOffset + Row * K + tileId * GEMM_BLOCK + threadIdx];
      }
      tidx.barrier.wait();

      for (int stride = GEMM_BLOCK / 2; stride >= 1; stride /= 2)
      {
        if (threadIdx < stride)
          sh[threadIdx] += sh[threadIdx + stride];
        tidx.barrier.wait();
      }

      if(threadIdx == 0 && Col < M && Row < N)
      {
        C[cOffset + Row * M + Col] *= beta;
        C[cOffset + Row * M + Col] += sh[0] * alpha;
      }
    });
  }
  else
  {
#define MICROTILESIZE_A 3 
#define MICROTILESIZE_B 1 
    Concurrency::extent<2> grdExt(((N / MICROTILESIZE_B) + (TILESIZE - 1)) & ~(TILESIZE - 1), ((M / MICROTILESIZE_A) + (TILESIZE - 1)) & ~(TILESIZE - 1));
    Concurrency::tiled_extent<TILESIZE, TILESIZE> t_ext(grdExt);

    Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<TILESIZE, TILESIZE> tidx) restrict(amp)
    {
      float rC[MICROTILESIZE_A][MICROTILESIZE_B] = {{0}};
      float rA[1][MICROTILESIZE_A];
      float rB[1][MICROTILESIZE_B];
      tile_static float lA[TILESIZE * TILESIZE * MICROTILESIZE_A];
      tile_static float lB[TILESIZE * TILESIZE * MICROTILESIZE_B];
      int gidx = tidx.tile[1];
      int gidy = tidx.tile[0];
      int idx = tidx.local[1];
      int idy = tidx.local[0];
      int idt = TILESIZE * idy + idx;
      int idxT = idt % TILESIZE;
      int idyT = idt / TILESIZE;

      int block_k = 0;
      do
      {
        for (int sec = 0; sec < MICROTILESIZE_B; ++sec)
        {
          if (gidy * TILESIZE * MICROTILESIZE_B + idxT + (sec * TILESIZE) < N && block_k * TILESIZE + idyT < K)
          {
            lB[(idyT * TILESIZE * MICROTILESIZE_B) + idxT + (sec * TILESIZE)] = B[bOffset + (gidy * TILESIZE * MICROTILESIZE_B + idxT + sec * TILESIZE) * ldb + idyT + block_k * TILESIZE];
          }
          else
          {
            lB[(idyT * TILESIZE * MICROTILESIZE_B) + idxT + (sec * TILESIZE)] = 0;
          }
        }

        for (int sec = 0; sec < MICROTILESIZE_A; ++sec)
        {
          if (gidx * TILESIZE * MICROTILESIZE_A + idxT + (sec * TILESIZE) < M && block_k * TILESIZE + idyT < K)
          {
            lA[(idyT * TILESIZE * MICROTILESIZE_A) + idxT + (sec * TILESIZE)] = A[aOffset + (gidx * TILESIZE * MICROTILESIZE_A + idxT + sec * TILESIZE) * lda +  idyT + block_k * TILESIZE];
          }
          else
          {
            lA[(idyT * TILESIZE * MICROTILESIZE_A) + idxT + (sec * TILESIZE)] = 0;
          }
        }
        tidx.barrier.wait();

        int offA = idx;
        int offB = idy;
        for (int iter=0; iter < TILESIZE; ++iter)
        {
          MTS;
        }
        tidx.barrier.wait();
      } while (++block_k < (((K + TILESIZE - 1) & ~(TILESIZE - 1))/TILESIZE));

      int xIndex = gidx * TILESIZE * MICROTILESIZE_A + idx;
      int yIndex = gidy * TILESIZE * MICROTILESIZE_B + idy;
      for (int col = 0; col < MICROTILESIZE_A; col++)
      {
        for (int row = 0; row < MICROTILESIZE_B ; row++)
        {
        if (xIndex + (TILESIZE * col) < M && (yIndex) + (TILESIZE * row) < N)
          C[cOffset + (xIndex + TILESIZE * col) + (yIndex + TILESIZE * row) * ldc] = alpha * rC[col][row] + beta * C[cOffset + (xIndex + TILESIZE * col) + (yIndex + TILESIZE * row) * ldc];
        }
      }
   });
#undef MICROTILESIZE_A
#undef MICROTILESIZE_B
  }
}

#define TILESIZE_BATCH 16

#define  MTS_BATCH                                                             \
           for(int iter = 0; iter < MICROTILESIZE_BATCH_A; iter++)             \
           {                                                                   \
             rA[0][iter] = lA[offA + (iter * TILESIZE_BATCH)];                 \
           }                                                                   \
           for(int iter = 0; iter < MICROTILESIZE_BATCH_B; iter++)             \
           {                                                                   \
             rB[0][iter] = lB[offB + (iter * TILESIZE_BATCH)];                 \
           }                                                                   \
           for(int rowIndex = 0; rowIndex < MICROTILESIZE_BATCH_A; rowIndex++) \
           {                                                                   \
           for(int colIndex = 0; colIndex < MICROTILESIZE_BATCH_B; colIndex++) \
           {                                                                   \
           rC[rowIndex][colIndex] = rA[0][rowIndex] * rB[0][colIndex] +        \
                                    rC[rowIndex][colIndex];                    \
           }                                                                   \
           }                                                                   \
           offA += (MICROTILESIZE_BATCH_A * TILESIZE_BATCH);                   \
           offB += (MICROTILESIZE_BATCH_B * TILESIZE_BATCH);                   \

#define TILE_SZ 8
#define TILESIZE_MK 8 

#define M1x1                            \
  rA[0][0] = lA[offA + 0];              \
  rB[0][0] = lB[offB + 0];              \
  offA += 1;                            \
  offB += 1;                            \
  rC[0][0]=rA[0][0]*rB[0][0]+rC[0][0];


static void gemm_NoTransB_batch(THCState *state, Concurrency::array_view<float, 1> &A, long aOffset, long A_batchOffset,
                                Concurrency::array_view<float, 1> &B, long bOffset, long B_batchOffset,
                                Concurrency::array_view<float, 1> &C, long cOffset, long C_batchOffset,
                                int M, int N, int K, int lda, int ldb, int ldc,
                                float alpha, float beta, int batchSize)
{
  THCGPUDeviceState* device_state = state->deviceState;
  Concurrency::accelerator_view accl_view = device_state->get_current_accelerator_view();
  // If K is small then make use of threads and blocks across M and N dimension
  if (K - (M + N) > 10000)
  {
   // Make grid size in each dimension an exact multiple of threadblock size in the corresponding dimension
    Concurrency::extent<3> grdExt(batchSize, N, M * GEMM_BLOCK);
    Concurrency::tiled_extent<1, 1, GEMM_BLOCK> t_ext(grdExt);

    Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<1, 1, GEMM_BLOCK> tidx) restrict(amp)
    {
      int elt = tidx.tile[0];
      int threadIdx = tidx.local[2];
      int blockIdx = tidx.tile[2];
      int Row = tidx.tile[1];
      int Col = blockIdx;

      tile_static float sh[GEMM_BLOCK];
      sh[threadIdx] = 0;

      for (int tileId = 0; tileId < ((K + GEMM_BLOCK - 1) & ~(GEMM_BLOCK - 1)) / GEMM_BLOCK; tileId++)
      {
        if (tileId * GEMM_BLOCK + threadIdx < K && Col < M && Row < N)
          sh[threadIdx] += A[aOffset + A_batchOffset * elt + Col * K + tileId * GEMM_BLOCK + threadIdx] * B[bOffset + B_batchOffset * elt + Row * K + tileId * GEMM_BLOCK + threadIdx];
      }
      tidx.barrier.wait();
      for (int stride = GEMM_BLOCK / 2; stride >= 1; stride /= 2)
      {
        if (threadIdx < stride)
          sh[threadIdx] += sh[threadIdx + stride];
        tidx.barrier.wait();
      }

      if(threadIdx == 0 && Col < M && Row < N)
      {
        C[cOffset + C_batchOffset * elt + Row * M + Col] *= beta;
        C[cOffset + C_batchOffset * elt + Row * M + Col] += sh[0] * alpha;
      }
    });
  }
  else if (M-N > 500)
  {
#define MICROTILESIZE_BATCH_A 4
#define MICROTILESIZE_BATCH_B 1
    Concurrency::extent<3> grdExt(batchSize, ((N / MICROTILESIZE_BATCH_B) + (TILESIZE_BATCH - 1)) & ~(TILESIZE_BATCH - 1), ((M / MICROTILESIZE_BATCH_A) + (TILESIZE_BATCH - 1)) & ~(TILESIZE_BATCH - 1));
    Concurrency::tiled_extent<1, TILESIZE_BATCH, TILESIZE_BATCH> t_ext(grdExt);

    Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<1, TILESIZE_BATCH, TILESIZE_BATCH> tidx) restrict(amp)
    {
      float rC[MICROTILESIZE_BATCH_A][MICROTILESIZE_BATCH_B] = {{0}};
      float rA[1][MICROTILESIZE_BATCH_A];
      float rB[1][MICROTILESIZE_BATCH_B];
      tile_static float lA[TILESIZE_BATCH * TILESIZE_BATCH * MICROTILESIZE_BATCH_A];
      tile_static float lB[TILESIZE_BATCH * TILESIZE_BATCH * MICROTILESIZE_BATCH_B];
      int elt = tidx.tile[0];
      int gidx = tidx.tile[2];
      int gidy = tidx.tile[1];
      int idx = tidx.local[2];
      int idy = tidx.local[1];
      int idt = TILESIZE_BATCH * idy + idx;
      int idxT = idt % TILESIZE_BATCH;
      int idyT = idt / TILESIZE_BATCH;
      int block_k = 0;
      do
      {
        tidx.barrier.wait();
        for (int sec = 0; sec < MICROTILESIZE_BATCH_B; ++sec)
        {
          if (gidy * TILESIZE_BATCH * MICROTILESIZE_BATCH_B + idxT + (sec * TILESIZE_BATCH) < N && block_k * TILESIZE_BATCH + idyT < K)
          {
            lB[(idyT * TILESIZE_BATCH * MICROTILESIZE_BATCH_B) + idxT + (sec * TILESIZE_BATCH)] = B[bOffset + B_batchOffset * elt + (gidy * TILESIZE_BATCH * MICROTILESIZE_BATCH_B + idxT + sec * TILESIZE_BATCH) * ldb + idyT + block_k * TILESIZE_BATCH];
          }
          else
          {
            lB[(idyT * TILESIZE_BATCH * MICROTILESIZE_BATCH_B) + idxT + (sec * TILESIZE_BATCH)] = 0;
   	  }
        }

        for (int sec = 0; sec < MICROTILESIZE_BATCH_A; ++sec)
        {
          if (gidx * TILESIZE_BATCH * MICROTILESIZE_BATCH_A + idxT + (sec * TILESIZE_BATCH) < M && block_k * TILESIZE_BATCH + idyT < K)
          {
            lA[(idyT * TILESIZE_BATCH * MICROTILESIZE_BATCH_A) + idxT + (sec * TILESIZE_BATCH)] = A[aOffset + A_batchOffset * elt + (gidx * TILESIZE_BATCH * MICROTILESIZE_BATCH_A + idxT + sec * TILESIZE_BATCH) * lda +  idyT + block_k * TILESIZE_BATCH];
          }
          else
          {
            lA[(idyT * TILESIZE_BATCH * MICROTILESIZE_BATCH_A) + idxT + (sec * TILESIZE_BATCH)] = 0;
          }
        }
        tidx.barrier.wait();

        int offA = idx;
        int offB = idy;
        for (int iter=0; iter < TILESIZE_BATCH; ++iter)
        {
          MTS_BATCH;
        }
        tidx.barrier.wait();
      } while (++block_k < (((K + TILESIZE_BATCH - 1) & ~(TILESIZE_BATCH - 1))/TILESIZE_BATCH));

      int xIndex = gidx * TILESIZE_BATCH * MICROTILESIZE_BATCH_A + idx;
      int yIndex = (gidy * TILESIZE_BATCH * MICROTILESIZE_BATCH_B + idy) * ldc;
      for (int row = 0; row < MICROTILESIZE_BATCH_B; row++)
      {
        for (int col = 0; col < MICROTILESIZE_BATCH_A ; col++)
        {
          if (xIndex + (TILESIZE_BATCH * col) < M && (yIndex / ldc) + (TILESIZE_BATCH * row) < N)
            C[cOffset + C_batchOffset * elt + (xIndex + TILESIZE_BATCH * col) + yIndex + (TILESIZE_BATCH * row) * ldc] = alpha * rC[col][row] + beta * C[cOffset + C_batchOffset * elt + (xIndex + TILESIZE_BATCH * col) + yIndex + (TILESIZE_BATCH * row) * ldc];
        }
      }
    });
#undef MICROTILESIZE_BATCH_A 
#undef MICROTILESIZE_BATCH_B
  }
  else if (K ==1)
  {
    Concurrency::extent<3> grdExt(batchSize, (N + (TILE_SZ - 1)) & ~(TILE_SZ - 1), (M + (TILE_SZ - 1)) & ~(TILE_SZ - 1));
    Concurrency::tiled_extent<1, TILE_SZ, TILE_SZ> t_ext(grdExt);

    Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<1, TILE_SZ, TILE_SZ> tidx) restrict(amp)
    {
      int elt = tidx.tile[0];
      float rC[1][1];
      float rA[1][1];
      float rB[1][1];
      tile_static float lA[TILE_SZ*TILE_SZ];
      tile_static float lB[TILE_SZ*TILE_SZ];
      rC[0][0] = 0;
      int gidx = tidx.tile[2];
      int gidy = tidx.tile[1];
      int idx = tidx.local[2];
      int idy = tidx.local[1];
      int idt = TILE_SZ*idy + idx;
      int idxT = idt % TILE_SZ;
      int idyT = idt / TILE_SZ;
      int block_k = 0;
      do
      {
        if (gidy*TILE_SZ+idyT < N && block_k*TILE_SZ+idxT < K && elt < batchSize)
        {
          lB[idxT+idyT*TILE_SZ] = B[bOffset + B_batchOffset * elt + (gidy*TILE_SZ+ idyT)*ldb + idxT + block_k * TILE_SZ];
        }
        else
          lB[idxT+idyT*TILE_SZ] = 0;

        tidx.barrier.wait();

        if (gidx*TILE_SZ+idyT < M && block_k*TILE_SZ+idxT < K && elt < batchSize)
        {
          lA[idxT+idyT*TILE_SZ] = A[aOffset + A_batchOffset * elt + (gidx*TILE_SZ+ idyT)*lda + idxT + block_k * TILE_SZ];
        }
        else
          lA[idxT+idyT*TILE_SZ] = 0;

        tidx.barrier.wait();

        int offA = idx*TILE_SZ;
        int offB = idy*TILE_SZ;

        for (int i = 0; i < TILE_SZ/8; i++)
        {
          M1x1
          M1x1
          M1x1
          M1x1
          M1x1
          M1x1
          M1x1
          M1x1
        }

        tidx.barrier.wait();
      } while (++block_k < (((K + TILE_SZ - 1) & ~(TILE_SZ - 1))/TILE_SZ));

      if (gidx*TILE_SZ+idx < M && gidy*TILE_SZ+idy < N && elt < batchSize)
          C[cOffset + C_batchOffset * elt + (gidx*TILE_SZ +idx) + (gidy*TILE_SZ + idy)*ldc] = alpha*rC[0][0] + beta*C[cOffset + C_batchOffset * elt + (gidx*TILE_SZ+idx) + (gidy*TILE_SZ + idy)*ldc];
    }); 
  }
  else
  {
#define MICROTILESIZE_A 3 
#define MICROTILESIZE_B 1 
    Concurrency::extent<3> grdExt(batchSize, ((N / MICROTILESIZE_B) + (TILESIZE - 1)) & ~(TILESIZE - 1), ((M / MICROTILESIZE_A) + (TILESIZE - 1)) & ~(TILESIZE - 1));
    Concurrency::tiled_extent<1, TILESIZE, TILESIZE> t_ext(grdExt);

    Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<1, TILESIZE, TILESIZE> tidx) restrict(amp)
    {
      int elt = tidx.tile[0];
      float rC[MICROTILESIZE_A][MICROTILESIZE_B] = {{0}};
      float rA[1][MICROTILESIZE_A];
      float rB[1][MICROTILESIZE_B];
      tile_static float lA[TILESIZE * TILESIZE * MICROTILESIZE_A];
      tile_static float lB[TILESIZE * TILESIZE * MICROTILESIZE_B];
      int gidx = tidx.tile[2];
      int gidy = tidx.tile[1];
      int idx = tidx.local[2];
      int idy = tidx.local[1];
      int idt = TILESIZE * idy + idx;
      int idxT = idt % TILESIZE;
      int idyT = idt / TILESIZE;

      int block_k = 0;
      do
      {
        for (int sec = 0; sec < MICROTILESIZE_B; ++sec)
        {
          if (gidy * TILESIZE * MICROTILESIZE_B + idxT + (sec * TILESIZE) < N && block_k * TILESIZE + idyT < K)
          {
            lB[(idyT * TILESIZE * MICROTILESIZE_B) + idxT + (sec * TILESIZE)] = B[bOffset + B_batchOffset * elt + (gidy * TILESIZE * MICROTILESIZE_B + idxT + sec * TILESIZE) * ldb + idyT + block_k * TILESIZE];
          }
          else
          {
            lB[(idyT * TILESIZE * MICROTILESIZE_B) + idxT + (sec * TILESIZE)] = 0;
          }
        }

        for (int sec = 0; sec < MICROTILESIZE_A; ++sec)
        {
          if (gidx * TILESIZE * MICROTILESIZE_A + idxT + (sec * TILESIZE) < M && block_k * TILESIZE + idyT < K)
          {
            lA[(idyT * TILESIZE * MICROTILESIZE_A) + idxT + (sec * TILESIZE)] = A[aOffset + A_batchOffset * elt + (gidx * TILESIZE * MICROTILESIZE_A + idxT + sec * TILESIZE) * lda +  idyT + block_k * TILESIZE];
          }
          else
          {
            lA[(idyT * TILESIZE * MICROTILESIZE_A) + idxT + (sec * TILESIZE)] = 0;
          }
        }
        tidx.barrier.wait();

        int offA = idx;
        int offB = idy;
        for (int iter=0; iter < TILESIZE; ++iter)
        {
          MTS;
        }
        tidx.barrier.wait();
      } while (++block_k < (((K + TILESIZE - 1) & ~(TILESIZE - 1))/TILESIZE));

      int xIndex = gidx * TILESIZE * MICROTILESIZE_A + idx;
      int yIndex = gidy * TILESIZE * MICROTILESIZE_B + idy;
      for (int col = 0; col < MICROTILESIZE_A; col++)
      {
        for (int row = 0; row < MICROTILESIZE_B ; row++)
        {
        if (xIndex + (TILESIZE * col) < M && (yIndex) + (TILESIZE * row) < N)
          C[cOffset + C_batchOffset * elt + (xIndex + TILESIZE * col) + (yIndex + TILESIZE * row) * ldc] = alpha * rC[col][row] + beta * C[cOffset + C_batchOffset * elt + (xIndex + TILESIZE * col) + (yIndex + TILESIZE * row) * ldc];
        }
      }
   });
#undef MICROTILESIZE_A
#undef MICROTILESIZE_B
  }
}

#define TILESIZE_NT 16
#define MICROTILESIZE_NT 2

#define  MTS_NT                                                                \
           for(int iter = 0; iter < MICROTILESIZE_NT ; iter++)                 \
           {                                                                   \
             rA[0][iter] = lA[offA + (iter * TILESIZE_NT)];                    \
             rB[0][iter] = lB[offB + (iter * TILESIZE_NT)];                    \
           }                                                                   \
           for(int rowIndex = 0; rowIndex < MICROTILESIZE_NT ; rowIndex++)     \
           {                                                                   \
           for(int colIndex = 0; colIndex < MICROTILESIZE_NT ; colIndex++)     \
           {                                                                   \
           rC[rowIndex][colIndex] = rA[0][rowIndex] * rB[0][colIndex] +        \
                                    rC[rowIndex][colIndex];                    \
           }                                                                   \
           }                                                                   \
           offA += (MICROTILESIZE_NT * TILESIZE_NT);                           \
           offB += (MICROTILESIZE_NT * TILESIZE_NT);                           \

// Matrix Multiplication when Matrix B is transposed
static void gemm_NoTransA(THCState *state, Concurrency::array_view<float, 1> &A, long aOffset,
                          Concurrency::array_view<float, 1> &B, long bOffset,
                          Concurrency::array_view<float, 1> &C, long cOffset,
                          int M, int N, int K, int lda, int ldb, int ldc,
                          float alpha, float beta)
{
  THCGPUDeviceState* device_state = state->deviceState;
  Concurrency::accelerator_view accl_view = device_state->get_current_accelerator_view();
  if (M < 1000 || N < 1000)
  {
    // Make grid size in each dimension an exact multiple of threadblock size in the corresponding dimension
    Concurrency::extent<2> grdExt((N + (THREADS - 1)) & ~(THREADS - 1), (M + (THREADS - 1)) & ~(THREADS - 1));
    Concurrency::tiled_extent<THREADS, THREADS> t_ext(grdExt);

    Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<THREADS, THREADS> tidx) restrict(amp)
    {
      float CValue = 0;
      int Row = tidx.tile[0] * TILE_DIM + tidx.local[0];
      int Col = tidx.tile[1] * TILE_DIM + tidx.local[1];
      tile_static float As[TILE_DIM][TILE_DIM];
      tile_static float Bs[TILE_DIM][TILE_DIM];
      for (int k = 0; k < (TILE_DIM + K - 1) / TILE_DIM; k++)
      {
        // Read Matrix B from global to shared tile
        if (k * TILE_DIM + tidx.local[0] < K && (tidx.tile[0] * TILE_DIM + tidx.local[1]) < N)
          Bs[tidx.local[0]][tidx.local[1]] = B[bOffset + (k * TILE_DIM + tidx.local[0]) * N + (tidx.tile[0] * TILE_DIM + tidx.local[1])];
        else
          Bs[tidx.local[0]][tidx.local[1]] = 0.0;

        // Read Matrix A from global to shared tile
        if (k*TILE_DIM + tidx.local[0] < K && Col < M)
          As[tidx.local[0]][tidx.local[1]] = A[aOffset + (k * TILE_DIM + tidx.local[0]) * M + Col];
        else
          As[tidx.local[0]][tidx.local[1]] = 0.0;

        tidx.barrier.wait();

        for (int n = 0; n < TILE_DIM; ++n)
          CValue += Bs[n][tidx.local[0]] * As[n][tidx.local[1]];

        tidx.barrier.wait();
      }

      if (Row < N && Col < M)
      {
        C[cOffset + (Row * M)+Col] *= beta;
        C[cOffset + (Row * M)+Col] += CValue * alpha;
      }
    });
  }
  else
  {
    Concurrency::extent<2> grdExt(((N / 2) + (TILESIZE_NT - 1)) & ~(TILESIZE_NT - 1), ((M / 2) + (TILESIZE_NT - 1)) & ~(TILESIZE_NT - 1));
    Concurrency::tiled_extent<TILESIZE_NT, TILESIZE_NT> t_ext(grdExt);
    Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<TILESIZE_NT, TILESIZE_NT> tidx) restrict(amp)
    {
      float rC[MICROTILESIZE_NT][MICROTILESIZE_NT] = {{0}};
      float rA[1][MICROTILESIZE_NT];
      float rB[1][MICROTILESIZE_NT];
      tile_static float lA[TILESIZE_NT * TILESIZE_NT * MICROTILESIZE_NT];
      tile_static float lB[TILESIZE_NT * TILESIZE_NT * MICROTILESIZE_NT];
      int gidx = tidx.tile[1];
      int gidy = tidx.tile[0];
      int idx = tidx.local[1];
      int idy = tidx.local[0];
      int idt = TILESIZE_NT * idy + idx;
      int idxT = idt % TILESIZE_NT;
      int idyT = idt / TILESIZE_NT;
      int block_k = 0;
      do
      {
        tidx.barrier.wait();

        for (int sec = 0; sec < MICROTILESIZE_NT; ++sec)
        {
          if (gidy * TILESIZE_NT * MICROTILESIZE_NT + idxT + (sec * TILESIZE_NT) < N && block_k * TILESIZE_NT + idyT < K)
          {
            lB[(idyT * TILESIZE_NT * MICROTILESIZE_NT) + idxT + (sec * TILESIZE_NT)] = B[bOffset + (gidy * TILESIZE_NT * MICROTILESIZE_NT) + idxT + (sec * TILESIZE_NT) + idyT * ldb + block_k * (ldb * TILESIZE_NT)];
          }
          else
          {
            lB[(idyT * TILESIZE_NT * MICROTILESIZE_NT) + idxT + (sec * TILESIZE_NT)] = 0;
  	  }

          if (gidx * TILESIZE_NT * MICROTILESIZE_NT + idxT + (sec * TILESIZE_NT) < M && block_k * TILESIZE_NT + idyT < K)
          {
            lA[(idyT * TILESIZE_NT * MICROTILESIZE_NT) + idxT + (sec * TILESIZE_NT)] = A[aOffset + (gidx * TILESIZE_NT * MICROTILESIZE_NT) + idxT + (sec * TILESIZE_NT) +  idyT * lda + block_k * (lda * TILESIZE_NT)];
          }
          else
          {
            lA[(idyT * TILESIZE_NT * MICROTILESIZE_NT) + idxT + (sec * TILESIZE_NT)] = 0;
          }
        }

        tidx.barrier.wait();

        int offA = idx;
        int offB = idy;
        for (int iter=0; iter < TILESIZE_NT; ++iter)
        {
          MTS_NT
        }
        tidx.barrier.wait();
      } while (++block_k < (((K + TILESIZE_NT - 1) & ~(TILESIZE_NT - 1))/TILESIZE_NT));

      int xIndex = gidx * TILESIZE_NT * MICROTILESIZE_NT + idx;
      int yIndex = (gidy * TILESIZE_NT * MICROTILESIZE_NT + idy) * ldc;
      for (int row = 0; row < MICROTILESIZE_NT; row++)
      {
        for (int col = 0; col < MICROTILESIZE_NT; col++)
        {
        if (xIndex + (TILESIZE_NT * col) < M && (yIndex / ldc) + (TILESIZE_NT * row) < N)
          C[cOffset + (xIndex + TILESIZE_NT * col) + yIndex + (TILESIZE_NT * row) * ldc] = alpha * rC[col][row] + beta * C[cOffset + (xIndex + TILESIZE_NT * col) + yIndex + (TILESIZE_NT * row) * ldc];
        }
      }
    });
  }
}

static void gemm_NoTransA_batch(THCState *state, Concurrency::array_view<float, 1> &A, long aOffset, long A_batchOffset,
                                Concurrency::array_view<float, 1> &B, long bOffset, long B_batchOffset,
                                Concurrency::array_view<float, 1> &C, long cOffset, long C_batchOffset,
                                int M, int N, int K, int lda, int ldb, int ldc,
                                float alpha, float beta, int batchSize)
{
  THCGPUDeviceState* device_state = state->deviceState;
  Concurrency::accelerator_view accl_view = device_state->get_current_accelerator_view();
  if (M - N > 500)
  {
#define MICROTILESIZE_BATCH_A 4
#define MICROTILESIZE_BATCH_B 1
    Concurrency::extent<3> grdExt(batchSize, ((N / MICROTILESIZE_BATCH_B) + (TILESIZE_BATCH - 1)) & ~(TILESIZE_BATCH - 1), ((M / MICROTILESIZE_BATCH_A) + (TILESIZE_BATCH - 1)) & ~(TILESIZE_BATCH - 1));
    Concurrency::tiled_extent<1, TILESIZE_BATCH, TILESIZE_BATCH> t_ext(grdExt);
    Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<1, TILESIZE_BATCH, TILESIZE_BATCH> tidx) restrict(amp)
    {
      float rC[MICROTILESIZE_BATCH_A][MICROTILESIZE_BATCH_B] = {{0}};
      float rA[1][MICROTILESIZE_BATCH_A];
      float rB[1][MICROTILESIZE_BATCH_B];
      tile_static float lA[TILESIZE_BATCH * TILESIZE_BATCH * MICROTILESIZE_BATCH_A];
      tile_static float lB[TILESIZE_BATCH * TILESIZE_BATCH * MICROTILESIZE_BATCH_B];
      int elt = tidx.tile[0];
      int gidx = tidx.tile[2];
      int gidy = tidx.tile[1];
      int idx = tidx.local[2];
      int idy = tidx.local[1];
      int idt = TILESIZE_BATCH * idy + idx;
      int idxT = idt % TILESIZE_BATCH;
      int idyT = idt / TILESIZE_BATCH;
      int block_k = 0;
      do
      {
        tidx.barrier.wait();

        for (int sec = 0; sec < MICROTILESIZE_BATCH_B; ++sec)
        {
          if (gidy * TILESIZE_BATCH * MICROTILESIZE_BATCH_B + idxT + (sec * TILESIZE_BATCH) < N && block_k * TILESIZE_BATCH + idyT < K)
          {
            lB[(idyT * TILESIZE_BATCH * MICROTILESIZE_BATCH_B) + idxT + (sec * TILESIZE_BATCH)] = B[bOffset + B_batchOffset * elt + (gidy * TILESIZE_BATCH * MICROTILESIZE_BATCH_B) + idxT + (sec * TILESIZE_BATCH) + idyT * ldb + block_k * (ldb * TILESIZE_BATCH)];
          }
          else
          {
            lB[(idyT * TILESIZE_BATCH * MICROTILESIZE_BATCH_B) + idxT + (sec * TILESIZE_BATCH)] = 0;
  	  }
        }

        for (int sec = 0; sec < MICROTILESIZE_BATCH_A; ++sec)
        {
          if (gidx * TILESIZE_BATCH * MICROTILESIZE_BATCH_A + idxT + (sec * TILESIZE_BATCH) < M && block_k * TILESIZE_BATCH + idyT < K)
          {
            lA[(idyT * TILESIZE_BATCH * MICROTILESIZE_BATCH_A) + idxT + (sec * TILESIZE_BATCH)] = A[aOffset + A_batchOffset * elt + (gidx * TILESIZE_BATCH * MICROTILESIZE_BATCH_A) + idxT + (sec * TILESIZE_BATCH) +  idyT * lda + block_k * (lda * TILESIZE_BATCH)];
          }
          else
          {
            lA[(idyT * TILESIZE_BATCH * MICROTILESIZE_BATCH_A) + idxT + (sec * TILESIZE_BATCH)] = 0;
          }
        }

        tidx.barrier.wait();

        int offA = idx;
        int offB = idy;
        for (int iter=0; iter < TILESIZE_BATCH; ++iter)
        {
          MTS_BATCH
        }
        tidx.barrier.wait();
      } while (++block_k < (((K + TILESIZE_BATCH - 1) & ~(TILESIZE_BATCH - 1))/TILESIZE_BATCH));

      int xIndex = gidx * TILESIZE_BATCH * MICROTILESIZE_BATCH_A + idx;
      int yIndex = (gidy * TILESIZE_BATCH * MICROTILESIZE_BATCH_B + idy) * ldc;
      for (int row = 0; row < MICROTILESIZE_BATCH_B; row++)
      {
        for (int col = 0; col < MICROTILESIZE_BATCH_A; col++)
        {
          if (xIndex + (TILESIZE_BATCH * col) < M && (yIndex / ldc) + (TILESIZE_BATCH * row) < N)
            C[cOffset + C_batchOffset * elt + (xIndex + TILESIZE_BATCH * col) + yIndex + (TILESIZE_BATCH * row) * ldc] = alpha * rC[col][row] + beta * C[cOffset + C_batchOffset * elt + (xIndex + TILESIZE_BATCH * col) + yIndex + (TILESIZE_BATCH * row) * ldc];
        }
      }
    });
#undef MICROTILESIZE_BATCH_A
#undef MICROTILESIZE_BATCH_B
  }
  else if ( N - M > 400)
  {
#define MICROTILESIZE_BATCH_A 1
#define MICROTILESIZE_BATCH_B 4
    Concurrency::extent<3> grdExt(batchSize, ((N / MICROTILESIZE_BATCH_B) + (TILESIZE_BATCH - 1)) & ~(TILESIZE_BATCH - 1), ((M / MICROTILESIZE_BATCH_A) + (TILESIZE_BATCH - 1)) & ~(TILESIZE_BATCH - 1));
    Concurrency::tiled_extent<1, TILESIZE_BATCH, TILESIZE_BATCH> t_ext(grdExt);
    Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<1, TILESIZE_BATCH, TILESIZE_BATCH> tidx) restrict(amp)
    {
      float rC[MICROTILESIZE_BATCH_A][MICROTILESIZE_BATCH_B] = {{0}};
      float rA[1][MICROTILESIZE_BATCH_A];
      float rB[1][MICROTILESIZE_BATCH_B];
      tile_static float lA[TILESIZE_BATCH * TILESIZE_BATCH * MICROTILESIZE_BATCH_A];
      tile_static float lB[TILESIZE_BATCH * TILESIZE_BATCH * MICROTILESIZE_BATCH_B];
      int elt = tidx.tile[0];
      int gidx = tidx.tile[2];
      int gidy = tidx.tile[1];
      int idx = tidx.local[2];
      int idy = tidx.local[1];
      int idt = TILESIZE_BATCH * idy + idx;
      int idxT = idt % TILESIZE_BATCH;
      int idyT = idt / TILESIZE_BATCH;
      int block_k = 0;
      do
      {
        tidx.barrier.wait();

        for (int sec = 0; sec < MICROTILESIZE_BATCH_B; ++sec)
        {
          if (gidy * TILESIZE_BATCH * MICROTILESIZE_BATCH_B + idxT + (sec * TILESIZE_BATCH) < N && block_k * TILESIZE_BATCH + idyT < K)
          {
            lB[(idyT * TILESIZE_BATCH * MICROTILESIZE_BATCH_B) + idxT + (sec * TILESIZE_BATCH)] = B[bOffset + B_batchOffset * elt + (gidy * TILESIZE_BATCH * MICROTILESIZE_BATCH_B) + idxT + (sec * TILESIZE_BATCH) + idyT * ldb + block_k * (ldb * TILESIZE_BATCH)];
          }
          else
          {
            lB[(idyT * TILESIZE_BATCH * MICROTILESIZE_BATCH_B) + idxT + (sec * TILESIZE_BATCH)] = 0;
  	  }
        }

        for (int sec = 0; sec < MICROTILESIZE_BATCH_A; ++sec)
        {
          if (gidx * TILESIZE_BATCH * MICROTILESIZE_BATCH_A + idxT + (sec * TILESIZE_BATCH) < M && block_k * TILESIZE_BATCH + idyT < K)
          {
            lA[(idyT * TILESIZE_BATCH * MICROTILESIZE_BATCH_A) + idxT + (sec * TILESIZE_BATCH)] = A[aOffset + A_batchOffset * elt + (gidx * TILESIZE_BATCH * MICROTILESIZE_BATCH_A) + idxT + (sec * TILESIZE_BATCH) +  idyT * lda + block_k * (lda * TILESIZE_BATCH)];
          }
          else
          {
            lA[(idyT * TILESIZE_BATCH * MICROTILESIZE_BATCH_A) + idxT + (sec * TILESIZE_BATCH)] = 0;
          }
        }

        tidx.barrier.wait();

        int offA = idx;
        int offB = idy;
        for (int iter=0; iter < TILESIZE_BATCH; ++iter)
        {
          MTS_BATCH
        }
        tidx.barrier.wait();
      } while (++block_k < (((K + TILESIZE_BATCH - 1) & ~(TILESIZE_BATCH - 1))/TILESIZE_BATCH));

      int xIndex = gidx * TILESIZE_BATCH * MICROTILESIZE_BATCH_A + idx;
      int yIndex = (gidy * TILESIZE_BATCH * MICROTILESIZE_BATCH_B + idy) * ldc;
      for (int row = 0; row < MICROTILESIZE_BATCH_B; row++)
      {
        for (int col = 0; col < MICROTILESIZE_BATCH_A; col++)
        {
          if (xIndex + (TILESIZE_BATCH * col) < M && (yIndex / ldc) + (TILESIZE_BATCH * row) < N)
            C[cOffset + C_batchOffset * elt + (xIndex + TILESIZE_BATCH * col) + yIndex + (TILESIZE_BATCH * row) * ldc] = alpha * rC[col][row] + beta * C[cOffset + C_batchOffset * elt + (xIndex + TILESIZE_BATCH * col) + yIndex + (TILESIZE_BATCH * row) * ldc];
        }
      }
    });
#undef MICROTILESIZE_BATCH_A
#undef MICROTILESIZE_BATCH_B
  }
  else
  {
    Concurrency::extent<3> grdExt(batchSize, (N + (THREADS - 1)) & ~(THREADS - 1), (M + (THREADS - 1)) & ~(THREADS - 1));
    Concurrency::tiled_extent<1, THREADS, THREADS> t_ext(grdExt);

    Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<1, THREADS, THREADS> tidx) restrict(amp)
    {
      int elt = tidx.tile[0];
      float CValue = 0;
      int Row = tidx.tile[1] * TILE_DIM + tidx.local[1];
      int Col = tidx.tile[2] * TILE_DIM + tidx.local[2];
      tile_static float As[TILE_DIM][TILE_DIM];
      tile_static float Bs[TILE_DIM][TILE_DIM];
      for (int k = 0; k < (TILE_DIM + K - 1) / TILE_DIM; k++)
      {
        // Read Matrix B from global to shared tile
        if (k * TILE_DIM + tidx.local[1] < K && (tidx.tile[1] * TILE_DIM + tidx.local[2]) < N)
          Bs[tidx.local[1]][tidx.local[2]] = B[bOffset + B_batchOffset * elt + (k * TILE_DIM + tidx.local[1]) * N + (tidx.tile[1] * TILE_DIM + tidx.local[2])];
        else
          Bs[tidx.local[1]][tidx.local[2]] = 0.0;

        // Read Matrix A from global to shared tile
        if (k*TILE_DIM + tidx.local[1] < K && Col < M)
          As[tidx.local[1]][tidx.local[2]] = A[aOffset + A_batchOffset * elt + (k * TILE_DIM + tidx.local[1]) * M + Col];
        else
          As[tidx.local[1]][tidx.local[2]] = 0.0;

        tidx.barrier.wait();

        for (int n = 0; n < TILE_DIM; ++n)
          CValue += Bs[n][tidx.local[1]] * As[n][tidx.local[2]];

        tidx.barrier.wait();
      }

      if (Row < N && Col < M)
      {
        C[cOffset + C_batchOffset * elt + (Row * M)+Col] *= beta;
        C[cOffset + C_batchOffset * elt + (Row * M)+Col] += CValue * alpha;
      }
    });
  }
}

// Matrix Multiplication when both A and B are transposed
static void gemm_TransAB(THCState *state, Concurrency::array_view<float, 1> &A, long aOffset,
                         Concurrency::array_view<float, 1> &B, long bOffset,
                         Concurrency::array_view<float, 1> &C, long cOffset,
                         int M, int N, int K, long lda, long ldb, long ldc,
                         float alpha, float beta)
{
  // Make grid size in each dimension an exact multiple of threadblock size in the corresponding dimension
  Concurrency::extent<2> grdExt((N + (THREADS - 1)) & ~(THREADS - 1), (M + (THREADS - 1)) & ~(THREADS - 1));
  Concurrency::tiled_extent<THREADS, THREADS> t_ext(grdExt);

  THCGPUDeviceState* device_state = state->deviceState;
  Concurrency::accelerator_view accl_view = device_state->get_current_accelerator_view();
  Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<THREADS, THREADS> tidx) restrict(amp)
  {
    float temp;
    int j = tidx.global[0];
    int i = tidx.global[1];
    if(i < M && j < N)
    {
      temp = 0;
      for (int l = 0; l < K; ++l)
        temp += A[aOffset + l + i * lda] * B[bOffset + j + l * ldb];

      C[cOffset + i + j * ldc] = alpha * temp + beta * C[cOffset + i + j * ldc];
    }
  });
}

// API used in torch to invoke AMP gemm operation
void THGPUBlas_gemm(THCState *state, char TransA, char TransB, const long M, const long N, const long K, const float alpha,
             Concurrency::array_view<float> &A_mat, long aOffset, long lda,
             Concurrency::array_view<float> &B_mat, long bOffset, long ldb, const float beta,
             Concurrency::array_view<float> &C_mat, long cOffset, long ldc)
{
  int i, j;

  // Quick return if possible
  if (!M || !N || ((alpha == 0 || !K) && beta == 1)) 
    return ;
  // For alpha = 0
  if (alpha == 0)
  {
    if (beta == 0)
    {
      for (j = 0; j < N; ++j)
        for (i = 0; i < M; ++i)
          C_mat[i + j * ldc] = 0;
    }
    else
    {
      for (j = 0; j < N; ++j)
        for (i = 0; i < M; ++i)
          C_mat[i + j * ldc] *= beta;
    }
    return ;
  }

  if (TransB == 'n')
  {
    if (TransA == 'n')
      gemm_NoTransAB(state, A_mat, aOffset, B_mat, bOffset, C_mat, cOffset, M, N, K, lda, ldb, ldc, alpha, beta);
    else
      gemm_NoTransB(state, A_mat, aOffset, B_mat, bOffset, C_mat, cOffset, M, N, K, lda, ldb, ldc, alpha, beta);
  }
  else if (TransA == 'n')
    gemm_NoTransA(state, A_mat, aOffset, B_mat, bOffset, C_mat, cOffset, M, N, K, lda, ldb, ldc, alpha, beta);
  else
    gemm_TransAB(state, A_mat, aOffset, B_mat, bOffset, C_mat, cOffset, M, N, K, lda, ldb, ldc, alpha, beta);
}

void THGPUBlas_gemm_batch(THCState *state, char TransA, char TransB, const long M, const long N, const long K, const float alpha,
                          Concurrency::array_view<float> &A_mat, long aOffset, long A_batchOffset, long lda,
                          Concurrency::array_view<float> &B_mat, long bOffset, long B_batchOffset, long ldb, const float beta,
                          Concurrency::array_view<float> &C_mat, long cOffset, long C_batchOffset, long ldc, long batchSize)
{
  int i, j;

  // Quick return if possible
  if (!M || !N || ((alpha == 0 || !K) && beta == 1)) 
    return ;
  // For alpha = 0
  if (alpha == 0)
  {
    if (beta == 0)
    {
      for (j = 0; j < N; ++j)
        for (i = 0; i < M; ++i)
          C_mat[i + j * ldc] = 0;
    }
    else
    {
      for (j = 0; j < N; ++j)
        for (i = 0; i < M; ++i)
          C_mat[i + j * ldc] *= beta;
    }
    return ;
  }

  if (TransB == 'n')
  {
    if (TransA == 'n')
      gemm_NoTransAB_batch(state, A_mat, aOffset, A_batchOffset, B_mat, bOffset, B_batchOffset, C_mat, cOffset, C_batchOffset, M, N, K, lda, ldb, ldc, alpha, beta, batchSize);
    else
      gemm_NoTransB_batch(state, A_mat, aOffset, A_batchOffset,  B_mat, bOffset, B_batchOffset, C_mat, cOffset, C_batchOffset,  M, N, K, lda, ldb, ldc, alpha, beta, batchSize);
  }
  else if (TransA == 'n')
    gemm_NoTransA_batch(state, A_mat, aOffset,A_batchOffset, B_mat, bOffset, B_batchOffset, C_mat, cOffset,C_batchOffset, M, N, K, lda, ldb, ldc, alpha, beta, batchSize);
  else
    gemm_TransAB(state, A_mat, aOffset, B_mat, bOffset, C_mat, cOffset, M, N, K, lda, ldb, ldc, alpha, beta);
}

// Matrix Vector Multiplication where the Matrix A is transposed
static void gemv_TransA(THCState *state, Concurrency::array_view<float> &A_mat, int aOffset,
                        Concurrency::array_view<float> &X_vec, long xOffset,
                        Concurrency::array_view<float> &Y_vec, long yOffset,
                        float alpha, float beta, int lenX, int lenY,
                        Concurrency::array_view<float> &tempBuf)
{
  THCGPUDeviceState* device_state = state->deviceState;
  Concurrency::accelerator_view accl_view = device_state->get_current_accelerator_view();
  // Case where Y vector's length (lenY) is very small compared to lenX
  // TO DO: Need to represent this case in a better way
  // The parameter tempBuf is used in this case to make a global sychronization across threadblocks
  if((lenX - lenY) > 5000)
  {
    // Make grid size in each dimension an exact multiple of threadblock size in the corresponding dimension
    int len_X = (lenX + (BLOCK_SIZE - 1)) & ~(BLOCK_SIZE - 1);
    int num_blocks = len_X / BLOCK_SIZE;
    Concurrency::extent<1> grdExt(len_X);
    Concurrency::tiled_extent<BLOCK_SIZE> t_ext(grdExt);

    Concurrency::parallel_for_each(accl_view, t_ext,[=] (Concurrency::tiled_index<BLOCK_SIZE> tidx) restrict(amp)
    {
      tile_static float t[BLOCK_SIZE];
      for (int Col = 0; Col < lenY; Col++)
      {
        int blockIdx = tidx.tile[0];
        int threadIdx = tidx.local[0];
        tempBuf[Col * num_blocks + blockIdx] = 0;
        t[threadIdx] = 0;

        if (Col < lenY && blockIdx * BLOCK_SIZE + threadIdx < lenX)
          t[threadIdx] = X_vec[xOffset + blockIdx * BLOCK_SIZE + threadIdx] * A_mat[aOffset + Col * lenX + blockIdx * BLOCK_SIZE + threadIdx];

        tidx.barrier.wait();

        for (int stride = BLOCK_SIZE / 2; stride >= 1; stride /= 2)
        {
          if(threadIdx < stride)
            t[threadIdx] += t[threadIdx + stride];
        }
        tempBuf[Col * num_blocks + blockIdx] = t[0];
        tidx.barrier.wait();
      }

      if (tidx.tile[0] == 0)
      {
        for(int Col = 0; Col < lenY; Col++)
        {
          tile_static float sh[BLOCK_SIZE];
          int threadId = tidx.local[0];
          sh[tidx.local[0]] = 0;

          for (int i = threadId; i < num_blocks; i += tidx.tile_dim0)
            sh[threadId] += tempBuf[Col * num_blocks + i];

          tidx.barrier.wait();

          for (int stride = BLOCK_SIZE / 2; stride >= 1; stride /= 2)
          {
            if(threadId < stride)
              sh[threadId] += sh[threadId + stride];
          }
          tidx.barrier.wait();
          Y_vec[yOffset + Col] *= beta;
          Y_vec[yOffset + Col] += alpha * sh[0];
        }
      }
    });
  }
  else
  {
    // Make grid size in each dimension an exact multiple of threadblock size in the corresponding dimension
    Concurrency::extent<1> grdExt(lenY * BLOCK_SIZE);
    Concurrency::tiled_extent<BLOCK_SIZE> t_ext(grdExt);

    Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<BLOCK_SIZE> tidx) restrict(amp)
    {
      int threadIdx = tidx.local[0];
      int blockIdx = tidx.tile[0];
      int Col = blockIdx;

      tile_static float sh[BLOCK_SIZE];
      sh[threadIdx] = 0;

      for (int tileId = 0; tileId < ((lenX + BLOCK_SIZE - 1) & ~(BLOCK_SIZE - 1)) / BLOCK_SIZE; tileId++)
      {
        if (tileId * BLOCK_SIZE + threadIdx < lenX && Col < lenY)
          sh[threadIdx] += X_vec[xOffset + tileId * BLOCK_SIZE + threadIdx] * A_mat[aOffset + Col * lenX + tileId * BLOCK_SIZE + threadIdx];
      }
      tidx.barrier.wait();

      for (int stride = BLOCK_SIZE / 2; stride >= 1; stride /= 2)
      {
        if (threadIdx < stride)
          sh[threadIdx] += sh[threadIdx + stride];
        tidx.barrier.wait();
      }

      if(threadIdx == 0 && Col < lenY)
      {
        Y_vec[yOffset + Col] *= beta;
        Y_vec[yOffset + Col] += alpha * sh[0];
      }
    });
  }
}

static void gemv_NoTransA(THCState *state, Concurrency::array_view<float> &A, long aOffset,
                          Concurrency::array_view<float> &X, long xOffset,
                          Concurrency::array_view<float> &Y, long yOffset,
                          float alpha, float beta, int lenX, int lenY)
{
  THCGPUDeviceState* device_state = state->deviceState;
  Concurrency::accelerator_view accl_view = device_state->get_current_accelerator_view();
  // Make grid size in each dimension an exact multiple of threadblock size in the corresponding dimension
  long size = (lenY + 255) & ~255;
  Concurrency::extent<1> compute_domain(size);

  Concurrency::parallel_for_each(accl_view, compute_domain.tile<BLOCK_SIZE>(),[=] (Concurrency::tiled_index<BLOCK_SIZE> tidx) restrict(amp)
  {
    int bx = tidx.tile[0];
    int tx = tidx.local[0];
    tile_static float Xds[BLOCK_SIZE];
    int Col = bx * BLOCK_SIZE + tx;
    float Pvalue = 0;

    for (int m = 0; m < (lenX - 1) / BLOCK_SIZE + 1; ++m)
    {
      if (m * BLOCK_SIZE + tx < lenX)
        Xds[tx] = X[xOffset + m * BLOCK_SIZE + tx];
      else
        Xds[tx]=0;

      tidx.barrier.wait();

      for (int k = 0; k < BLOCK_SIZE; k++)
        if (Col < lenY && m * BLOCK_SIZE + k < lenX)
          Pvalue += Xds[k] * A[aOffset + Col + (m * BLOCK_SIZE + k) * lenY];

      tidx.barrier.wait();
    }
    if (Col < lenY)
    {
      Y[yOffset + Col] *= beta;
      Y[yOffset + Col] += alpha * Pvalue;
    }
  });
}

// API used in torch to invoke matrix vector multiplication
void THGPUBlas_gemv(THCState *state, char TransA, long M, long N, float alpha,
              Concurrency::array_view<float> &A, long aOffset,
              Concurrency::array_view<float> &X, long xOffset, long incX, float beta,
              Concurrency::array_view<float> &Y, long yOffset, long incY,
              Concurrency::array_view<float> &temp_buf)
{
  if (alpha == 0.0)
    return;

  int lenX, lenY;
  if (M == 0 || N == 0)
    return;

  if (alpha == 0.0 && beta == 1.0)
    return;

  if (TransA == 'n')
  {
    lenX = N;
    lenY = M;
  }
  else
  {
    lenX = M;
    lenY = N;
  }

  if (TransA == 't')
    gemv_TransA(state, A, aOffset, X, xOffset, Y, yOffset, alpha, beta, lenX, lenY, temp_buf);
  else if (TransA == 'n')
    gemv_NoTransA(state, A, aOffset, X, xOffset, Y, yOffset, alpha, beta, lenX, lenY);
}

// Scale vecotr X and add to vectory Y
void THGPUBlas_axpy(THCState *state, long n, float alpha,
              Concurrency::array_view<float> &X, long xOffset, long incx,
              Concurrency::array_view<float> &Y, long yOffset, long incy)
{
  // Make grid size in each dimension an exact multiple of threadblock size in the corresponding dimension
  if(n <= 20480)
  {
    long size = (n + BLOCK_SIZE - 1) & ~(BLOCK_SIZE - 1);
    Concurrency::extent<1> compute_domain(size);
    THCGPUDeviceState* device_state = state->deviceState;
    Concurrency::accelerator_view accl_view = device_state->get_current_accelerator_view();
    Concurrency::parallel_for_each(accl_view, compute_domain.tile<BLOCK_SIZE>(),[=] (Concurrency::tiled_index<BLOCK_SIZE> tidx) restrict(amp)
    {
      if(tidx.global[0] < n)
        Y[yOffset + tidx.global[0]] += X[xOffset + tidx.global[0]] * alpha;
    });
  }
  else
  {
    int step_sz;

    if (n <= 409600)
      step_sz = 3;
    else
      step_sz = 15;

    long size = (n/step_sz + BLOCK_SIZE - 1) & ~(BLOCK_SIZE - 1);
    long nBlocks = size/BLOCK_SIZE;
    Concurrency::extent<1> compute_domain(size);
    THCGPUDeviceState* device_state = state->deviceState;
    Concurrency::accelerator_view accl_view = device_state->get_current_accelerator_view();
    Concurrency::parallel_for_each(accl_view, compute_domain.tile<BLOCK_SIZE>(),[=] (Concurrency::tiled_index<BLOCK_SIZE> tidx) restrict(amp)
    {
      if(tidx.tile[0] != nBlocks -1)
      {
        for(int iter = 0; iter < step_sz; iter++)
        {
          Y[yOffset + tidx.tile[0] * 256 * step_sz + tidx.local[0] + iter*256] += X[xOffset + tidx.tile[0] * 256 * step_sz + tidx.local[0] + iter*256] * alpha;
        }
      }
      else
      {
        int n_iter = ((n - ((nBlocks - 1) * 256 * step_sz)) / 256) + 1;
        for(int iter = 0; iter < n_iter; iter++)
        {
          if (((nBlocks - 1) * 256 * step_sz + iter * 256 + tidx.local[0]) < n)
            Y[yOffset + tidx.tile[0] * 256 * step_sz + tidx.local[0] + iter*256] += X[xOffset + tidx.tile[0] * 256 * step_sz + tidx.local[0] + iter*256] * alpha;
        }
      }
    });
  }
}

// Single Precision General matrix rank 1 operation
void THGPUBlas_ger(THCState *state, long m, long n, float alpha,
             Concurrency::array_view<float> &x, long xOffset, long incx,
             Concurrency::array_view<float> &y, long yOffset, long incy,
             Concurrency::array_view<float> &a, long aOffset, long lda)
{
  // Make grid size in each dimension an exact multiple of threadblock size in the corresponding dimension
  long M = (m + 15) & ~15;
  long N = (n + 15) & ~15;
  Concurrency::extent<2> compute_domain(M, N);
  THCGPUDeviceState* device_state = state->deviceState;
  Concurrency::accelerator_view accl_view = device_state->get_current_accelerator_view();
  Concurrency::parallel_for_each(accl_view, compute_domain.tile<16, 16>(),[=] (Concurrency::tiled_index<16, 16> tidx) restrict(amp)
  {
    int i = tidx.global[0];
    int j = tidx.global[1];
    if(i < m && j < n)
      a[aOffset + j*m + i] += x[xOffset + i] * y[yOffset + j] * alpha;
  });
}

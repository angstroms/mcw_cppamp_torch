#ifndef TH_GPU_TENSOR_COPY_INC
#define TH_GPU_TENSOR_COPY_INC

#include "THCTensor.h"
#include "THCGeneral.h"

THC_API void THGPUTensor_copy(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_copyByte(THCState *state, THGPUTensor *self, THByteTensor *src);
THC_API void THGPUTensor_copyChar(THCState *state, THGPUTensor *self, THCharTensor *src);
THC_API void THGPUTensor_copyShort(THCState *state, THGPUTensor *self, THShortTensor *src);
THC_API void THGPUTensor_copyInt(THCState *state, THGPUTensor *self, THIntTensor *src);
THC_API void THGPUTensor_copyLong(THCState *state, THGPUTensor *self, THLongTensor *src);
THC_API void THGPUTensor_copyFloat(THCState *state, THGPUTensor *self, THFloatTensor *src);
THC_API void THGPUTensor_copyDouble(THCState *state, THGPUTensor *self, THDoubleTensor *src);

THC_API void THByteTensor_copyGPU(THCState *state, THByteTensor *self, THGPUTensor *src);
THC_API void THCharTensor_copyGPU(THCState *state, THCharTensor *self, THGPUTensor *src);
THC_API void THShortTensor_copyGPU(THCState *state, THShortTensor *self, THGPUTensor *src);
THC_API void THIntTensor_copyGPU(THCState *state, THIntTensor *self, THGPUTensor *src);
THC_API void THLongTensor_copyGPU(THCState *state, THLongTensor *self, THGPUTensor *src);
THC_API void THFloatTensor_copyGPU(THCState *state, THFloatTensor *self, THGPUTensor *src);
THC_API void THDoubleTensor_copyGPU(THCState *state, THDoubleTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_copyGPU(THCState *state, THGPUTensor *self, THGPUTensor *src);

#endif

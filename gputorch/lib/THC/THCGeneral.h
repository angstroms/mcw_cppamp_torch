#ifndef THC_GENERAL_INC
#define THC_GENERAL_INC

#include "THGeneral.h"
#undef log1p

#include "amp.h"



#ifdef __cplusplus
# define THC_EXTERNC extern "C"
#else
# define THC_EXTERNC extern
#endif

#ifdef WIN32
# ifdef THC_EXPORTS
#  define THC_API THC_EXTERNC __declspec(dllexport)
# else
#  define THC_API THC_EXTERNC __declspec(dllimport)
# endif
#else
# define THC_API THC_EXTERNC
#endif

struct THCRNGState;  /* Random number generator state. */
struct THCGPUDeviceState;
/* Global state to be held in gputorch table*/
typedef struct THCGPUDeviceState
{
  long deviceCount; // Total num of accelerator
  int currentDeviceID; // Current active device
  // Array of accelerators
  concurrency::accelerator *accArr;

  // Routine to set the current device ID
  inline void setCurrentDevID(int devID)
  {
    this->currentDeviceID = devID;
  }

  // Routine to return the current device ID
  inline int getCurrentDevID()
  {
    return this->currentDeviceID;
  }
  
  // get Current accelerator
  inline concurrency::accelerator get_current_accelerator()
  {
    concurrency::accelerator currentAcc = concurrency::accelerator::get_all()[this->currentDeviceID];
    return currentAcc;
  }

  // get current accelerator_view
  inline concurrency::accelerator_view get_current_accelerator_view()
  {
    concurrency::accelerator currentAcc = concurrency::accelerator::get_all()[this->currentDeviceID];
    return currentAcc.get_default_view();
  }

} THCGPUDeviceState;

/* Global state to be held in gputorch table*/
typedef struct THCState
{
  struct THCRNGState* rngState;
  struct THCGPUDeviceState* deviceState;
} THCState;


THC_API void THGPUInit(THCState* state);
THC_API void THGPUShutdown(THCState* state);

#define THGPUCheck(err)  __THGPUCheck(err, __FILE__, __LINE__)

THC_API void __THGPUCheck(int err, const char *file, const int line);

THC_API void THGPUGetGridSize(int *nBlockPerColumn_, int *nBlockPerRow_, int *nThreadPerBlock_, long size);

#endif

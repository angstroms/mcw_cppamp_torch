#ifndef THC_STORAGE_INC
#define THC_STORAGE_INC

#include "THStorage.h"
#include "THCGeneral.h"
#include "amp.h"

#define TH_STORAGE_REFCOUNTED 1
#define TH_STORAGE_RESIZABLE  2
#define TH_STORAGE_FREEMEM    4


typedef struct THGPUStorage
{
    float *data;
    long size;
    int refcount;
    char flag;
    THAllocator *allocator;
    Concurrency::array_view<float> *allocatorContext;
} THGPUStorage;


THC_API float* THGPUStorage_data(THCState *state, const THGPUStorage*);
THC_API long THGPUStorage_size(THCState *state, const THGPUStorage*);

/* slow access -- checks everything */
THC_API void THGPUStorage_set(THCState *state, THGPUStorage*, long, float);
THC_API float THGPUStorage_get(THCState *state, const THGPUStorage*, long);

THC_API THGPUStorage* THGPUStorage_new(THCState *state);
THC_API THGPUStorage* THGPUStorage_newWithSize(THCState *state, long size);
THC_API THGPUStorage* THGPUStorage_newWithSize1(THCState *state, float);
THC_API THGPUStorage* THGPUStorage_newWithSize2(THCState *state, float, float);
THC_API THGPUStorage* THGPUStorage_newWithSize3(THCState *state, float, float, float);
THC_API THGPUStorage* THGPUStorage_newWithSize4(THCState *state, float, float, float, float);
THC_API THGPUStorage* THGPUStorage_newWithMapping(THCState *state, const char *filename, long size, int shared);

/* takes ownership of data */
THC_API THGPUStorage* THGPUStorage_newWithData(THCState *state, float *data, long size);

THC_API THGPUStorage* THGPUStorage_newWithAllocator(THCState *state, long size,
                                                      THAllocator* allocator,
                                                      void *allocatorContext);
THC_API THGPUStorage* THGPUStorage_newWithDataAndAllocator(
   THCState *state, float* data, long size, THAllocator* allocator, void *allocatorContext);

THC_API void THGPUStorage_setFlag(THCState *state, THGPUStorage *storage, const char flag);
THC_API void THGPUStorage_clearFlag(THCState *state, THGPUStorage *storage, const char flag);
THC_API void THGPUStorage_retain(THCState *state, THGPUStorage *storage);

THC_API void THGPUStorage_free(THCState *state, THGPUStorage *storage);
THC_API void THGPUStorage_resize(THCState *state, THGPUStorage *storage, long size);
THC_API void THGPUStorage_fill(THCState *state, THGPUStorage *storage, float value);

#endif

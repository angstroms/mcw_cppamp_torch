#include "THCStorageCopy.h"
#include "THCGeneral.h"

void THGPUStorage_copyFloat(THCState *state, THGPUStorage *self, struct THFloatStorage *src)
{
  THArgCheck(self->size == src->size, 2, "size does not match");
  Concurrency::array_view<float, 1>* avDst = static_cast<Concurrency::array_view<float, 1>* >(self->allocatorContext);
  Concurrency::copy(src->data, src->data + src->size, *avDst);
}

#define TH_GPU_STORAGE_IMPLEMENT_COPY(TYPEC)                                      \
void THGPUStorage_copy##TYPEC(THCState *state, THGPUStorage *self, struct TH##TYPEC##Storage *src) \
{                                                                                 \
  THFloatStorage *buffer;                                                         \
  THArgCheck(self->size == src->size, 2, "size does not match");                  \
  buffer = THFloatStorage_newWithSize(src->size);                                 \
  THFloatStorage_copy##TYPEC(buffer, src);                                        \
  THGPUStorage_copyFloat(state, self, buffer);                                    \
  THFloatStorage_free(buffer);                                                    \
}

TH_GPU_STORAGE_IMPLEMENT_COPY(Byte)
TH_GPU_STORAGE_IMPLEMENT_COPY(Char)
TH_GPU_STORAGE_IMPLEMENT_COPY(Short)
TH_GPU_STORAGE_IMPLEMENT_COPY(Int)
TH_GPU_STORAGE_IMPLEMENT_COPY(Long)
TH_GPU_STORAGE_IMPLEMENT_COPY(Double)

void THFloatStorage_copyGPU(THCState *state, THFloatStorage *self, struct THGPUStorage *src)
{
  THArgCheck(self->size == src->size, 2, "size does not match");
  Concurrency::array_view<float, 1>* avSrc = static_cast<Concurrency::array_view<float, 1>* >(src->allocatorContext);
  Concurrency::copy(*avSrc, self->data);
}

#define TH_GPU_STORAGE_IMPLEMENT_COPYTO(TYPEC)                                      \
void TH##TYPEC##Storage_copyGPU(THCState *state, TH##TYPEC##Storage *self, struct THGPUStorage *src) \
{                                                                                   \
  THFloatStorage *buffer;                                                           \
  THArgCheck(self->size == src->size, 2, "size does not match");                    \
  buffer = THFloatStorage_newWithSize(src->size);                                   \
  THFloatStorage_copyGPU(state, buffer, src);                                       \
  TH##TYPEC##Storage_copyFloat(self, buffer);                                       \
  THFloatStorage_free(buffer);                                                      \
}

TH_GPU_STORAGE_IMPLEMENT_COPYTO(Byte)
TH_GPU_STORAGE_IMPLEMENT_COPYTO(Char)
TH_GPU_STORAGE_IMPLEMENT_COPYTO(Short)
TH_GPU_STORAGE_IMPLEMENT_COPYTO(Int)
TH_GPU_STORAGE_IMPLEMENT_COPYTO(Long)
TH_GPU_STORAGE_IMPLEMENT_COPYTO(Double)

// FIXME: device2device. 'src' is on device
#if 0
void THGPUStorage_rawCopy(THCState *state, THGPUStorage *self, float *src)
{
  float* device_ptr = static_cast<float*>(Concurrency::getDevicePointer(self->data));
  THGPUCheck(gpuMemcpyAsync(device_ptr, 0, src, 0, self->size * sizeof(float), gpuMemcpyDeviceToDevice));
}
#endif
void THGPUStorage_copy(THCState *state, THGPUStorage *self, THGPUStorage *src)
{
  THArgCheck(self->size == src->size, 2, "size does not match");
  Concurrency::array_view<float, 1>* avSrc = static_cast<Concurrency::array_view<float, 1>* >(src->allocatorContext);
  Concurrency::array_view<float, 1>* avDst = static_cast<Concurrency::array_view<float, 1>* >(self->allocatorContext);
  Concurrency::copy_async(*avSrc, *avDst);
}

void THGPUStorage_copyGPU(THCState *state, THGPUStorage *self, THGPUStorage *src)
{
  THGPUStorage_copy(state, self, src);
}

#include "THCTensorCopy.h"
#include "THCGeneral.h"
#include "THGeneral.h"
#include "THCTensor.h"
#include <iostream>
#include "amp_math.h"
#include "THCBolt.h"

// FIXME: suggest to call the same bolt::amp APIs in this file to avoid multiple definition error
// introduced by the Compiler. Will fix.
// See bolt::amp::copy in MemcpyHostToTHGPUTensor and THFloatTensor_copyGPU

using namespace std;

// Maximum number of dimensions allowed for gputorch
#define MAX_DIMS 25

/* specific methods */
void THGPUTensor_copyFloat(THCState *state, THGPUTensor *self, struct THFloatTensor *src)
{
  THArgCheck(THGPUTensor_nElement(state, self) == THFloatTensor_nElement(src), 2, "sizes do not match");
  {
    THGPUTensor *selfc = THGPUTensor_newContiguous(state, self);
    src = THFloatTensor_newContiguous(src);
    auto size = THGPUTensor_nElement(state, self);
    auto avDst = selfc->get_array_view();
    auto Dst = avDst.section(Concurrency::index<1>(selfc->storageOffset), Concurrency::extent<1>(size));
    auto begin = src->storage->data + src->storageOffset;
    Concurrency::copy(begin, begin + size, Dst);
    THFloatTensor_free(src);
    THGPUTensor_freeCopyTo(state, selfc, self);
  }
}

/* everything comes down to copy to a tensor of floats */
#define IMPLEMENT_TH_GPU_TENSOR_COPY(TYPEC)                                                           \
void THGPUTensor_copy##TYPEC(THCState *state, THGPUTensor *self, struct TH##TYPEC##Tensor *src) \
{                                                                                                     \
  THArgCheck(THGPUTensor_nElement(state, self) == TH##TYPEC##Tensor_nElement(src), 2, "sizes do not match"); \
                                                                                                      \
  {                                                                                                   \
    THLongStorage *size = TH##TYPEC##Tensor_newSizeOf(src);                                           \
    THFloatTensor *srcf = THFloatTensor_newWithSize(size, NULL);                                      \
                                                                                                      \
    THFloatTensor_copy##TYPEC(srcf, src);                                                             \
    THGPUTensor_copyFloat(state, self, srcf);                                                    \
                                                                                                      \
    THLongStorage_free(size);                                                                         \
    THFloatTensor_free(srcf);                                                                         \
  }                                                                                                   \
}

IMPLEMENT_TH_GPU_TENSOR_COPY(Byte)
IMPLEMENT_TH_GPU_TENSOR_COPY(Char)
IMPLEMENT_TH_GPU_TENSOR_COPY(Short)
IMPLEMENT_TH_GPU_TENSOR_COPY(Int)
IMPLEMENT_TH_GPU_TENSOR_COPY(Long)
IMPLEMENT_TH_GPU_TENSOR_COPY(Double)

/* copyGPU */
void THFloatTensor_copyGPU(THCState *state, THFloatTensor *self, struct THGPUTensor *src)
{
  THArgCheck(THFloatTensor_nElement(self) == THGPUTensor_nElement(state, src), 2, "sizes do not match");
  {
    THFloatTensor *selfc = THFloatTensor_newContiguous(self);
    src = THGPUTensor_newContiguous(state, src);
    auto size = THGPUTensor_nElement(state, src);
    auto avSrc = src->get_array_view();
    auto Src = avSrc.section(Concurrency::index<1>(src->storageOffset), Concurrency::extent<1>(size));
    Concurrency::copy(Src, selfc->storage->data + selfc->storageOffset);
    THGPUTensor_free(state, src);
    THFloatTensor_freeCopyTo(selfc, self);
  }
}

#define IMPLEMENT_TH_GPU_TENSOR_COPY_TO(TYPEC)                                                        \
void TH##TYPEC##Tensor_copyGPU(THCState *state, TH##TYPEC##Tensor *self, struct THGPUTensor *src) \
{                                                                                                     \
  THArgCheck(TH##TYPEC##Tensor_nElement(self) == THGPUTensor_nElement(state, src), 2, "sizes do not match"); \
                                                                                                      \
  {                                                                                                   \
    THLongStorage *size = THGPUTensor_newSizeOf(state, src);                                     \
    THFloatTensor *srcf = THFloatTensor_newWithSize(size, NULL);                                      \
                                                                                                      \
    THFloatTensor_copyGPU(state, srcf, src);                                                     \
    TH##TYPEC##Tensor_copyFloat(self, srcf);                                                          \
                                                                                                      \
    THLongStorage_free(size);                                                                         \
    THFloatTensor_free(srcf);                                                                         \
  }                                                                                                   \
}

IMPLEMENT_TH_GPU_TENSOR_COPY_TO(Byte)
IMPLEMENT_TH_GPU_TENSOR_COPY_TO(Char)
IMPLEMENT_TH_GPU_TENSOR_COPY_TO(Short)
IMPLEMENT_TH_GPU_TENSOR_COPY_TO(Int)
IMPLEMENT_TH_GPU_TENSOR_COPY_TO(Long)
IMPLEMENT_TH_GPU_TENSOR_COPY_TO(Double)

void THGPUTensor_copyGPU(THCState *state, THGPUTensor *self, THGPUTensor *src)
{
  THGPUTensor_copy(state, self, src);
}

#ifndef DIVUP
#define DIVUP(x, y) (((x) + (y) - 1) / (y))
#endif

// Copy self->size to device and remove all dims of size=1
static void THGPUTensor_computesz(THCState *state, THGPUTensor *self, Concurrency::array_view<long,1> **sz_,
                                  Concurrency::array_view<long> **st_, int *dim_, long *innermostdim)
{
  long *szh, *sth;
  int i, j, dim;
  long last_sz;

  dim = 0;
  // how many dims with size > 1 ?
  for (i = self->nDimension - 1; i >= 0; i--)
  {
    if (self->size[i] != 1)
      dim++;
  }

  if (dim == 0) THError("Error: using non-contiguous code-path for tensor with all singleton dimensions");
  Concurrency::extent<1> nDim(dim);

  Concurrency::array<long,1> arrSz = Concurrency::array<long, 1>(nDim, state->deviceState->get_current_accelerator_view());
  Concurrency::array<long,1> arrSt = Concurrency::array<long, 1>(nDim, state->deviceState->get_current_accelerator_view());
  *sz_ = new Concurrency::array_view<long, 1>(arrSz);
  *st_ = new Concurrency::array_view<long, 1>(arrSt);
  szh = (long*)THAlloc(sizeof(long)*dim);
  sth = (long*)THAlloc(sizeof(long)*dim);

  j = dim - 1;
  for (i = self->nDimension - 1; i >= 0; i--)
  {
    // ignore dimensions of size 1 to prevent copy bug
    if (self->size[i] != 1)
    {
      sth[j] = self->stride[i];

      if(j == dim - 1)
      {
        szh[j] = 1;
        *innermostdim = self->size[i];
      }
      else
        szh[j] = szh[j+1] * last_sz; //this makes no sense to me (should be size[i])

      j--;
      last_sz = self->size[i];
    }
  }


  Concurrency::copy(szh, szh + dim, **sz_);
  Concurrency::copy(sth, sth + dim, **st_);

  THFree(szh);
  THFree(sth);

  *dim_ = dim;
}

void THGPUTensor_kernel_copy(THCState *state, Concurrency::array_view<float>& av_dst, long dstOffset,
                             Concurrency::array_view<float>& av_src, long srcOffset,
                             Concurrency::array_view<long, 1> &av_dst_sz,
                             Concurrency::array_view<long, 1> &av_dst_st, int dst_dim,
                             Concurrency::array_view<long, 1> &av_src_sz,
                             Concurrency::array_view<long, 1> &av_src_st,
                             int src_dim, long n_elem, long innerdim, int nblockx,
                             int nblocky, int nblockz)
{
  Concurrency::extent<3> copyExt(nblockz, nblocky *16, nblockx * 16);
  Concurrency::tiled_extent<1, 16, 16> t_ext(copyExt);

  THCGPUDeviceState* device_state = state->deviceState;
  Concurrency::accelerator_view accl_view = device_state->get_current_accelerator_view();
  //Copy Kernel
  Concurrency::parallel_for_each(accl_view, t_ext, [=] (Concurrency::tiled_index<1, 16, 16> tidx) restrict(amp)
  {
    #if 0
    long x = t_ext.tile_dim2;
    long y = t_ext.tile_dim1;
    #endif
    long k = (tidx.tile[0] * (t_ext[2] / t_ext.tile_dim2) * (t_ext[1] / t_ext.tile_dim1) + tidx.tile[1] * (t_ext[2] / t_ext.tile_dim2) + tidx.tile[2] ) * t_ext.tile_dim1 + tidx.local[1];
    long i_start = tidx.local[2] * av_src_st[Concurrency::index<1>(src_dim - 1)];
    long i_step = t_ext.tile_dim2 * av_src_st[Concurrency::index<1>(src_dim - 1)];
    long o_start = tidx.local[2] * av_dst_st[Concurrency::index<1>(src_dim - 1)];
    long o_step = t_ext.tile_dim2 * av_dst_st[Concurrency::index<1>(src_dim - 1)];
    long o_end = innerdim * av_dst_st[Concurrency::index<1>(src_dim - 1)];

    if (((k + 1) * innerdim) <= n_elem) // too safe
    {
      long dst_idx = 0;
      long dst_rest = k * innerdim;
      for (int dim = 0; dim < dst_dim; dim++)
      {
        dst_idx += (dst_rest / av_dst_sz[Concurrency::index<1>(dim)]) * av_dst_st[Concurrency::index<1>(dim)];
        dst_rest = dst_rest % av_dst_sz[Concurrency::index<1>(dim)];
      }
      long src_idx = 0;
      long src_rest = k * innerdim;
      for (int dim = 0; dim < src_dim; dim++)
      {
        src_idx += (src_rest / av_src_sz[Concurrency::index<1>(dim)]) * av_src_st[Concurrency::index<1>(dim)];
        src_rest = src_rest % av_src_sz[Concurrency::index<1>(dim)];
      }
      for (int i = i_start, o = o_start; o < o_end; i += i_step, o += o_step)
      {
        av_dst[Concurrency::index<1>(dstOffset + dst_idx + o)] = av_src[Concurrency::index<1>(srcOffset + src_idx + i)];
      }
    }
  });
}

THC_API void THGPUTensor_copy(THCState *state, THGPUTensor *self, THGPUTensor *src)
{
  // Avoid unnecessary copy
  if (self == src)
    return;

  long totalElements = THGPUTensor_nElement(state, self);

  THArgCheck(totalElements == THGPUTensor_nElement(state, src), 2, "sizes do not match");
  THArgCheck(THGPUTensor_nDimension(state, self) <= MAX_DIMS, 2, "Copy only supported for <= 25 dimensions");
  THArgCheck(THGPUTensor_nDimension(state, src) <= MAX_DIMS, 3, "Copy only supported for <= 25 dimensions");

  if (THGPUTensor_nDimension(state, self) == 0)
  {
    // Zero-dim tensor; copy nothing
    return;
  }

  if ((THGPUTensor_isContiguous(state, self) && THGPUTensor_isContiguous(state, src)) || (totalElements == 1))
  {
    auto avSrc = src->get_array_view();
    auto Src = avSrc.section(Concurrency::index<1>(src->storageOffset), Concurrency::extent<1>(totalElements));
    auto avDst = self->get_array_view();
    auto Dst = avDst.section(Concurrency::index<1>(self->storageOffset), Concurrency::extent<1>(totalElements));
    Concurrency::copy_async(Src, Dst);
  }
  else
  {
    Concurrency::array_view<long, 1> *d_self_sz, *d_self_st, *d_src_sz, *d_src_st;
    int self_dim, src_dim;
    long size = THGPUTensor_nElement(state, self);
    long innermostdim;

    // Data is valid only in device side of d_src_sz, d_src_st, d_self_sz, d_self_st
    THGPUTensor_computesz(state, src, &d_src_sz, &d_src_st, &src_dim, &innermostdim);
    THGPUTensor_computesz(state, self, &d_self_sz, &d_self_st, &self_dim, &innermostdim);

    int nblocks = ceil((float)size / (16 * innermostdim ));

    // if nblocks greater than 65535 then we need to open a second dimension
    #define __MAX_NUM_BLOCKS_PER_GRID_DIM__ 65535

    // The configuration below can deal with Tensors
    // of size up to 65535 * 65535 * 65535 * 16 elements.
    int nblocks_x = (nblocks > __MAX_NUM_BLOCKS_PER_GRID_DIM__) ? __MAX_NUM_BLOCKS_PER_GRID_DIM__ : nblocks;
    int number_blocks_dim_x = DIVUP(nblocks, nblocks_x);
    int nblocks_y = (number_blocks_dim_x > __MAX_NUM_BLOCKS_PER_GRID_DIM__) ? __MAX_NUM_BLOCKS_PER_GRID_DIM__ : number_blocks_dim_x;
    int number_blocks_dim_y = DIVUP(nblocks, nblocks_x * nblocks_y);
    int nblocks_z = number_blocks_dim_y;

    auto avSelf = self->get_array_view();
    auto avSrc = src->get_array_view();

    d_self_sz->discard_data();
    d_self_st->discard_data();
    d_src_sz->discard_data();
    d_src_st->discard_data();

    THGPUTensor_kernel_copy(state, avSelf, self->storageOffset, avSrc, src->storageOffset,
                           *d_self_sz, *d_self_st, self_dim,
                           *d_src_sz, *d_src_st, src_dim,
                           size, innermostdim, nblocks_x, nblocks_y, nblocks_z);

    delete d_self_st;
    delete d_self_sz;
    delete d_src_st;
    delete d_src_sz;
  }
}

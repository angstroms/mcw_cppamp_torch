#ifndef TH_GPU_TENSOR_MATH_INC
#define TH_GPU_TENSOR_MATH_INC

#include "THCTensor.h"
#include "THCTensorRandom.h"

THC_API void THGPUTensor_fill(THCState *state, THGPUTensor *self, float value);
THC_API void THGPUTensor_zero(THCState *state, THGPUTensor *self);

THC_API void THGPUTensor_zeros(THCState *state, THGPUTensor *r_, THLongStorage *size);
THC_API void THGPUTensor_ones(THCState *state, THGPUTensor *r_, THLongStorage *size);
THC_API void THGPUTensor_reshape(THCState *state, THGPUTensor *r_, THGPUTensor *t, THLongStorage *size);
THC_API long THGPUTensor_numel(THCState *state, THGPUTensor *t);

THC_API void THGPUTensor_add(THCState *state, THGPUTensor *self, THGPUTensor *src, float value);
THC_API void THGPUTensor_mul(THCState *state, THGPUTensor *self, THGPUTensor *src, float value);
THC_API void THGPUTensor_div(THCState *state, THGPUTensor *self, THGPUTensor *src, float value);


THC_API void THGPUTensor_cadd(THCState *state, THGPUTensor *self, THGPUTensor *src1, float value, THGPUTensor *src2);
THC_API void THGPUTensor_cadd_tst(THCState *state, THGPUTensor *self, THGPUTensor *src1, float value, THGPUTensor *src2);
THC_API void THGPUTensor_cmul(THCState *state, THGPUTensor *self, THGPUTensor *src1, THGPUTensor *src2);
THC_API void THGPUTensor_cdiv(THCState *state, THGPUTensor *self, THGPUTensor *src1, THGPUTensor *src2);

THC_API void THGPUTensor_addcmul(THCState *state, THGPUTensor *self, THGPUTensor *t, float value, THGPUTensor *src1, THGPUTensor *src2);
THC_API void THGPUTensor_addcdiv(THCState *state, THGPUTensor *self, THGPUTensor *t, float value, THGPUTensor *src1, THGPUTensor *src2);

THC_API float THGPUTensor_dot(THCState *state, THGPUTensor *self, THGPUTensor *src);

THC_API float THGPUTensor_minall(THCState *state, THGPUTensor *self);
THC_API float THGPUTensor_maxall(THCState *state, THGPUTensor *self);
THC_API float THGPUTensor_sumall(THCState *state, THGPUTensor *self);
THC_API float THGPUTensor_prodall(THCState *state, THGPUTensor *self);
THC_API void THGPUTensor_min(THCState *state, THGPUTensor *values, THGPUTensor *indices, THGPUTensor *src, long dim);
THC_API void THGPUTensor_max(THCState *state, THGPUTensor *values, THGPUTensor *indices, THGPUTensor *src, long dim);
THC_API void THGPUTensor_sum(THCState *state, THGPUTensor *self, THGPUTensor *src, long dim);
THC_API void THGPUTensor_prod(THCState *state, THGPUTensor *self, THGPUTensor *src, long dim);

THC_API void THGPUTensor_addmv(THCState *state, THGPUTensor *self, float beta, THGPUTensor *t, float alpha, THGPUTensor *mat, THGPUTensor *vec);
THC_API void THGPUTensor_addmm(THCState *state, THGPUTensor *self, float beta, THGPUTensor *t, float alpha, THGPUTensor *mat1, THGPUTensor *mat2);
THC_API void THGPUTensor_addr(THCState *state, THGPUTensor *self, float beta, THGPUTensor *t, float alpha, THGPUTensor *vec1, THGPUTensor *vec2);

THC_API void THGPUTensor_log(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_log1p(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_exp(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_cos(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_acos(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_cosh(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_sin(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_asin(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_sinh(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_tan(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_atan(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_tanh(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_pow(THCState *state, THGPUTensor *self, THGPUTensor *src, float value);
THC_API void THGPUTensor_clamp(THCState *state, THGPUTensor *self, THGPUTensor *src, float min_value, float max_value);
THC_API void THGPUTensor_sqrt(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_ceil(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_floor(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_abs(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_sign(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_round(THCState *state, THGPUTensor *self, THGPUTensor *src);
THC_API void THGPUTensor_atan2(THCState *state, THGPUTensor *r_, THGPUTensor *tx, THGPUTensor *ty);

THC_API void THGPUTensor_ltValue(THCState *state, THGPUTensor *self_, THGPUTensor *src, float value);
THC_API void THGPUTensor_gtValue(THCState *state, THGPUTensor *self_, THGPUTensor *src, float value);
THC_API void THGPUTensor_leValue(THCState *state, THGPUTensor *self_, THGPUTensor *src, float value);
THC_API void THGPUTensor_geValue(THCState *state, THGPUTensor *self_, THGPUTensor *src, float value);
THC_API void THGPUTensor_eqValue(THCState *state, THGPUTensor *self_, THGPUTensor *src, float value);
THC_API void THGPUTensor_neValue(THCState *state, THGPUTensor *self_, THGPUTensor *src, float value);

THC_API void THGPUTensor_ltTensor(THCState *state, THGPUTensor *self_, THGPUTensor *src1, THGPUTensor *src2);
THC_API void THGPUTensor_gtTensor(THCState *state, THGPUTensor *self_, THGPUTensor *src1, THGPUTensor *src2);
THC_API void THGPUTensor_leTensor(THCState *state, THGPUTensor *self_, THGPUTensor *src1, THGPUTensor *src2);
THC_API void THGPUTensor_geTensor(THCState *state, THGPUTensor *self_, THGPUTensor *src1, THGPUTensor *src2);
THC_API void THGPUTensor_eqTensor(THCState *state, THGPUTensor *self_, THGPUTensor *src1, THGPUTensor *src2);
THC_API void THGPUTensor_neTensor(THCState *state, THGPUTensor *self_, THGPUTensor *src1, THGPUTensor *src2);

THC_API float THGPUTensor_meanall(THCState *state, THGPUTensor *self);
THC_API void  THGPUTensor_mean(THCState *state, THGPUTensor *self, THGPUTensor *src, long dim);
THC_API float THGPUTensor_varall(THCState *state, THGPUTensor *self);
THC_API float THGPUTensor_stdall(THCState *state, THGPUTensor *self);
THC_API float THGPUTensor_normall(THCState *state, THGPUTensor *self, float value);
THC_API void  THGPUTensor_norm(THCState *state, THGPUTensor* self, THGPUTensor* src, float value, long dimension);
THC_API void  THGPUTensor_renorm(THCState *state, THGPUTensor* self, THGPUTensor* src, float value, long dimension, float max_norm);
THC_API float THGPUTensor_dist(THCState *state, THGPUTensor *self, THGPUTensor *src, float value);

THC_API void THGPUTensor_rand(THCState *state, THCRNGState* rng_state,THGPUTensor *r_, THLongStorage *size);
THC_API void THGPUTensor_randn(THCState *state, THCRNGState* rng_state,THGPUTensor *r_, THLongStorage *size);

THC_API void THGPUTensor_indexCopy(THCState *state, THGPUTensor *res_, int dim, THLongTensor *indices, THGPUTensor *src);
THC_API void THGPUTensor_indexFill(THCState *state, THGPUTensor *tensor, int dim, THLongTensor *index, float val);
THC_API void THGPUTensor_indexSelect(THCState *state, THGPUTensor *tensor, THGPUTensor *src, int dim, THLongTensor *index);


#endif

#include "THCStorage.h"
#include "THCGeneral.h"
#include "THCBolt.h"

void THGPUStorage_set(THCState *state, THGPUStorage *self, long index, float value)
{
  THArgCheck((index >= 0) && (index < self->size), 2, "index out of bounds");
  Concurrency::array_view<float, 1>* avDst = static_cast<Concurrency::array_view<float, 1>* >(self->allocatorContext);
  auto Dst = avDst->section(Concurrency::index<1>(index), Concurrency::extent<1>(1));
  Concurrency::copy(&value, (&value) + 1, Dst);
}

float THGPUStorage_get(THCState *state, const THGPUStorage *self, long index)
{
  float value;
  THArgCheck((index >= 0) && (index < self->size), 2, "index out of bounds");
  Concurrency::array_view<float, 1>* avSrc = static_cast<Concurrency::array_view<float, 1>* >(self->allocatorContext);
  auto Src = avSrc->section(Concurrency::index<1>(index), Concurrency::extent<1>(1));
  Concurrency::copy(Src, &value);
  return value;
}

THGPUStorage* THGPUStorage_new(THCState *state)
{
#ifndef __KALMAR_CC__
  int default_size = 0;
#else
  int default_size = 1;
#endif
  THGPUStorage *storage = (THGPUStorage *)THAlloc(sizeof(THGPUStorage));
  // Construct array_view on selected device
  Concurrency::accelerator_view accl_view = state->deviceState->get_current_accelerator_view();
  Concurrency::array<float, 1> arr = Concurrency::array<float, 1>(Concurrency::extent<1>(default_size), accl_view);
  storage->allocatorContext = new Concurrency::array_view<float, 1>(arr);
  // Need to keep data member
  Concurrency::array_view<float>* avData = storage->allocatorContext;
  storage->data = avData->data();
  //
  storage->size = default_size;
  storage->refcount = 1;
  storage->flag = TH_STORAGE_REFCOUNTED | TH_STORAGE_RESIZABLE | TH_STORAGE_FREEMEM;
  return storage;
}

THGPUStorage* THGPUStorage_newWithSize(THCState *state, long size)
{
  THArgCheck(size >= 0, 2, "invalid size");

  if (size > 0)
  {
    THGPUStorage *storage = (THGPUStorage *)THAlloc(sizeof(THGPUStorage));
    Concurrency::extent<1> eA(size);
    // Allocating device array of given size
    Concurrency::accelerator_view accl_view = state->deviceState->get_current_accelerator_view();
    Concurrency::array<float, 1> arr = Concurrency::array<float, 1>(eA, accl_view);
    Concurrency::array_view<float>* avData = new Concurrency::array_view<float>(arr);
    storage->allocatorContext = avData;
    // Need to keep data member
    storage->data = avData->data();
    //
    storage->size = size;
    storage->refcount = 1;
    storage->flag = TH_STORAGE_REFCOUNTED | TH_STORAGE_RESIZABLE | TH_STORAGE_FREEMEM;
    return storage;
  }
  else
  {
    return THGPUStorage_new(state);
  }
}

THGPUStorage* THGPUStorage_newWithSize1(THCState *state, float data0)
{
  THGPUStorage *self = THGPUStorage_newWithSize(state, 1);
  THGPUStorage_set(state, self, 0, data0);
  return self;
}

THGPUStorage* THGPUStorage_newWithSize2(THCState *state, float data0, float data1)
{
  THGPUStorage *self = THGPUStorage_newWithSize(state, 2);
  THGPUStorage_set(state, self, 0, data0);
  THGPUStorage_set(state, self, 1, data1);
  return self;
}

THGPUStorage* THGPUStorage_newWithSize3(THCState *state, float data0, float data1, float data2)
{
  THGPUStorage *self = THGPUStorage_newWithSize(state, 3);
  THGPUStorage_set(state, self, 0, data0);
  THGPUStorage_set(state, self, 1, data1);
  THGPUStorage_set(state, self, 2, data2);
  return self;
}

THGPUStorage* THGPUStorage_newWithSize4(THCState *state, float data0, float data1, float data2, float data3)
{
  THGPUStorage *self = THGPUStorage_newWithSize(state, 4);
  THGPUStorage_set(state, self, 0, data0);
  THGPUStorage_set(state, self, 1, data1);
  THGPUStorage_set(state, self, 2, data2);
  THGPUStorage_set(state, self, 3, data3);
  return self;
}

THGPUStorage* THGPUStorage_newWithMapping(THCState *state, const char *fileName, long size, int isShared)
{
  THError("not available yet for THGPUStorage");
  return NULL;
}

// Note that 'data' is on host
THGPUStorage* THGPUStorage_newWithData(THCState *state, float *data, long size)
{
  THGPUStorage *storage = (THGPUStorage *)THAlloc(sizeof(THGPUStorage));
  Concurrency::accelerator_view accl_view = state->deviceState->get_current_accelerator_view();
  Concurrency::array<float, 1> arr = Concurrency::array<float, 1>(Concurrency::extent<1>(size), data, accl_view);
  Concurrency::array_view<float>* avData  = new Concurrency::array_view<float>(arr);
  storage->allocatorContext = avData;
  storage->data = data;
  storage->size = size;
  storage->refcount = 1;
  storage->flag = TH_STORAGE_REFCOUNTED | TH_STORAGE_RESIZABLE | TH_STORAGE_FREEMEM;
  return storage;
}

void THGPUStorage_retain(THCState *state, THGPUStorage *self)
{
  if (self && (self->flag & TH_STORAGE_REFCOUNTED))
    ++self->refcount;
}

void THGPUStorage_free(THCState *state, THGPUStorage *self)
{
  if (!(self->flag & TH_STORAGE_REFCOUNTED))
    return;

  if (--(self->refcount) <= 0)
  {
    if (self->flag & TH_STORAGE_FREEMEM)
    {
      if (self->allocatorContext)
      {
        delete (Concurrency::array_view<float> *)self->allocatorContext;
        self->allocatorContext = NULL;
      }

      self->data = NULL;
      self->size = 0;
      self->refcount =0;
      self->flag = 0;
    }
    THFree(self);
  }
}

void THGPUStorage_fill(THCState *state, THGPUStorage *self, float value)
{
  // Make sure every changes need to be made to its array_view
  Concurrency::array_view<float,1> *pavSelf = self->allocatorContext;
  pavSelf->discard_data();
  fill(state, *pavSelf, self->size, value);
}

void THGPUStorage_resize(THCState *state, THGPUStorage *self, long size)
{
  THArgCheck(size >= 0, 2, "invalid size");
  if (!(self->flag & TH_STORAGE_RESIZABLE))
    return;

  if (size == 0)
  {
    if (self->flag & TH_STORAGE_FREEMEM)
    {
      if (self->allocatorContext)
      {
        self->allocatorContext->~array_view();
      }
      else
      {
        Concurrency::array_view<float, 1> delSelf (Concurrency::extent<1>(self->size), self->data);
        delSelf.~array_view();
      }
    }
    self->data = NULL;
    self->size = 0;
  }
  else if (self->size != size)
  {
    // Resizing the extent
    Concurrency::extent<1> eA(size);
    // Allocating device array of resized value
    Concurrency::accelerator_view accl_view = state->deviceState->get_current_accelerator_view();
    Concurrency::array<float, 1> arr = Concurrency::array<float, 1>(eA, accl_view);
    Concurrency::array_view<float, 1> *avDest = new Concurrency::array_view<float>(arr);
    Concurrency::array_view<float, 1>* avSrc = static_cast<Concurrency::array_view<float, 1>* >(self->allocatorContext);
    if (avSrc->get_extent().size() > 1)
        Concurrency::copy_async(*avSrc, *avDest);

    avSrc->~array_view();
    self->allocatorContext = avDest;
    // Need to keep data member
    self->data = avDest->data();
    //
    self->size = size;
    // Set default refcount for the new resized
    self->refcount = 1;
  }
}

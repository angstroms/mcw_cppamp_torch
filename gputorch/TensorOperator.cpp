#include "torch/utils.h"
#include "luaT.h"
#include "THC.h"

static int gputorch_GPUTensorOperator___add__(lua_State *L)
{
  THGPUTensor *tensor1 = (THGPUTensor*)luaT_toudata(L, 1, "torch.GPUTensor");
  THGPUTensor *tensor2 = (THGPUTensor*)luaT_toudata(L, 2, "torch.GPUTensor");
  THGPUTensor *r;
  THCState *state = gputorch_getstate(L);
  //THAssert(THGPUTensor_checkGPU(state, 2, tensor1, tensor2));

  if(!tensor1 && !tensor2)
    luaL_error(L, "expecting two Tensors or one Tensor and one number");
  else
  {
    r = THGPUTensor_new(state);
    luaT_pushudata(L, r, "torch.GPUTensor");

    if(!tensor1 && tensor2)
    {
      THGPUTensor_resizeAs(state, r, tensor2);
      THGPUTensor_copy(state, r, tensor2);
      THGPUTensor_add(state, r, r, luaL_checknumber(L, 1));
    }
    else if(tensor1 && !tensor2)
    {
      THGPUTensor_resizeAs(state, r, tensor1);
      THGPUTensor_copy(state, r, tensor1);
      THGPUTensor_add(state, r, r, luaL_checknumber(L, 2));
    }
    else
    {
      THGPUTensor_resizeAs(state, r, tensor1);
      THGPUTensor_copy(state, r, tensor1);
      THGPUTensor_cadd(state, r, r, 1, tensor2);
    }
  }
  return 1;
}

static int gputorch_GPUTensorOperator___sub__(lua_State *L)
{
  THGPUTensor *tensor1 = (THGPUTensor*)luaT_toudata(L, 1, "torch.GPUTensor");
  THGPUTensor *tensor2 = (THGPUTensor*)luaT_toudata(L, 2, "torch.GPUTensor");
  THGPUTensor *r;
  THCState *state = gputorch_getstate(L);
  //THAssert(THGPUTensor_checkGPU(state, 2, tensor1, tensor2));

  if(!tensor1 && !tensor2)
    luaL_error(L, "expecting two Tensors or one Tensor and one number");
  else
  {
    r = THGPUTensor_new(state);
    luaT_pushudata(L, r, "torch.GPUTensor");

    if(!tensor1 && tensor2)
    {
      THGPUTensor_resizeAs(state, r, tensor2);
      THGPUTensor_fill(state, r, luaL_checknumber(L, 1));
      THGPUTensor_cadd(state, r, r, -1, tensor2);
    }
    else if(tensor1 && !tensor2)
    {
      THGPUTensor_resizeAs(state, r, tensor1);
      THGPUTensor_copy(state, r, tensor1);
      THGPUTensor_add(state, r, r, -luaL_checknumber(L, 2));
    }
    else
    {
      THGPUTensor_resizeAs(state, r, tensor1);
      THGPUTensor_copy(state, r, tensor1);
      THGPUTensor_cadd(state, r, r, -1, tensor2);
    }
  }
  return 1;
}

static int gputorch_GPUTensorOperator___unm__(lua_State *L)
{
  THGPUTensor *tensor = (THGPUTensor*)luaT_checkudata(L, 1, "torch.GPUTensor");
  THGPUTensor *r;
  THCState *state = gputorch_getstate(L);
  //THAssert(THGPUTensor_checkGPU(state, 1, tensor));

  r = THGPUTensor_new(state);
  luaT_pushudata(L, r, "torch.GPUTensor");
  THGPUTensor_resizeAs(state, r, tensor);
  THGPUTensor_copy(state, r, tensor);
  THGPUTensor_mul(state, r, r, -1);

  return 1;
}

static int gputorch_GPUTensorOperator___mul__(lua_State *L)
{
  THGPUTensor *tensor1 = (THGPUTensor*)luaT_toudata(L, 1, "torch.GPUTensor");
  THGPUTensor *tensor2 = (THGPUTensor*)luaT_toudata(L, 2, "torch.GPUTensor");
  THGPUTensor *r;
  THCState *state = gputorch_getstate(L);
  //THAssert(THGPUTensor_checkGPU(state, 2, tensor1, tensor2));

  if(!tensor1 && !tensor2)
    luaL_error(L, "expecting two Tensors or one Tensor and one number");
  else
  {
    r = THGPUTensor_new(state);
    luaT_pushudata(L, r, "torch.GPUTensor");

    if(!tensor1 && tensor2)
    {
      THGPUTensor_resizeAs(state, r, tensor2);
      THGPUTensor_copy(state, r, tensor2);
      THGPUTensor_mul(state, r, r, luaL_checknumber(L, 1));
    }
    else if(tensor1 && !tensor2)
    {
      THGPUTensor_resizeAs(state, r, tensor1);
      THGPUTensor_copy(state, r, tensor1);
      THGPUTensor_mul(state, r, r, luaL_checknumber(L, 2));
    }
    else
    {
      int dimt = tensor1->nDimension;
      int dims = tensor2->nDimension;

      if(dimt == 1 && dims == 1)
        lua_pushnumber(L, THGPUTensor_dot(state, tensor1, tensor2)); /* ok, we wasted r, but who cares */
      else if(dimt == 2 && dims == 1)
      {
        THGPUTensor_resize1d(state, r, tensor1->size[0]);
        THGPUTensor_zero(state, r);
        THGPUTensor_addmv(state, r, 1, r, 1, tensor1, tensor2);
      }
      else if(dimt == 2 && dims == 2)
      {
        THGPUTensor_resize2d(state, r, tensor1->size[0], tensor2->size[1]);
        THGPUTensor_zero(state, r);
        THGPUTensor_addmm(state, r, 1, r, 1, tensor1, tensor2);
      }
      else
        luaL_error(L, "multiplication between %dD and %dD tensors not yet supported", tensor1->nDimension, tensor2->nDimension);
    }
  }
  return 1;
}

static int gputorch_GPUTensorOperator___div__(lua_State *L)
{
  THGPUTensor *tensor = (THGPUTensor*)luaT_checkudata(L, 1, "torch.GPUTensor");
  THGPUTensor *r;
  THCState *state = gputorch_getstate(L);
  //THAssert(THGPUTensor_checkGPU(state, 1, tensor));

  luaL_argcheck(L, lua_isnumber(L,2), 2, "number expected");

  r = THGPUTensor_new(state);
  luaT_pushudata(L, r, "torch.GPUTensor");

  THGPUTensor_resizeAs(state, r, tensor);
  THGPUTensor_copy(state, r, tensor);
  THGPUTensor_mul(state, r, r, 1/lua_tonumber(L, 2));

  return 1;
}

static const struct luaL_Reg gputorch_GPUTensorOperator__ [] = {
  {"__add__", gputorch_GPUTensorOperator___add__},
  {"__sub__", gputorch_GPUTensorOperator___sub__},
  {"__unm__", gputorch_GPUTensorOperator___unm__},
  {"__mul__", gputorch_GPUTensorOperator___mul__},
  {"__div__", gputorch_GPUTensorOperator___div__},
  {NULL, NULL}
};

void gputorch_GPUTensorOperator_init(lua_State *L)
{
  luaT_pushmetatable(L, "torch.GPUTensor");
  luaL_register(L, NULL, gputorch_GPUTensorOperator__);
  lua_pop(L, 1);
}

#include "utils.h"
#include "luaT.h"
#include "THCGeneral.h"
#include "THCTensorRandom.h"

extern void gputorch_GPUStorage_init(lua_State* L);
extern void gputorch_GPUTensor_init(lua_State* L);
extern void gputorch_GPUTensorMath_init(lua_State* L);
extern void gputorch_GPUTensorOperator_init(lua_State* L);

static int gputorch_synchronize(lua_State *L)
{
  /* TO BE IMPLEMENTED */
  return 0;
}

static int gputorch_getDevice(lua_State *L)
{
  int device;
  THCState *state = gputorch_getstate(L);
  THCGPUDeviceState* device_state = state->deviceState;
  device = device_state->getCurrentDevID();
  lua_pushnumber(L, device);
  return 1;
}

static int gputorch_deviceReset(lua_State *L)
{
  /* TO BE IMPLEMENTED */
  return 0;
}

static int gputorch_getDeviceCount(lua_State *L)
{
  int ndevice;
  THCState *state = gputorch_getstate(L);
  THCGPUDeviceState* device_state = state->deviceState;
  ndevice = device_state->deviceCount;
  lua_pushnumber(L, ndevice);
  return 1;
}

#define CL_CHECK(_expr)                                                         \
   do {                                                                         \
     cl_int _err = _expr;                                                       \
     if (_err == CL_SUCCESS)                                                    \
       break;                                                                   \
     fprintf(stderr, "OpenCL Error: '%s' returned %d!\n", #_expr, (int)_err);   \
     abort();                                                                   \
   } while (0)

// Gets the total and free memory in bytes for the given device ID.
static int gputorch_getMemoryUsage(lua_State *L) {
#ifndef __KALMAR_CC__
  THCState *state = gputorch_getstate(L);
  THCGPUDeviceState* device_state = state->deviceState;
  int deviceID = device_state->getCurrentDevID();
  if (deviceID < 1)
  {
    std::cout << "Err: Invalid device id (device id > 0)" << std::endl;
    return 0;
  }
  if (deviceID > device_state->deviceCount)
  {
    std::cout << "Err: Invalid device id (number of available devices "
              << device_state->deviceCount  << ")" << std::endl;
    return 0;
  }

  // Getting the accelerator corresponding to the device set by user
  cl_device_id devID = Concurrency::accelerator::get_all()[deviceID].get_device_id();

  size_t freeBytes = 0;
  size_t totalBytes = 0;
  CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(totalBytes), &totalBytes, NULL));
  // TODO: free memory of specified device is not available
  lua_pushnumber(L, freeBytes);
  lua_pushnumber(L, totalBytes);
#endif
  return 2;
}

static int gputorch_setDevice(lua_State *L)
{
  THCState *state = gputorch_getstate(L);
  int deviceID = (int)luaL_checknumber(L, 1);
  THCGPUDeviceState* device_state = state->deviceState;
  if (deviceID < 1)
  {
    std::cout << "Err: Invalid device id (device id > 0)" << std::endl;
    return 0;
  }
  if (deviceID > device_state->deviceCount)
  {
    std::cout << "Err: Invalid device id (number of available devices "
              << device_state->deviceCount  << ")" << std::endl;
    return 0;
  }
  device_state->setCurrentDevID(deviceID);
  return 1;
}

#define SET_DEVN_STR_PROP(NAME, VAL) \
lua_pushstring(L, NAME); \
lua_pushstring(L, VAL); \
lua_settable(L, -3);

#define SET_DEVN_NUM_PROP(NAME, VAL) \
lua_pushstring(L, NAME); \
lua_pushnumber(L, VAL); \
lua_settable(L, -3);

static int gputorch_getDeviceProperties(lua_State *L)
{
#ifndef __KALMAR_CC__
  THCState *state = gputorch_getstate(L);
  THCGPUDeviceState* device_state = state->deviceState;
  int deviceID = device_state->getCurrentDevID();
  if (deviceID < 1)
  {
    std::cout << "Err: Invalid device id (device id > 0)" << std::endl;
    return 0;
  }
  if (deviceID > device_state->deviceCount)
  {
    std::cout << "Err: Invalid device id (number of available devices "
              << device_state->deviceCount  << ")" << std::endl;
    return 0;
  }

  // Getting the accelerator corresponding to the device set by user
  cl_device_id devID = Concurrency::accelerator::get_all()[deviceID].get_device_id();
  char strBuf[1024] = {0x0};
  cl_uint buf_uint;

  lua_newtable(L);
  SET_DEVN_NUM_PROP("Current Device", deviceID);
  CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_NAME, sizeof(strBuf), strBuf, NULL));
  SET_DEVN_STR_PROP("Device Name", strBuf);
  CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_VENDOR, sizeof(strBuf), strBuf, NULL));
  SET_DEVN_STR_PROP("Vendor Name", strBuf);
  CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_VERSION, sizeof(strBuf), strBuf, NULL));
  SET_DEVN_STR_PROP("Device Version", strBuf);
  CL_CHECK(clGetDeviceInfo(devID, CL_DRIVER_VERSION, sizeof(strBuf), strBuf, NULL));
  SET_DEVN_STR_PROP("Driver Version", strBuf);
  CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(buf_uint), &buf_uint, NULL));
  SET_DEVN_NUM_PROP("Max Compute Units", buf_uint);
  {
    cl_uint dimensions = 0;
    CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(cl_uint), &dimensions, NULL));
    size_t* maxSizes = new size_t[dimensions];
    CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(size_t) * dimensions, maxSizes, NULL));
    char str[256] = {0x0};
    for (int i = 0; i < dimensions; i++) {
      int offset = strlen(str);
      if (i != dimensions -1)
        sprintf(&str[offset], "%ld,", maxSizes[i]);
      else
        sprintf(&str[offset], "%ld", maxSizes[i]);
    }
    SET_DEVN_STR_PROP("Max work items", str);
    delete maxSizes;
    size_t szValue;
    CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(szValue), &szValue, NULL));
    SET_DEVN_NUM_PROP("Max work group size", szValue);
  }
  {
    cl_ulong lvalue;
    cl_uint ivalue;
    CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(ivalue), &ivalue, NULL));
    SET_DEVN_NUM_PROP("Max clock frequency(MHz)", ivalue);
    CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(lvalue), &lvalue, NULL));
    SET_DEVN_NUM_PROP("Max memory allocation", lvalue);
    CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(lvalue), &lvalue, NULL));
    SET_DEVN_NUM_PROP("Global memory size", lvalue);
    CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, sizeof(lvalue), &lvalue, NULL));
    SET_DEVN_NUM_PROP("Constant buffer size", lvalue);
    CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(lvalue), &lvalue, NULL));
    SET_DEVN_NUM_PROP("Local memory size", lvalue);
  }
#endif
  return 1;
}

static int gputorch_seed(lua_State *L)
{
  unsigned long seed = THCRandom_seed(gputorch_getstate(L));
  lua_pushnumber(L, seed);
  return 1;
}

static int gputorch_seedAll(lua_State *L)
{
  unsigned long seed = THCRandom_seedAll(gputorch_getstate(L));
  lua_pushnumber(L, seed);
  return 1;
}

static int gputorch_initialSeed(lua_State *L)
{
  unsigned long seed = THCRandom_initialSeed(gputorch_getstate(L));
  lua_pushnumber(L, seed);
  return 1;
}

static int gputorch_manualSeed(lua_State *L)
{
  unsigned long seed = luaL_checknumber(L, 1);
  THCRandom_manualSeed(gputorch_getstate(L), seed);
  return 0;
}

static int gputorch_manualSeedAll(lua_State* L)
{
  unsigned long seed = luaL_checknumber(L, 1);
  THCRandom_manualSeedAll(gputorch_getstate(L), seed);
  return 0;
}

static int gputorch_getRNGState(lua_State *L)
{
  THByteTensor* t = THByteTensor_new();
  THCRandom_getRNGState(gputorch_getstate(L), t);
  luaT_pushudata(L, t, "torch.ByteTensor");
  return 1;
}

static int gputorch_setRNGState(lua_State *L)
{
  THByteTensor* t = (THByteTensor*)luaT_checkudata(L, 1, "torch.ByteTensor");
  THCRandom_setRNGState(gputorch_getstate(L), t);
  return 0;
}

static int gputorch_getState(lua_State *L)
{
  lua_getglobal(L, "gputorch");
  lua_getfield(L, -1, "_state");
  lua_remove(L, -2);
  return 1;
}

static const struct luaL_Reg gputorch_stuff__ [] = {
  {"synchronize", gputorch_synchronize},
  {"getDevice", gputorch_getDevice},
  {"deviceReset", gputorch_deviceReset},
  {"getDeviceCount", gputorch_getDeviceCount},
  {"getDeviceProperties", gputorch_getDeviceProperties},
  {"getMemoryUsage", gputorch_getMemoryUsage},
  {"setDevice", gputorch_setDevice},
  {"seed", gputorch_seed},
  {"seedAll", gputorch_seedAll},
  {"initialSeed", gputorch_initialSeed},
  {"manualSeed", gputorch_manualSeed},
  {"manualSeedAll", gputorch_manualSeedAll},
  {"getRNGState", gputorch_getRNGState},
  {"setRNGState", gputorch_setRNGState},
  {"getState", gputorch_getState},
  {NULL, NULL}
};

LUA_EXTERNC DLL_EXPORT int luaopen_libgputorch(lua_State *L);

int luaopen_libgputorch(lua_State *L)
{
  lua_newtable(L);
  luaL_register(L, NULL, gputorch_stuff__);

  THCState* state = (THCState*)malloc(sizeof(THCState));
  THGPUInit(state);

  gputorch_GPUStorage_init(L);
  gputorch_GPUTensor_init(L);
  gputorch_GPUTensorMath_init(L);
  gputorch_GPUTensorOperator_init(L);
  /* Store state in gputorch table. */
  lua_pushlightuserdata(L, state);
  lua_setfield(L, -2, "_state");

  return 1;
}

# ** C++ AMP backend Implementation for Torch7 ** #

##Introduction: ##

This repository hosts the C++ AMP backend implementation project for  [torch7](http://torch.ch/). Torch7 framework currently has a CUDA backend support in the form of [cutorch](https://github.com/torch/cutorch) and [cunn](https://github.com/torch/cunn) packages. The goal of this project is to develop  gputorch and gpunn packages that would functionally behave as  C++ AMP counterparts for existing cutorch and cunn packages. This project mainly targets the linux platform and makes use of the linux-based C++ AMP compiler implementation hosted [here](https://bitbucket.org/multicoreware/cppamp-driver-ng/overview)



##Repository Structure: ##

##Prerequisites: ##
* **dGPU**:  AMD firepro S9150
* **OS** : Ubuntu 14.04 LTS
* **Ubuntu Pack**: libc6-dev-i386, liblapack-dev
* **AMD APP SDK** : Ver 2.9.1 launched on 18/8/2014 from [here](http://developer.amd.com/tools-and-sdks/opencl-zone/amd-accelerated-parallel-processing-app-sdk/)
* **AMD Driver**: 15.20


## Installation Steps:    

### A. C++ AMP Compiler Installation: 

Make sure the parent directory chosen is say ~/ or any other folder of your choice. Lets take ~/ as an example

  (a) Prepare a directory for work space

       * mkdir ~/mcw_cppamp

       * cd ~/mcw_cppamp 
   
       * git clone https://bitbucket.org/multicoreware/cppamp-driver-ng.git src

       * cd ~/mcw_cppamp/src/

       * git checkout origin/torch-specific

  (b) Create a build directory and configure using CMake.

       * mkdir ~/mcw_cppamp/build

       * cd ~/mcw_cppamp/build

       * export CLAMP_NOTILECHECK=ON

       * cmake ../src -DCMAKE_BUILD_TYPE=Release -DCXXAMP_ENABLE_BOLT=ON -DOPENCL_HEADER_DIR=<path to SDK's OpenCL headers> -DOPENCL_LIBRARY_DIR=<path to SDK's OpenCL library> 
  
       * For example, cmake ../src -DCMAKE_BUILD_TYPE=Release -DCXXAMP_ENABLE_BOLT=ON  -DOPENCL_HEADER_DIR=/opt/AMDAPPSDK-3.0.0-Beta/include/CL -DOPENCL_LIBRARY_DIR=/opt/AMDAPPSDK-3.0-0-Beta/lib/x86_64


  (c) Build AMP

       * cd ~/mcw_cppamp/build

       * make [-j #] world && make          (# is the number of parallel builds. Generally it is # of CPU cores)

With this the C++ AMP Compiler installation is complete.

### B. Torch7 Installation 

(i) Install Prerequisites for Torch: 

Need sudo access for this step

       sudo curl -sk https://raw.githubusercontent.com/torch/ezinstall/master/install-deps | bash

(ii) Clone MCW Torch7 source codes

      * cd ~/

      * git clone https://bitbucket.org/multicoreware/mcw_cppamp_torch.git

      * cd ~/mcw_cppamp_torch

      * git checkout origin/multi-gpu


(iii) Run one time build script

      * cd ~/mcw_cppamp_torch
  
      * export MCWCPPAMPBUILD=<path_to>/mcw_cppamp/build (Here path_to points to parent folder of mcw_cppamp. ~/ in our case)

      * chmod 777 ./install.sh

      *./install.sh

(iv) At the completion of (iii) the following prompt appears. Press Enter key to continue build and installation of MCW source codes  

      Do you want to automatically prepend the Torch install location
      to PATH and LD_LIBRARY_PATH in your /home/.../.bashrc? (yes/no)
      [yes] >>> 

(v) Make sure to restart the machine or use another terminal so as to reflect the environment variable settings.

(vi) Run th command to invoke Torch IDE. You should see a prompt as below

      torchtest@mcw:~$ th
 
       ______             __   |  Torch7                                         
      /_  __/__  ________/ /   |  Scientific computing for Lua. 
       / / / _ \/ __/ __/ _ \  |  Type ? for help                                
      /_/  \___/_/  \__/_//_/  |  https://github.com/torch         
                               |  http://torch.ch                  
	
      th> 



## C. Unit Testing ##

Run the following commands to perform unit testing of different rocks.

(a) gputorch: 

       th -lgputorch -e "gputorch.test()"
     
              (or)

       ~/mcw_cppamp_torch/gputorch/build/gputorch.test
        

(b) gpunn:

       th -lgpunn -e "nn.testgpu()"

             (or)

       ~/mcw_cppamp_torch/gpunn/build/gpunn.test

(c) mcw_nn:

       th -lnn -e "nn.test()"

(d) mcw_torch7_core:

       th -ltorch -e "torch.test()"
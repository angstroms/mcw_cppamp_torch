#include "amp_math.h"

struct tanhupdateOutput_functor
{
  float operator()(const float& input) const restrict(amp,cpu)
  {
    return Concurrency::fast_math::tanh(input);
  }
};

static int gpunn_Tanh_updateOutput(lua_State *L)
{
  THCState *state = getGputorchState(L);
  THGPUTensor *input = (THGPUTensor*)luaT_checkudata(L, 2, "torch.GPUTensor");
  THGPUTensor *output = (THGPUTensor*)luaT_getfieldcheckudata(L, 1, "output", "torch.GPUTensor");
  long size = THGPUTensor_nElement(state, input);

  input = THGPUTensor_newContiguous(state, input);
  THGPUTensor_resizeAs(state, output, input);

  auto dv_output_data = output->get_bolt_dev_vec(state);
  auto dv_input_data = input->get_bolt_dev_vec(state);

  bolt::amp::transform(dv_input_data.begin() + input->storageOffset,
                       dv_input_data.begin() + input->storageOffset + size,
                       dv_output_data.begin() + output->storageOffset,
                       tanhupdateOutput_functor());

  THGPUTensor_free(state, input);
  return 1;
}

struct tanhupdateGradInput_functor
{
  float operator()(const float& output, const float& gradOutput) const restrict(amp,cpu)
  {
    return gradOutput * (1 - output * output);
  }
};

static int gpunn_Tanh_updateGradInput(lua_State *L)
{
  THCState *state = getGputorchState(L);
  THGPUTensor *output = (THGPUTensor*)luaT_getfieldcheckudata(L, 1, "output", "torch.GPUTensor");
  THGPUTensor *gradOutput = (THGPUTensor*)luaT_checkudata(L, 3, "torch.GPUTensor");
  THGPUTensor *gradInput = (THGPUTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", "torch.GPUTensor");
  long size = THGPUTensor_nElement(state, output);
  gradOutput = THGPUTensor_newContiguous(state, gradOutput);
  THGPUTensor_resizeAs(state, gradInput, output);

  auto dv_output_data = output->get_bolt_dev_vec(state);
  auto dv_gradOutput_data = gradOutput->get_bolt_dev_vec(state);
  auto dv_gradInput_data = gradInput->get_bolt_dev_vec(state);

  bolt::amp::transform(dv_output_data.begin() + output->storageOffset,
                       dv_output_data.begin() + output->storageOffset + size,
                       dv_gradOutput_data.begin() + gradOutput->storageOffset,
                       dv_gradInput_data.begin() + gradInput->storageOffset,
                       tanhupdateGradInput_functor());

  THGPUTensor_free(state, gradOutput);
  return 1;
}

static const struct luaL_Reg gpunn_Tanh__ [] = {
  {"Tanh_updateOutput", gpunn_Tanh_updateOutput},
  {"Tanh_updateGradInput", gpunn_Tanh_updateGradInput},
  {NULL, NULL}
};

static void gpunn_Tanh_init(lua_State *L)
{
  luaT_pushmetatable(L, "torch.GPUTensor");
  luaT_registeratname(L, gpunn_Tanh__, "nn");
  lua_pop(L,1);
}

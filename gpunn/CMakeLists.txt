CMAKE_MINIMUM_REQUIRED(VERSION 2.6 FATAL_ERROR)
CMAKE_POLICY(VERSION 2.6)

FIND_PACKAGE(Torch REQUIRED)

INCLUDE_DIRECTORIES("${Torch_INSTALL_INCLUDE}/THC")
INCLUDE_DIRECTORIES("${Torch_INSTALL_INCLUDE}/MTGP")
LINK_DIRECTORIES("${Torch_INSTALL_LIB}")

SET(src init.cpp)


SET (PREFIX "$ENV{MCWCPPAMPBUILD}")
SET (CLANG_AMP "${PREFIX}/compiler/bin/clang++")
SET (CLAMP_CONFIG "${PREFIX}/build/Release/bin/clamp-config")
execute_process(COMMAND ${CLAMP_CONFIG} --build --bolt --cxxflags
    OUTPUT_VARIABLE CLAMP_CXXFLAGS)
string(STRIP "${CLAMP_CXXFLAGS}" CLAMP_CXXFLAGS)
set (CLAMP_CXXFLAGS "${CLAMP_CXXFLAGS}")
execute_process(COMMAND ${CLAMP_CONFIG} --build --bolt --ldflags --shared
    OUTPUT_VARIABLE CLAMP_LDFLAGS)
string(STRIP "${CLAMP_LDFLAGS}" CLAMP_LDFLAGS)
set (CLAMP_CXXFLAGS "${CLAMP_CXXFLAGS} -Wall -Wno-deprecated-register -Wdeprecated-declarations")
set (CLAMP_LDFLAGS "${CLAMP_LDFLAGS}")




SET_PROPERTY(SOURCE init.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " ${CLAMP_CXXFLAGS}")
SET(luasrc init.lua test/test.lua AbstractParallel.lua ModelParallel.lua DataParallel.lua Optim.lua)
ADD_TORCH_PACKAGE(gpunn "${src}" "${luasrc}")
SET_PROPERTY(TARGET gpunn APPEND_STRING PROPERTY LINK_FLAGS " ${CLAMP_LDFLAGS} ${CLAMP_SHAREDFLAGS}")
TARGET_LINK_LIBRARIES(gpunn luaT THC TH)

ADD_EXECUTABLE(gpunn.test test/test.cpp)
TARGET_LINK_LIBRARIES(gpunn.test luaT THC TH luajit)
SET_PROPERTY(TARGET gpunn.test APPEND_STRING PROPERTY LINK_FLAGS " -I/usr/local/include -I./test")

# get imagenet-barebones folder path
set (fbpath ${CMAKE_CURRENT_SOURCE_DIR}/../imagenet-barebones)
set (gpunnpath ${CMAKE_CURRENT_SOURCE_DIR})

if(EXISTS "${fbpath}" AND EXISTS "${gpunnpath}/test/fbtest.cpp")
  ADD_EXECUTABLE(fbtest test/fbtest.cpp)  
  TARGET_LINK_LIBRARIES(fbtest luaT THC TH luajit)
  SET_PROPERTY(TARGET fbtest APPEND_STRING PROPERTY LINK_FLAGS " -I/usr/local/include -I./test")  
  
   GET_TARGET_PROPERTY(FB fbtest LOCATION)
   ADD_CUSTOM_COMMAND( TARGET fbtest
     POST_BUILD
     COMMAND ${CMAKE_COMMAND} -E copy ${FB} ${fbpath}
     COMMAND ${CMAKE_COMMAND} -E remove -f ${FB}
   )
endif()


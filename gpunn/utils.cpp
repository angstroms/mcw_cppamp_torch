#include "utils.h"

THCState* getGputorchState(lua_State* L)
{
    lua_getglobal(L, "gputorch");
    lua_getfield(L, -1, "getState");
    lua_call(L, 0, 1);
    THCState *state = (THCState*) lua_touserdata(L, -1);
    assert (state && "state is invalid!");
    lua_pop(L, 2);
    return state;
}
